<?php 

//use gracian_project\application\service\ConfigFactory;
use gracian_system\domain\model\valueObjects\SchemaVO;     
//use gracian_system\domain\service\Stopwatch;    
//use gracian_system\domain\exceptions\GracianAssertException;  
//use \GeneralTools\StopWatch;

class SchemaVoTest extends \PHPUnit_Framework_TestCase
{


    //_____________________________________________________________________________________________
    public function __construct(){      
        //$this->config = ConfigFactory::Instance('testConfig')->config;       
        //echo  $this->config['gracian LibPath']; exit();
        //require_once($this->config['gracian LibPath'] . "/generalTools/autoload.php");        

    }


    //_____________________________________________________________________________________________
    public function testParseArray() {  
        //echo new \DateTime(); echo "\n"    ;
        
        $commonFieldNames = array(
           'id'        => array('sanitize' => 'purge', 'validate' => 'required|integer'),
           'subdir'    => array('sanitize' => 'purge', 'validate' => 'required|alnum|min:2|max:2'),
        );  
        
       
        $variableFieldNames = array( 
        );   
       
        $expected_result = Array(
            'id' => Array(
                    'sanitizers' => Array(
                            'purge' => Array()
                        ),
                    'validators' => Array(
                            'required' => Array(),
                            'integer' => Array()
                        )
                ),
            'subdir' => Array(
                    'sanitizers' => Array(
                            'purge' => Array()
                        ),
                    'validators' => Array(
                            'required' => Array(),
                            'alnum' => Array(),
                            'min' => Array('value' => 2),
                            'max' => Array('value' => 2)
                        )
                )
        );
        
       
        $schemaVO = new SchemaVO($commonFieldNames, $variableFieldNames);
        $schema = $schemaVO->getSchema();
        //print_r($schema);

        $this->assertEquals($expected_result, $schema);                                                                     
    }  
                                
    

    //_____________________________________________________________________________________________
    public function testParseArrayWithVariableFieldNames() {  
        //echo new \DateTime(); echo "\n"    ;
        
        $commonFieldNames = array(
           'id'        => array('sanitize' => 'purge', 'validate' => 'required|integer'),
           'subdir'    => array('sanitize' => 'purge', 'validate' => 'required|alnum|min:2|max:2'),
        );  
        
       
        $variableFieldNames = array( 
            'introduction' => array('sanitize' => 'purge', 'validate' => 'string|max:255'),        
        );   
       
        $expected_result = Array(
            'id' => Array(
                    'sanitizers' => Array(
                            'purge' => Array()
                        ),
                    'validators' => Array(
                            'required' => Array(),
                            'integer' => Array()
                        )
                ),
            'subdir' => Array(
                    'sanitizers' => Array(
                            'purge' => Array()
                        ),
                    'validators' => Array(
                            'required' => Array(),
                            'alnum' => Array(),
                            'min' => Array('value' => 2),
                            'max' => Array('value' => 2)
                        )
                ), 
            'variable_fields' => Array(
                    'introduction' => Array(
                            'sanitizers' => Array(
                                    'purge' => Array()
                                ),
                            'validators' => Array
                                (
                                    'string' => Array(),
                                    'max' => Array ('value' => 255)
                                )
                        )
                )                     
        );
        
       
        $schemaVO = new SchemaVO($commonFieldNames, $variableFieldNames);
        $schema = $schemaVO->getSchema();
        //print_r($schema);

        $this->assertEquals($expected_result, $schema);                                                                     
    }  
                                
          
    
    
    
}    