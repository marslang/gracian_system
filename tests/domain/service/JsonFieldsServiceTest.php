<?php 

use gracian_system\domain\service\JsonFieldsService;


class JsonFieldsServiceTest extends \PHPUnit_Framework_TestCase
{


    //_____________________________________________________________________________________________
    public function __construct(){      
        //$this->config = ConfigFactory::Instance('testConfig')->config;
        $this->jsonFieldsService = new JsonFieldsService();    

    }
       

    //_____________________________________________________________________________________________
    public function test_jsonToVariableAndOrphanFields_with_orphanfield() {
        
        $record = array (
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"weesveld":"een weesveld","introduction":"zozo0xsz","bodytext":"a first page line ok"}'
        );
        
        $commonFieldNames = array(
            'id' => array(),
            'parent_id' => array(),
            'publish' => array(),
            'node_name' => array(),
            'title' => array()             
        ); 

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field' => 'with text'
        
        );   
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            //'variable_fields_json' => '{"weesveld":"een weesveld","introduction":"zozo0xsz","bodytext":"a first page line ok"}',
            'variable_fields' => Array(
                    'introduction' => 'zozo0xsz',
                    'bodytext' => 'a first page line ok'      
             ),               
             'orphan_fields' => Array(
                    'weesveld' => 'een weesveld',
             )   
        );       
        
        $item = $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $commonFieldNames, $variableFieldNames);
        //print_r($item); exit();
        
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }    
    //_____________________________________________________________________________________________
    public function test_jsonToVariableAndOrphanFields_with_acolade_tag() {
        
        $record = array (
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"weesveld":"een weesveld","introduction":"zozo0xsz","bodytext":"a first page line {a href=\"http:\/\/www.google.com\" title=\"google\"} ok"}'
            //'variable_fields_json' => '{"weesveld":"een weesveld","introduction":"zozo0xsz","bodytext":"a first page line ok"}'
        );
        
        $commonFieldNames = array(
            'id' => array(),
            'parent_id' => array(),
            'publish' => array(),
            'node_name' => array(),
            'title' => array()             
        ); 

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field' => 'with text'
        
        );   
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            //'variable_fields_json' => '{"weesveld":"een weesveld","introduction":"zozo0xsz","bodytext":"a first page line {a href=\"http:\/\/www.google.com\" title=\"google\"} ok"}',
            'variable_fields' => Array(
                    'introduction' => 'zozo0xsz',
                    'bodytext' => 'a first page line {a href="http://www.google.com" title="google"} ok'      
             ),
             'orphan_fields' => Array(
                    'weesveld' => 'een weesveld',
             )          
        );       
        
        $item = $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $commonFieldNames, $variableFieldNames);
        //print_r($item);  exit();
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }           
         
    
    //_____________________________________________________________________________________________
    // if jsonfield is empty and is not a variablefieldname, then don't add it  to record['variable_fields']
    public function test_jsonToVariableAndOrphanFields_with_empty_orphanfield() {
        
        $record = array (
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"weesveld":"","introduction":"zozo0xsz","bodytext":"a first page line ok"}'
        );
        
        $commonFieldNames = array(
            'id' => array(),
            'parent_id' => array(),
            'publish' => array(),
            'node_name' => array(),
            'title' => array()             
        ); 

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field' => 'with text'
        
        );   
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            //'variable_fields_json' => '{"weesveld":"","introduction":"zozo0xsz","bodytext":"a first page line ok"}',
            'variable_fields' => Array(
                    'introduction' => 'zozo0xsz',
                    'bodytext' => 'a first page line ok'      
             ),  
             'orphan_fields' => Array(
             )                   
        );       
        
        $item = $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $commonFieldNames, $variableFieldNames);
        //print_r($item);  exit();
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }     
    /*
    */
    
    //_____________________________________________________________________________________________
    // if jsonfield is empty and is not a variablefieldname, then don't add it  to record['variable_fields']
    public function test_jsonToVariableAndOrphanFields_jsonfield_has_same_name_as_a_commonField() {
        
        $record = array (
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"title":"variable field with same name","introduction":"zozo0xsz","bodytext":"a first page line ok"}'
        );
        
        $commonFieldNames = array(
            'id' => array(),
            'parent_id' => array(),
            'publish' => array(),
            'node_name' => array(),
            'title' => array()             
        ); 

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field' => 'with text'
        
        );   
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            //'variable_fields_json' => '{"title":"variable field with same name","introduction":"zozo0xsz","bodytext":"a first page line ok"}',
            'variable_fields' => Array(  
                    'introduction' => 'zozo0xsz',
                    'bodytext' => 'a first page line ok'      
             ), 
             'orphan_fields' => Array(
                    'title' => 'variable field with same name',
             )                
        );       
        
        $item = $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $commonFieldNames, $variableFieldNames);
        //print_r($item); exit();
        $this->assertEquals($expected_result, $item);                                                                     
    }     
    
    
    
    
    
    
    
    
    
 


    //_____________________________________________________________________________________________
    public function test_variableAndOrphanFieldsToJson() {
         $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
        );  
        $orphanFields = array(
            'an_orphan_field' => 'with text'
        );   
               
        $fields = array (
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',   
            'variable_fields' => $variableFields,
            'orphan_fields' => $orphanFields,
        );
  

           
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'parent_id' => 5,
            'publish' => 0,
            'node_name' => 'page',
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"introduction":"test bodytext","bodytext":"","an_orphan_field":"with text"}',
        );       
        
        $item = $this->jsonFieldsService->variableAndOrphanFieldsToJson($fields);
        //print_r($item); exit();
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }      
    
    
    //_____________________________________________________________________________________________
    public function test_variableAndOrphanFieldsToJson_remove_checked_orphan_fields() {
         $variableFields = array(
            'introduction' => 'test bodytext',
        );  
        $orphanFields = array(
            'an_orphan_field' => 'with text',   
            'second_orphan_field' => 'with more text',   
            'remove_second_orphan_field' => 'on'
        );   
               
        $fields = array (
            'id' => 89,
            'title' => 'aaa2a2a',   
            'variable_fields' => $variableFields,
            'orphan_fields' => $orphanFields,
        );
           
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"introduction":"test bodytext","an_orphan_field":"with text"}',
        );       
        
        $item = $this->jsonFieldsService->variableAndOrphanFieldsToJson($fields);
        //print_r($item); exit();
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }    
    
    //_____________________________________________________________________________________________
    public function test_variableAndOrphanFieldsToJson_remove_checked_orphan_fields_starting_with_remove() {
         $variableFields = array(
            'introduction' => 'test bodytext',
        );  
        $orphanFields = array(
            'an_orphan_field' => 'with text',   
            'second_orphan_field' => 'with more text',   
            'remove_second_orphan_field' => 'on',
            'remove_orphan_field' => 'with even more text',   
            'remove_remove_orphan_field' => 'on',
        );   
               
        $fields = array (
            'id' => 89,
            'title' => 'aaa2a2a',   
            'variable_fields' => $variableFields,
            'orphan_fields' => $orphanFields,
        );
           
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"introduction":"test bodytext","an_orphan_field":"with text"}',
        );       
        
        $item = $this->jsonFieldsService->variableAndOrphanFieldsToJson($fields);
        //print_r($item); exit();
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }      
    
    
    //_____________________________________________________________________________________________
    public function test_variableAndOrphanFieldsToJson_remove_all_orphan_fields() {
         $variableFields = array(
            'introduction' => 'test bodytext',
        );  
        $orphanFields = array(
            'an_orphan_field' => 'with text',   
            'remove_an_orphan_field' => 'on',    
            'second_orphan_field' => 'with more text',   
            'remove_second_orphan_field' => 'on'
        );   
               
        $fields = array (
            'id' => 89,
            'title' => 'aaa2a2a',   
            'variable_fields' => $variableFields,
            'orphan_fields' => $orphanFields,
        );
           
        //echo "\n--- test_jsonToVariableAndOrphanFields_ok ---\n";      
        $expected_result = Array(
            'id' => 89,
            'title' => 'aaa2a2a',
            'variable_fields_json' => '{"introduction":"test bodytext"}',
        );       
        
        $item = $this->jsonFieldsService->variableAndOrphanFieldsToJson($fields);
        //print_r($item); exit();
        //$orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals($expected_result, $item);                                                                     
    }           
           
   
    /*
    //_____________________________________________________________________________________________
    public function test_getOrphanJsonFields_HasOrphanFieldWithText() {

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field' => 'with text'
        
        );            
        $orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals(array('an_orphan_field' => 'with text'), $orphanFields);                                                                     
    }

    //_____________________________________________________________________________________________
    public function test_getOrphanJsonFields_HasOrphanFieldWithoutText() {

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field' => ''
        
        );    
        
        $orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals(array(), $orphanFields);                                                                     
        //print_r($orphanFields); //exit();

    }      
    
    //_____________________________________________________________________________________________
    public function test_getOrphanJsonFields_HasOrphanFieldsWithAndWithoutText() {

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
            'an_orphan_field_with' => 'some text',      
            'an_orphan_field_without' => ''
        
        );    
        
        $orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals(array('an_orphan_field_with' => 'some text'), $orphanFields);                                                                     
        //print_r($orphanFields); //exit();

    }         

    //_____________________________________________________________________________________________
    public function test_getOrphanJsonFields_HasNoOrphanField() {

        $variableFieldNames = array(
             'introduction' => '', 
             'bodytext' => ''
        );      

        $variableFields = array(
            'introduction' => 'test bodytext',
            'bodytext' => '',
        );    
        
        $orphanFields = $this->jsonFieldsService->getOrphanJsonFields($variableFieldNames, $variableFields);
        $this->assertEquals(array(), $orphanFields);                                                                     
        //print_r($orphanFields); //exit();

    }    
   */
}
