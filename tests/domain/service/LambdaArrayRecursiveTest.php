<?php 

//use gracian_project\application\service\ConfigFactory;
use gracian_system\domain\service\LambdaArrayRecursive;     
//use gracian_system\domain\service\Stopwatch;    
use gracian_system\domain\exceptions\GracianAssertException;  
//use \GeneralTools\StopWatch;

class LambdaArrayRecursiveTest extends \PHPUnit_Framework_TestCase
{


    //_____________________________________________________________________________________________
    public function __construct(){      
        //$this->config = ConfigFactory::Instance('testConfig')->config;       
        //echo  $this->config['gracian LibPath']; exit();
        //require_once($this->config['gracian LibPath'] . "/generalTools/autoload.php");        

    }


    //_____________________________________________________________________________________________
    public function testParseArray() {

        $a = array(
             'a' => 'one', 
             'b' => 'two',
             'c' => array(
                 'ca' => 'three-one',
                 'cb' => 'three-two', 
                 'cc' => array( 
                     'cca' => 'three-three-one'
                 )     
             ),
             'd' => 'four' 
        );      

        //with lambdaFunction  
        $lambdaFunc = function($arg1) {
            return $arg1 .  '-y';  
        };      
        $lar = new LambdaArrayRecursive($a , $lambdaFunc);
        
        // with callback to class, method             
        //$lar = new ArrayService($a , $this, 'doStuff');    
        
        //StopWatch::start();         
        $a2 = $lar->parseArray();    
        //echo "Elapsed time: " . StopWatch::elapsed() . " seconds\n";       
        //print_r($a2);          
        
        $expected_result = Array(
            'a' => 'one-y',
            'b' => 'two-y',
            'c' => Array(
                'ca' => 'three-one-y',
                'cb' => 'three-two-y',
                'cc' => Array(
                    'cca' => 'three-three-one-y'
                )
            ),
            'd' => 'four-y' 
        );   
        
        $this->assertEquals($expected_result, $a2);                                                                     
    }  
    
        //_____________________________________________________________________________________________
    public function testParseArrayTooManyDimensions() {

        $a = array(
            'a1' => 'one', 
            'a2' => array(
                'b' => array( 
                    'c' => array(
                        'd' => array(
                            'e' => array(
                                'f' => array(
                                    'g' => array(
                                        'h' => 'eight levels'
                                    )
                                )
                            )
                        )
                    )
                )     
            )
        );   
        
        $lambdaFunc = function($arg1) {
            return $arg1 .  '-q';  
        };   
        try{
            $lar = new LambdaArrayRecursive($a , $lambdaFunc);       
            //StopWatch::start();         
            $a2 = $lar->parseArray($a);
            //$a2 = $lar->getParsedArray();           
        }catch (\Exception $e){        
            //echo "Elapsed time: " . StopWatch::elapsed() . " seconds\n";       
            $expected_result = 'GracianAssertException: ArrayService::change_array_value() array is more than 6 dimensions. add a dimension handler to this method'; 
            $this->assertEquals($expected_result, $e->getMessage());  
        }
    }   

    
    // with callback to class, method             
    public function doStuff($val){
         return $val . '-z';
    } 
    
    
    
    
}    