<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\domain\service\ImagelibService;   
use gracian_project\domain\service\NodeFactory;       
use gracian_system\application\service\TestDbService;
 
use gracian_system\domain\exceptions\GracianException;      


class ImagelibServiceTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        //$this->nodeFactory = new NodeFactory();  
        $this->imagelibService = new ImagelibService($this->config);      

        //$this->testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';    
        
        
        //$session['login'] = Array('id' => '1', 'fullname' => 'Mars','username' => 'su','role' => 'su');    
        //$this->config->sessionVo->transformIn($session);


    }
    
    //_____________________________________________________________________________________________
    public function testIsImage(){     
        //$result = $this->imagelibService->isImage('test.jpg');   
        $this->assertEquals(true, $this->imagelibService->isImage('test.jpg'));    
        $this->assertEquals(true, $this->imagelibService->isImage('test.jpeg'));    
        $this->assertEquals(true, $this->imagelibService->isImage('test.png'));    
        $this->assertEquals(true, $this->imagelibService->isImage('test.gif'));    
        $this->assertEquals(false, $this->imagelibService->isImage('test.bmp'));    
        $this->assertEquals(false, $this->imagelibService->isImage(''));      
        $this->assertEquals(true, $this->imagelibService->isImage('39sgejky9wa-Screen_Shot_2013-08-23_at_12.47.14_PM.png'));      
        
    }     
    
    //_____________________________________________________________________________________________
    public function testGetFileOriginalUrl(){    
        $result = $this->imagelibService->getFileOriginalUrl('aa', 'test.jpg');     
        
        //$expectedResult = array('original' => $this->config->app['media_ original_abs_url'] . 'aa/test.jpg');   
        $expectedResult = array('original' => $this->config->app['media_url'] . '/original/aa/test.jpg');  
        $this->assertEquals($expectedResult, $result);  
        
        
        $result = $this->imagelibService->getFileOriginalUrl('aa', '');   
        $expectedResult = array();  
        $this->assertEquals($expectedResult, $result);
    }


    //_____________________________________________________________________________________________
    public function testGetFileResizedUrls(){    
    	$image_resize = array(
    	    'small'  => Array( 'width' => 100, 'height' => 100),
    		'medium' => Array( 'width' => 200, 'height' => 200),
    	    'large'  => Array( 'width' => 500, 'height' => 500)   
        );  
    
        $result = $this->imagelibService->getFileResizedUrls('aa', 'water.jpg', $image_resize);  
        //print_r($result);
        $expectedResult = Array(
            'original' => $this->config->app['media_url'] . '/original/aa/water.jpg',
            'small'    => $this->config->app['media_url'] . '/resized/aa/water_100x100.jpg',
            'medium'   => $this->config->app['media_url'] . '/resized/aa/water_200x200.jpg',
            'large'    => $this->config->app['media_url'] . '/resized/aa/water_500x500.jpg'
        );                                                                                                      
        $this->assertEquals($expectedResult, $result);   
        
        

    }

/*    
    //_____________________________________________________________________________________________
    public function testGetCrumbWithoutId(){     
        try{
            $node = $this->nodeFactory->getNode('page');
            $crumb = $this->crumbService->getCrumb($node);   
        }catch(GracianException $e) {
            $this->assertEquals('CrumbService: node has empty crumbItem. probably you called getCrumb() without an id.', $e->getUserMessage());   
        }  
    }     
    
    //_____________________________________________________________________________________________
    public function testGetCrumbWithWrongId(){     
        try{  
            $id = 123;
            $node = $this->nodeFactory->getNode('page');
            $crumb = $this->crumbService->getCrumb($node, $id);   
        }catch(GracianException $e) {         
            //echo $e->getUserMessage();
            $this->assertEquals('c No item found with this id 123', $e->getUserMessage());   
        }  
    }          
*/                
             
}
         