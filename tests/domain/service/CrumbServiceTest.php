<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\domain\service\CrumbService;   
use gracian_project\domain\service\NodeFactory;       
use gracian_system\application\service\TestDbService;
 
use gracian_system\domain\exceptions\GracianException;      

/*
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianFileValidationException;
*/

class CrumbServiceTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        $this->nodeFactory = new NodeFactory();  
        $this->crumbService = new CrumbService();      
        $this->testDbService = new TestDbService();

        //$this->testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';    
        
        $this->testDbService->truncateTestTable('stree');
        $this->testDbService->addTestStreeRoot();    
        $this->testDbService->addTestStreeChapter();
        $this->testDbService->addTestStreePage();   
        
        $session['login'] = Array('id' => '1', 'fullname' => 'Mars','username' => 'su','role' => 'su');    
        $this->config->sessionVo->transformIn($session);


    }
    
    //_____________________________________________________________________________________________
    public function testGetCrumb(){     
        
        try{
            $id = 3;
            $node = $this->nodeFactory->getNode('page');
            $crumb = $this->crumbService->getCrumb($node, $id);   
        }catch(\Exception $e) {
            echo 'testGetCrumb: ' . $e->getMessage();  
        }  
        $expectedResult = Array( 
            Array('id' => '1', 'title' => 'root', 'node_name' => 'root'),
            Array('id' => '2', 'title' => 'Chapter Item', 'node_name' => 'chapter'),
            Array('id' => '3', 'title' => 'Page Item', 'node_name' => 'page')
        );
        $this->assertEquals($expectedResult, $crumb);    
    }  
    
    //_____________________________________________________________________________________________
    public function testGetCrumbWithoutId(){     
        try{
            $node = $this->nodeFactory->getNode('page');
            $crumb = $this->crumbService->getCrumb($node);   
        }catch(GracianException $e) {
            $this->assertEquals('CrumbService: node has empty crumbItem. probably you called getCrumb() without an id.', $e->getUserMessage());   
        }  
    }     
    
    //_____________________________________________________________________________________________
    public function testGetCrumbWithWrongId(){     
        try{  
            $id = 123;
            $node = $this->nodeFactory->getNode('page');
            $crumb = $this->crumbService->getCrumb($node, $id);   
        }catch(GracianException $e) {         
            //echo $e->getUserMessage();
            $this->assertEquals('c No item found with this id 123', $e->getUserMessage());   
        }  
    }          
                
             
}
         