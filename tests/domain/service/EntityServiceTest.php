<?php 

use gracian_system\domain\service\EntityService;     

class EntityServiceTest extends \PHPUnit_Framework_TestCase
{    
    
    //_____________________________________________________________________________________________
    public function __construct(){      
        $this->entityService = new EntityService();    
    }
                                   
    //_____________________________________________________________________________________________
    public function test_mergeItemWithFields(){   
        $item = array(            
            'parent_id' => 2,
            'title' => '',
            'node_name' => 'article'    
        );

        $fields = array(            
            'title' => 'field title',
            'extra_field' => 'an extra'    
        );   
        
        $expected_result =  array(            
            'parent_id' => 2,
            'title' => 'field title',
            'node_name' => 'article',    
            'extra_field' => 'an extra'     
        );      
        
        $result = $this->entityService->mergeItemWithFields($item, $fields);
        $this->assertEquals($expected_result, $result);     
    }       
    
    //_____________________________________________________________________________________________
    public function test_filterItemWithWhitelist(){   
        $item = array(            
            'parent_id' => 2,
            'title' => 'een',
            'node_name' => 'article'    
        );

        $commonFieldNames = array(   
            'parent_id' => array('sanitize' => 'purge', 'validate' => 'required|integer'),
            'title' => array('sanitize' => 'purge', 'validate' => 'required|string|max:255')
        );              

        $expected_result =  array(            
            'parent_id' => 2,
            'title' => 'een'
        );     
        
        $result = $this->entityService->filterItemWithWhitelist($item, $commonFieldNames);
        $this->assertEquals($expected_result, $result); 
    }       
    
    //_____________________________________________________________________________________________
    public function test_correctCheckboxes_publish_is_on(){    
        $fields = array(            
            'parent_id' => 2,
            'title' => 'een',
            'publish' => 'on'    
        ); 
        $expected_result = array(            
            'parent_id' => 2,
            'title' => 'een',
            'publish' => 1    
        );           
        $arrayOfCheckboxFieldnames = array('publish');      
        $result = $this->entityService->correctCheckboxes($fields, $arrayOfCheckboxFieldnames) ;
        //print_r($result);  
        $this->assertEquals($expected_result, $result);  
    }    

    //_____________________________________________________________________________________________
    public function test_correctCheckboxes_publish_is_0(){    
        $fields = array(            
            'parent_id' => 2,
            'title' => 'een',
            'publish' => 0    
        );  
        $arrayOfCheckboxFieldnames = array('publish');      
        $result = $this->entityService->correctCheckboxes($fields, $arrayOfCheckboxFieldnames) ;
        //print_r($result);    
        $this->assertEquals($fields, $result);      
    }  
    
    //_____________________________________________________________________________________________
    public function test_correctCheckboxes_publish_is_1(){    
        $fields = array(            
            'parent_id' => 2,
            'title' => 'een',
            'publish' => 1    
        );  
        $arrayOfCheckboxFieldnames = array('publish');      
        $result = $this->entityService->correctCheckboxes($fields, $arrayOfCheckboxFieldnames) ;
        //print_r($result);    
        $this->assertEquals($fields, $result);      
    }   
    
    //_____________________________________________________________________________________________
    public function test_correctCheckboxes_publish_is_missing(){    
        $fields = array(            
            'parent_id' => 2,
            'title' => 'een'
        );   
        $expected_result = array(            
            'parent_id' => 2,
            'title' => 'een',
            'publish' => 0    
        );           
        $arrayOfCheckboxFieldnames = array('publish');      
        $result = $this->entityService->correctCheckboxes($fields, $arrayOfCheckboxFieldnames) ;
        //print_r($result);    
        $this->assertEquals($expected_result, $result);      
    }                
    
}
                