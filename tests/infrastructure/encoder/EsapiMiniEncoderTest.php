<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\infrastructure\encoder\EsapiMiniEncoder;     
use gracian_system\domain\exceptions\GracianIntrusionException;     
/*
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;      
use gracian_system\domain\exceptions\GracianFileValidationException;
*/

class EsapiMiniEncoderTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        //$this->config = ConfigFactory::Instance('testConfig')->config;
        $this->encoder = new EsapiMiniEncoder();
        //$this->testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';  
    } 
    
        /*
         $input = 'no encoding: ../';  // no encoding  
         $input = 'hexadecimal encoding:  %2E%2E%2f'; // hexadecimal encoding
         $input = 'double encoding: %252E%252E%252F'; // double encoding      
        */       
    
    //_____________________________________________________________________________________________
    public function testEncoderNoEncoding(){         
        $inputString = '../';
        $result = $this->encoder->canonicalize($inputString); 
        $this->assertEquals('../', $result);    
    }          

    //_____________________________________________________________________________________________
    public function testEncoderNoEncodingWithUtf8(){         
        $inputString = '../ü';
        $result = $this->encoder->canonicalize($inputString); 
        $this->assertEquals('../ü', $result);    
    }          
    
    //_____________________________________________________________________________________________
    public function testEncoderWithHexadecimalEncoding(){         
        $inputString = '%2E%2E%2f';
        $result = $this->encoder->canonicalize($inputString); 
        $this->assertEquals('../', $result);    
    }   
    
    //_____________________________________________________________________________________________
    public function testEncoderWithDoubleEncoding(){         
        $inputString = '%252E%252E%252F';
        try{
            $result = $this->encoder->canonicalize($inputString); 
        }catch(GracianIntrusionException $e) {
            $this->assertEquals('Input validation failure', $e->getUserMessage());    
        }  
    }    
    
    //_____________________________________________________________________________________________
    public function testEncoderCanonicalizeArray(){         
        $inputString = array('title' => 'een', 'subtitle' => 'twee%2E%2E%2f');    
        $expectedResult = array('title' => 'een', 'subtitle' => 'twee../');
        $result = $this->encoder->canonicalizeArray($inputString);      
        //print_r($result);
        $this->assertEquals($expectedResult, $result);    
    }           
             
}
         