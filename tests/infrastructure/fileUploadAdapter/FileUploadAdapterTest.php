<?php

use gracian_project\application\service\ConfigFactory;
//use gracian_system\application\service\FileService;    
use gracian_system\domain\exceptions\GracianUploadException;  
use gracian_system\domain\exceptions\GracianAssertException;


class FileUploadAdapterTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        //$this->fileService = new FileService();      
        //$this->config->fileUploadAdapter
        $this->testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';    
        //$this->targetDir = $this->config->app['file_ upload_abs_path'];
        $this->targetDir = $this->config->app['media_abs_path'] . '/original';
        $this->subdir = '_aa_test';
        $this->filename = "star.png";           
    }
    
    //_____________________________________________________________________________________________
    public function testUploadFileNoErrors(){    
 
        $fileToUpload = Array ( 
            "name" => $this->filename, 
            "type" => "image/png",
            "tmp_name" => $this->testMedia,
            "error" => '0',
            "size" => '640'  
        ); 
        try{   
            //$this->fileService->uploadFile($fileToUpload, $subdir);//, $fields);
            $this->config->fileUploadAdapter->uploadFile($this->targetDir, $this->subdir, $fileToUpload);
        }catch(GracianUploadException $e) {  
            echo '-a-'.  $e->getUserMessage();  
        }    
        $this->assertEquals(TRUE,  $this->config->fileUploadAdapter->exists($this->targetDir, $this->subdir, $this->filename));    
        
         
        // cleanup file  
        try{           
            //$this->fileService->removeFile( $subdir, $fileToUpload['name']);     
            $this->config->fileUploadAdapter->removeFile($this->targetDir, $this->subdir, $this->filename);
        }catch(GracianUploadException $e) {  
            echo '-b-'.  $e->getUserMessage();  
        }        
        $this->assertEquals(FALSE,   $this->config->fileUploadAdapter->exists($this->targetDir, $this->subdir, $this->filename));    

    }   
    
    //_____________________________________________________________________________________________
    public function testUploadFileThatAlreadyExist(){         
        $fileToUpload = Array ( 
            "name" => $this->filename, 
            "type" => "image/png",
            "tmp_name" => $this->testMedia,
            "error" => '0',
            "size" => '640'  
        ); 
        // eerste upload         
        try{   
            //$this->fileService->uploadFile($fileToUpload, $subdir);//, $fields);         
            $this->config->fileUploadAdapter->uploadFile($this->targetDir, $this->subdir, $fileToUpload);
            
        }catch(GracianUploadException $e) {  
            echo '-c-'.  $e->getUserMessage();  
        } 
        // tweede upload 
        try{   
            //$this->fileService->uploadFile($fileToUpload, $subdir);//, $fields);    
            $this->config->fileUploadAdapter->uploadFile($this->targetDir, $this->subdir, $fileToUpload);
            
        }catch(GracianUploadException $e) {  
            $this->assertEquals('GracianUploadException: TestFileUploadAdapter: File already exists.', $e->getUserMessage());    
        }finally{  
            // cleanup file
            //$this->fileService->removeFile( $subdir, $fileToUpload['name']);        
            $this->config->fileUploadAdapter->removeFile($this->targetDir, $this->subdir, $this->filename);          
        }      
        $this->assertEquals(FALSE,   $this->config->fileUploadAdapter->exists($this->targetDir, $this->subdir, $this->filename));       
              
    }             
    
    
}
         