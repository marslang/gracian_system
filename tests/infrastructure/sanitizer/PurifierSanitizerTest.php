<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\infrastructure\sanitizer\PurifierSanitizer;     

use gracian_system\domain\exceptions\GracianValidationException;      


class PurifierSanitizerTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;       
        $this->sanitizer = new PurifierSanitizer($this->config->app['gracian_cache_path'], $this->config->app['gracian_lib_custom_abs_path']);
    }
   
    //_____________________________________________________________________________________________
    public function testPurifierSanitizerNoErrors(){    
        $fields  = Array( 'id' => 2, 'name' => 'movies');   
        $schema['id']['sanitizers']['purge'] = Array();   
        $schema['name']['sanitizers']['purge'] = Array();   
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);     
        $this->assertEquals($fields , $fieldsWhitelist);    
    }     
    
    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_whitelistfilter(){    
        $fields  = Array( 'id' => 2, 'name' => 'movies', 'aarrgg' => 'field not in whitelist');   
        $schema['id']['sanitizers']['purge'] = Array();   
        $schema['name']['sanitizers']['purge'] = Array();   
        $expected_result =  Array('id' => 2, 'name' => 'movies');                
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);     
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }     
        
    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_purge(){    
        $fields  = Array( 'id' => 2, 'name' => 'test<b>bold</b>text');   
        $expected_result =  Array( 'id' => 2, 'name' => 'testboldtext'); 
        $schema['id']['sanitizers']['purge'] = Array();
        $schema['name']['sanitizers']['purge'] = Array();         
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);    
        //print_r($fieldsWhitelist);
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }   
    
    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_escape(){    
        $fields  = Array( 'id' => 2, 'name' => 'test<b>bold</b>text');   
        $expected_result =  Array( 'id' => 2, 'name' => 'test&#60;b&#62;bold&#60;/b&#62;text'); 
        $schema['id']['sanitizers']['purge'] = Array();
        $schema['name']['sanitizers']['escape'] = Array();         
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);    
        //print_r($fieldsWhitelist);
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }    
    
    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_raw(){    
        $fields  = Array( 'id' => 2, 'name' => 'test<b>bold</b>text');   
        $expected_result =  Array( 'id' => 2, 'name' => 'test<b>bold</b>text'); 
        $schema['id']['sanitizers']['purge'] = Array();
        $schema['name']['sanitizers']['raw'] = Array();         
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);    
        //print_r($fieldsWhitelist);
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }   


    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_purge_nospaces(){    
        $fields  = Array('name' => 'test three  <b>words</b>');   
        $expected_result =  Array('name' => 'testthreewords'); 
        $schema['name']['sanitizers']['purge'] = Array();         
        $schema['name']['sanitizers']['nospaces'] = Array();         
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);    
        //print_r($fieldsWhitelist);
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }                  
    
    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_with_variable_fields(){    
        $fields  = Array(
            'name' => 'test three  <b>words</b>',
            'variable_fields' =>array('introduction' => 'test varfields  <b>introduction</b>')
        );   
        $expected_result =  Array(
            'name' => 'testthreewords',
            'variable_fields' =>array('introduction' => 'test varfields  introduction')
        ); 
        $schema['name']['sanitizers']['purge'] = Array();         
        $schema['name']['sanitizers']['nospaces'] = Array();         
        $schema['variable_fields']['introduction']['sanitizers']['purge'] = Array();         
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);    
        //print_r($fieldsWhitelist);
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }    
          
    //_____________________________________________________________________________________________
    public function testPurifierSanitizer_with_orphan_fields(){    
        $fields  = Array(
            'name' => 'test three  <b>words</b>',
            'variable_fields' =>array('introduction' => 'test varfields  <b>introduction</b>'),
            'orphan_fields' =>array('firstorphan' => 'first orphan field')
        );   
        $expected_result =  Array(
            'name' => 'testthreewords',
            'variable_fields' =>array('introduction' => 'test varfields  introduction'),
            'orphan_fields' =>array('firstorphan' => 'first orphan field')
        ); 
        $schema['name']['sanitizers']['purge'] = Array();         
        $schema['name']['sanitizers']['nospaces'] = Array();         
        $schema['variable_fields']['introduction']['sanitizers']['purge'] = Array();         
        $schema['orphan_fields'] = Array();         
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);    
        //print_r($fieldsWhitelist);
        $this->assertEquals($expected_result , $fieldsWhitelist);    
    }          
 
}
         