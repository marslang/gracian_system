<?php

use gracian_project\application\service\ConfigFactory;
//use gracian_system\application\service\FileService;    
//use gracian_system\domain\exceptions\GracianUploadException;  
//use gracian_system\domain\exceptions\GracianAssertException;   
use gracian_system\infrastructure\validators\ValitronValidatorAdapter;  
use gracian_system\domain\exceptions\GracianValidationException;      


class ValitronValidatorAdapterTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        //$this->fileService = new FileService();      
        //$this->config->fileUploadAdapter
        //$this->testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';    
        //$this->targetDir = $this->config->app['file_ upload_abs_path'];
        //$this->subdir = '_aa_test';
        //$this->filename = "star.png";    
             
    }    
    
    //_____________________________________________________________________________________________
    public function testValitronValidateFlattenArray(){    
         
         $treeArray = array ('id' => 2, 'name' => 'john', 'variable_fields' => array('intro' => 'an introduction'), 'publish' => 1 );
         $expected_result = array ('id' => 2, 'name' => 'john', 'variable_fields:intro' =>  'an introduction', 'publish' => 1 );
         $valitronValidatorAdapter = new ValitronValidatorAdapter();  
         $flattenArray = $valitronValidatorAdapter->flattenArray($treeArray); 
         //print_r($flattenArray);  
         $this->assertEquals($expected_result , $flattenArray);    
         //exit();                                      
    }    
    
    //_____________________________________________________________________________________________
    public function testValitronValidate_with_variable_fields_orphan_fields(){    

        $fields  = Array(
            'name' => 'test three  <b>words</b>',
            'variable_fields' =>array('introduction' => 'test varfields  <b>introduction</b>'),
            'orphan_fields' =>array('firstorphan' => 'first orphan field')
        );   
        $expected_result =  Array(
            'name' => 'testthreewords',
            'variable_fields' =>array('introduction' => 'test varfields  introduction'),
            'orphan_fields' =>array('firstorphan' => 'first orphan field')
        );     
                 
        $expected_result = array (
            'name' => Array( 'Name must contain only letters a-z, numbers 0-9, dashes and underscores'),
            'introduction' => Array('Introduction must contain less than 12 characters')         
        );


        $schema['name']['validators']['required'] = Array();   
        $schema['name']['validators']['alpha_dash'] = Array();           
        $schema['variable_fields']['introduction']['validators']['lengthMax'] = Array('value'=> 12);           
                 
         $valitronValidatorAdapter = new ValitronValidatorAdapter();  
         //$flattenArray = $valitronValidatorAdapter->flattenArray($fields);          
         $isValid = $valitronValidatorAdapter->validate($fields, $schema);      
         //print_r($valitronValidatorAdapter->getValidationErrorMessages());      
         $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());                
         //$this->assertEquals($expected_result , $flattenArray);    
         //exit();                                      
    }
                  
    
    //_____________________________________________________________________________________________
    public function testValitronValidateNoErrors(){    
        
        $fields  = Array( 'id' => 2, 'name' => 'movies');   
        
        //$schema['id']['sanitizers']['purge'] = Array();
        $schema['id']['validators']['required'] = Array();   
               
        //$schema['name']['sanitizers']['purge'] = Array();         
        $schema['name']['validators']['required'] = Array();   
        $schema['name']['validators']['length'] = Array('value'=> 6);       
        
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);    
        $this->assertEquals(TRUE , $isValid);    
    }    
    //_____________________________________________________________________________________________
    public function testValitronValidateNoErrors2(){    
        $fields  = Array( 'id' => '');   
        $schema['id']['validators']['integer'] = Array();           
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);    
        $this->assertEquals(TRUE , $isValid);   
    }   

    //_____________________________________________________________________________________________
    public function testValitronValidate_REQUIRED(){    
        
        $fields  = Array( 'id' => '', 'name' => 't');   
        
        //$schema['id']['sanitizers']['purge'] = Array();
        $schema['id']['validators']['required'] = Array();   
               
        //$schema['name']['sanitizers']['purge'] = Array();         
        $schema['name']['validators']['required'] = Array();   
        $schema['name']['validators']['lengthMin'] = Array('value'=> 2);       
        $schema['name']['validators']['email'] = Array();      
        
        $expected_result = Array(    
        'id' => Array ( 'Id is required'  ) ,   
        'name' => Array ( 'Name must contain more than 2 characters',   'Name is not a valid email address' )
        );    
        
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);      
        //print_r($valitronValidatorAdapter->getValidationErrorMessages());
        $this->assertEquals(FALSE , $isValid);    
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    

    }   
    
   //_____________________________________________________________________________________________
    public function testValitronValidate_Integer(){    
        $fields  = Array( 'id' => 'a');   
        $schema['id']['validators']['integer'] = Array();           
        $expected_result = Array('id' => Array( 'Id must be an integer (0-9)'));    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);    
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }           
    
   //_____________________________________________________________________________________________
    public function testValitronValidate_Required_Integer(){    
        $fields  = Array( 'id' => 'q');   
        $schema['id']['validators']['required'] = Array();           
        $schema['id']['validators']['integer'] = Array();           
        $expected_result = Array(
            'id' => Array('Id is required', 
            'Id must be an integer (0-9)')
        );    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);     
        $valitronValidatorAdapter->getValidationErrorMessages();
//        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }           
    
   //_____________________________________________________________________________________________
    public function testValitronValidate_alpha_dash(){    
        $fields  = Array( 'name' => 'abc-123_DEF#');   
        $schema['name']['validators']['alpha_dash'] = Array();           
        $expected_result = Array('name' => Array( 'Name must contain only letters a-z, numbers 0-9, dashes and underscores'));    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);     
        //print_r($valitronValidatorAdapter->getValidationErrorMessages());
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }       

   //_____________________________________________________________________________________________
    public function testValitronValidate_mysqldate_ok(){    
        $fields  = Array( 'date' => '2006-03-22 14:22:12');   
        //$fields  = Array( 'date' => 'helnloz');   
        //$schema['id']['validators']['required'] = Array();           
        $schema['date']['validators']['mysqldate'] = Array();           
        $expected_result = Array(  'date' => Array('Date contains invalid characters')  );    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);   
        if( !$isValid){
            print_r($valitronValidatorAdapter->getValidationErrorMessages());
        }   
        $this->assertEquals(TRUE , $isValid);        
        //$this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }       

   //_____________________________________________________________________________________________
    public function testValitronValidate_mysqldate_err(){    
        $fields  = Array( 'date' => '2006-03-22 14:22:10a');   
        //$fields  = Array( 'date' => 'helnloz');   
        //$schema['id']['validators']['required'] = Array();           
        $schema['date']['validators']['mysqldate'] = Array();           
        $expected_result = Array(  'date' => Array('Date date must match yyyy-mm-dd hh:mm:ss')  );    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);   
        //if( !$isValid){
        //    print_r($valitronValidatorAdapter->getValidationErrorMessages());
        //}   
        //$this->assertEquals(TRUE , $isValid);        
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }         
        
   //_____________________________________________________________________________________________
    public function testValitronValidate_max25(){    
        $fields  = Array( 'name' => '123456789012345678901234567890');   
        $schema['name']['validators']['lengthMax'] = Array('value' => 25);           
        $expected_result = Array('name' => Array( 'Name must contain less than 25 characters'));    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);     
        //print_r($valitronValidatorAdapter->getValidationErrorMessages());
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }        

   //_____________________________________________________________________________________________
    public function testValitronValidate_title_ok(){    
        $fields  = Array( 'title' => 'een titel');   
        $schema['title']['validators']['title'] = Array();           
        $expected_result = Array('title' => Array( 'Title can only contain letters, numbers spaces and dashes'));    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);     
        $this->assertEquals(TRUE , $isValid);  
    }     
                       
   //_____________________________________________________________________________________________
    public function testValitronValidate_title_err(){    
        $fields  = Array( 'title' => 'een titel~');   
        $schema['title']['validators']['title'] = Array();           
        $expected_result = Array('title' => Array( 'Title can only contain letters, numbers spaces and dashes'));    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);     
        //print_r($valitronValidatorAdapter->getValidationErrorMessages());
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }   
    
   //_____________________________________________________________________________________________
    public function testValitronValidate_password_err(){    
        $fields  = Array( 'password' => 'geengoedpassword');   
        $schema['password']['validators']['password4t2x'] = Array();           
        $expected_result = Array('password' => Array( 'Password must contain upper and lower case letters, numbers and special characters (at least 2 of each), cannot contain spaces and must be between 8 and 72 characters.'));    
        $valitronValidatorAdapter = new ValitronValidatorAdapter();
        $isValid = $valitronValidatorAdapter->validate($fields, $schema);     
        //print_r($valitronValidatorAdapter->getValidationErrorMessages());
        $this->assertEquals($expected_result , $valitronValidatorAdapter->getValidationErrorMessages());    
    }   
    
           
          
}
         