<?php

//use gracian_system\application\command\stree\ShowStreeCommand;

use gracian_project\application\service\ConfigFactory;
use gracian_system\application\service\LogService;

use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;




class LogServiceTest extends \PHPUnit_Framework_TestCase
{

    //private $session = array();

    //_____________________________________________________________________________________________
    public function __construct(){
        $configFactory = ConfigFactory::Instance('testConfig');
        $this->config = $configFactory->config;
        $this->logService = new LogService();
    }


    //_____________________________________________________________________________________________
    public function testLogIntrusionReport(){
        $report = array('description' => 'the testreport');
        $request['get']['email'] = 'testemail';
        try{
            $this->logService->logIntrusionReport($report, $request);
        }catch(\Exception $e) {
            echo $e->getMessage();
        }finally{
        }
        $this->assertEquals('the testreport', $report['description']);
        //echo 'ok;';
    }



}
