<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\application\service\IdsService;
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;

class IdsServiceTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        $this->idsService = new IdsService();
    }

    //_____________________________________________________________________________________________
    public function testIdsDetectNoErrors(){
        $request['get']['id'] = '2';
        $request['get']['node'] = 'root';
        try{
            $this->idsService->idsDetect($request);
        }catch(\Exception $e) {
            echo $e->getMessage();  
        }finally{      
            $this->assertEquals(FALSE, $this->idsService->hasIntrusions());    
        }
    }     
    
    // For our first test of the IDS feature, we simulate an SQL-injection attack
    //_____________________________________________________________________________________________
    public function testIdsDetectErr1(){

        $request['get']['test'] = "'%20OR%201=1--";// "?test='%20OR%201=1--";
        //$request['get']['test'] = '>XXX';// "?test='%20OR%201=1--";
        try{
            $this->idsService->idsDetect($request);
        } catch(GracianIntrusionException $e) {
            $this->assertEquals('ids intrusion detected', $e->getUserMessage());   
        }
    }     
    
    
    //_____________________________________________________________________________________________
    public function testIdsDetectErr2(){

        $request['post']['test'] = 'test {a href="http://www.limonades.org" title="hello een"}';// "?test='%20OR%201=1--";
        //$request['get']['test'] = '>XXX';// "?test='%20OR%201=1--";
        try{
            $this->idsService->idsDetect($request);
        } catch(GracianIntrusionException $e) {   
            //echo '-GracianIntrusionException-'; 
            echo $e->getUserMessage();    
            //print_r($e->getLogMessage());
            $this->assertEquals('ids intrusion detected', $e->getUserMessage());   
        }finally{      
            $this->assertEquals(false, $this->idsService->hasIntrusions());    
        } 
    }       
    
    
  

}
