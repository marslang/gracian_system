<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\application\service\FileService;    
use gracian_system\application\service\TestFileService;
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;      
use gracian_system\domain\exceptions\GracianFileValidationException;


class FileServiceTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        $this->fileService = new FileService();          
        $this->testFileService = new TestFileService();          
        $this->testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';  
    }
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_ok(){         
        $request['files']["fileToUpload"] = array(
                    'name' => 'star.png',
                    'type' => 'image/png',
                    'tmp_name' => $this->testMedia,
                    'error' => '0',
                    'size' => '640'     
        );          
        $subdir = '_aa_test';      
        $oldFilename='';

        try{    
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(\Exception $e) {
            echo $e->getMessage();  
        }finally{       
            $this->assertStringEndsWith($request['files']["fileToUpload"]['name'], $newFilename);
            $this->testFileService->removeTestMediaFile($subdir, $newFilename);   
        }
    }        
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_whitespace(){         
        $request['files']["fileToUpload"] = array(
                    'name' => 'star with whitespace.png',
                    'type' => 'image/png',
                    'tmp_name' => $this->testMedia,
                    'error' => '0',
                    'size' => '640'     
        );          
        $subdir = '_aa_test';      
        $oldFilename='';        
                
        try{       
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(\Exception $e) {
            echo $e->getMessage();  
        }      
        $expected_result = 'star_with_whitespace.png';
        $this->assertStringEndsWith($expected_result, $newFilename);     
        $this->testFileService->removeTestMediaFile($subdir, $newFilename);   
    }          
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_assertFilename(){         
        $request['files']["fileToUpload"] = array(
                    'name' => 'starpng',
                    'type' => 'image/png',
                    'tmp_name' => $this->testMedia,
                    'error' => '0',
                    'size' => '640'     
        );          
        $subdir = '_aa_test';      
        $oldFilename='';

        try{    
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(GracianFileValidationException $e) {        
            $expected_result = 'validation error: filename must contain a dot and an extension';
            $this->assertEquals($expected_result, $e->getUserMessage());    
        }
    }        
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_upload_max_filesize(){     
        $request['files']["fileToUpload"] = array(
                    'name' => 'star.png',
                    'type' => 'image/png',
                    'tmp_name' => $this->testMedia, //'/private/var/tmp/phpIojVAf',
                    'error' => '1',
                    'size' => '640'     
        );
        $subdir = '_aa_test';      
        $oldFilename='';      
        try{    
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(GracianFileValidationException $e) {
            $this->assertEquals('validation errors: DefaultFileValidator: The uploaded file exceeds the upload_max_filesize directive in php.ini', $e->getUserMessage());    
        }
    }     
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_NotAnImage(){     
        $fakeTestMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/fake_image.png';
        $request['files']["fileToUpload"] = array(
                    'name' => 'star.png',
                    'type' => 'image/png',
                    'tmp_name' => $fakeTestMedia, 
                    'error' => '0',
                    'size' => '640'     
        );
        $subdir = '_aa_test';      
        $oldFilename='';    
        try{
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(GracianFileValidationException $e) {
            $this->assertEquals('validation errors: DefaultFileValidator: File is not an image.', $e->getUserMessage());    
        }
    }    
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_CheckFileSize(){     
        $testMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/star.png';
        $request['files']["fileToUpload"] = array(
                    'name' => 'star.png',
                    'type' => 'image/png',
                    'tmp_name' => $this->testMedia, 
                    'error' => '0',
                    'size' => '2094393'     
        );
        $subdir = '_aa_test';      
        $oldFilename='';      
        try{
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(GracianFileValidationException $e) {
            $this->assertEquals('validation errors: DefaultFileValidator: Your file is too large. Max file size is 2MB.', $e->getUserMessage());    
        }
    }     
    
    //_____________________________________________________________________________________________
    public function testFileService_performFileUpload_NotAnImageExtension(){     
        $wrongTestMedia = $this->config->app['gracian_abs_path'] . '/gracian_system/tests/test_media/not_an_image.txt';
        $request['files']["fileToUpload"] = array(
                    'name' => 'not_an_image.txt',
                    'type' => 'text/plain',
                    'tmp_name' => $wrongTestMedia, 
                    'error' => '0',
                    'size' => '640'     
        );
        $subdir = '_aa_test';      
        $oldFilename='';    
        try{
            $newFilename = $this->fileService->performFileUpload($request['files']["fileToUpload"], $subdir, $oldFilename);
        }catch(GracianFileValidationException $e) {
            $this->assertEquals('validation errors: DefaultFileValidator: Only JPG, JPEG, PNG & GIF files are allowed.', $e->getUserMessage());    
        }
    }                                
        
}
         