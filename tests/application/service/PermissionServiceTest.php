<?php

use gracian_project\application\service\ConfigFactory;    
use gracian_project\domain\service\NodeFactory;       
use gracian_system\application\service\TestDbService;
use gracian_system\application\service\PermissionService;    
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianAuthorizationException;


class PermissionServiceTest extends \PHPUnit_Framework_TestCase
{      
    /*        
    what is the permission service (see PermissionService)
    done: test with entity (no item)              
    todo: test with entity->item (no permission)   
    done: test with entity->item['permission] (no or wrong named action)
    done: test with entity->item['permission][action]  = true
    done: test with entity->item['permission][action]  = false
    */
              
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;        
        $this->nodeFactory = new NodeFactory();  
        $this->testDbService = new TestDbService();
        $this->permissionService = new PermissionService();      

        $this->testDbService->truncateTestTable('stree');
        $this->testDbService->addTestStreeRoot();    
        $this->testDbService->addTestStreeChapter();
        $this->testDbService->addTestStreePage();   
        
    }
    
    //_____________________________________________________________________________________________
    public function testAssertPermission_NoErrors(){    
        $session['login'] = Array('id' => '1', 'fullname' => 'Mars','username' => 'su','role' => 'su');   
        $this->config->sessionVo->transformIn($session);       
        $id = 3;
        $node = $this->nodeFactory->getNode('page');  
        $node->fetchItem($id);    
        $this->permissionService->assertPermission($node, 'view') ;    
    }     

    //_____________________________________________________________________________________________
    public function testAssertPermission_NoPermission(){    
        $session['login'] = Array('id' => '0', 'fullname' => 'anonymous','username' => 'anon','role' => 'anon');   
        $this->config->sessionVo->transformIn($session);       
        try{   
            $id = 3;
            $node = $this->nodeFactory->getNode('page');  
            $node->fetchItem($id);    
            $this->permissionService->assertPermission($node, 'view') ;    
        }catch(GracianAuthorizationException $e){
            $this->assertEquals('AuthorizationException:You have no permission to view this item (view/page/3)',$e->getUserMessage());  
        }
    }     
    
    //_____________________________________________________________________________________________
    public function testAssertPermission_NoItemSet(){    
        $session['login'] = Array('id' => '0', 'fullname' => 'anonymous','username' => 'anon','role' => 'anon');   
        $this->config->sessionVo->transformIn($session);       
        try{   
            $id = 3;
            $node = $this->nodeFactory->getNode('page');  
            $this->permissionService->assertPermission($node, 'view') ;    
        }catch(GracianException $e){
            $this->assertEquals('assert_permission: entity->item not found.',$e->getUserMessage());  
        }
    }     
    

            
    
    
}
         