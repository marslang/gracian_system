<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\application\service\IntrusionService;
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;      
use gracian_system\domain\exceptions\GracianFileValidationException;


class IntrusionServiceTest extends \PHPUnit_Framework_TestCase
{
    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        $this->intrusionService = new IntrusionService();
    }

    //_____________________________________________________________________________________________
    public function testSanitizeValidateValue(){
        try{
            $id = $this->intrusionService->sanitizeValidateValue('id', '2a');    
            echo '-id = ' . $id;
        }catch(GracianIntrusionException $e) {    
            //echo $e->getUserMessage();
            $this->assertEquals('Intrusion error: id: Id must be an integer (0-9)', $e->getUserMessage());   
        }  
        //$nodeName = $this->intrusionService->sanitizeValidateValue('node', $this->requestBag->request['get']['node']);

    }   
             
}
         