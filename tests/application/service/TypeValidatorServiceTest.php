<?php

use gracian_project\application\service\ConfigFactory;
use gracian_system\application\service\TypeValidatorService;
/*
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;
*/



class TypeValidatorServiceTest extends \PHPUnit_Framework_TestCase
{


    //_____________________________________________________________________________________________
    public function __construct(){
        $this->config = ConfigFactory::Instance('testConfig')->config;
        $this->typeValidatorService = new TypeValidatorService();
    }


    //_____________________________________________________________________________________________
    public function testIsInteger(){
        try{
            $this->assertEquals(TRUE, $this->typeValidatorService->isInteger(0));
            $this->assertEquals(TRUE, $this->typeValidatorService->isInteger(1));
            $this->assertEquals(TRUE, $this->typeValidatorService->isInteger(2));      
            $this->assertEquals(TRUE, $this->typeValidatorService->isInteger(1234567890123456789)); 

            $this->assertEquals(FALSE, $this->typeValidatorService->isInteger(-1));
            $this->assertEquals(FALSE, $this->typeValidatorService->isInteger(12345678901234567890)); 
            $this->assertEquals(FALSE, $this->typeValidatorService->isInteger('a'));    
            
        }catch(\Exception $e) {
            echo $e->getMessage();
        }
     
    }      
    
    //_____________________________________________________________________________________________    
    /*
    * alleen ascii A-Z a-z _
    * geen dash, geen unicode  , geen cijfers
    * TODO: waar gebruik je deze voor?
    */    
    public function testIsString(){
        try{
            $this->assertEquals(TRUE, $this->typeValidatorService->isString('a'));  
            $this->assertEquals(TRUE, $this->typeValidatorService->isString('A'));
            $this->assertEquals(TRUE, $this->typeValidatorService->isString('_'));   
            
            $this->assertEquals(FALSE, $this->typeValidatorService->isString('0'));
            $this->assertEquals(FALSE, $this->typeValidatorService->isString('é'));            
            $this->assertEquals(FALSE, $this->typeValidatorService->isString('A-'));
            
        }catch(\Exception $e) {
            echo $e->getMessage();
        }
     
    }  



}
