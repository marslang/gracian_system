<?php

/*
* SYNTAX: assertEquals(mixed $expected, mixed $actual[, string $message = ''])
* COMMAND: phpunit application/usecase/user
*/


//use gracian_system\application\usecase\stree\ShowStreeUsecase;
use gracian_system\application\service\TestDbService;

use gracian_system\application\usecase\admin\user\ViewUserUsecase;
use gracian_system\application\usecase\admin\user\CreateUserUsecase;
use gracian_system\application\usecase\admin\user\StoreUserUsecase;
use gracian_system\application\usecase\admin\user\EditUserUsecase;
use gracian_system\application\usecase\admin\user\UpdateUserUsecase;
use gracian_system\application\usecase\admin\user\DeleteUserUsecase;
use gracian_system\application\usecase\admin\user\DestroyUserUsecase;
use gracian_system\application\usecase\admin\authentication\ValidateLoginUsecase;

use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;

use gracian_project\application\service\ConfigFactory;
//use gracian_project\domain\service\NodeFactory;
//use gracian_project\application\service\UsecaseFactory;




class UserCrudTest extends \PHPUnit_Framework_TestCase
{

    private $session = array();


    //_____________________________________________________________________________________________
    public function __construct(){

		//$configFactory = ConfigFactory::Instance('testConfig');
		//$this->config = $configFactory->config;       
		

        //$this->config->sessionVo->transformIn($httpRequest->session());
        //$this->nodeFactory = new NodeFactory();
        //$this->usecaseFactory = new UsecaseFactory();
        //$this->config->sessionVo->transformIn($httpRequest->session());

		$this->config = ConfigFactory::Instance('testConfig')->config;
        $this->testDbService = new TestDbService();
        $this->session['login'] = Array('id' => '1', 'fullname' => 'Mars','username' => 'su','role' => 'su');    
        //$this->session['login'] = Array('id' => '0', 'fullname' => 'anonymous','username' => 'anon','role' => 'anon');    
        //$this->config->sessionVo->transformIn($this->session);      

    }


    //_____________________________________________________________________________________________
    public function testAll() {

        $this->testDbService->truncateTestTable('user');
        $this->testDbService->addTestSuperUser();
        $this->viewUserAsAnon();
        $this->loginUser();
        $this->viewUser();
        $this->createUser();
        $uid = $this->storeUserWithUsernameDoublure();
        $user_id = $this->storeUser();
        $this->editUser($user_id);
        $this->updateUser($user_id) ;
        $this->viewUpdatedUser($user_id);   
        $this->updateInvalidPassword($user_id);    
        $this->updatePassword($user_id); 
        $this->loginUserWithNewPassword();
        
        $this->updateUserWithUsernameDoublure($user_id);
        $this->updateUserWithEmailDoublure($user_id);
        $this->deleteUser($user_id);
        $this->destroyUser($user_id);
        $this->viewRemovedUser($user_id);
        /*
        */
        // change password
    }  
    

    /**     7) Test an admin page url without login to application:
    */
    public function viewUserAsAnon() {
        ////unset($responseBag->get('flashMessages'));   
        $session['login'] = Array('id' => '0', 'fullname' => 'anonymous','username' => 'anon','role' => 'anon');    
        $this->config->sessionVo->transformIn($session);
        //print_r($this->session); exit();
        $request['get']['id'] = $this->testDbService->userItem['id'];
        //$request['get']['node'] = $nodeName = 'root';
        //try{

        $usecase = new ViewUserUsecase(); //$this->usecaseFactory->getUsecase($nodeName, 'view');
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();    
        //}catch(GracianAccessException $e){
        
        //$this->assertEquals('AuthorizationException:You have no permission to view this item (view/user/1)', $responseBag->get('flashMessages')['error'][0]);
        $this->assertEquals('You have no permission to view this page (not logged in).', $responseBag->get('flashMessages')['error'][0]);
        //}
        //print_r($response); exit();
        //$this->assertEquals($response->userEntity->item['id']        , $request['get']['id'] );
        //$this->assertEquals($response->userEntity->item['email'] , $this->testDbService->userItem['email']);

        //$this->config->sessionVo->transformOut($this->session);

//exit();
    }

    public function loginUser() {       
        
        // $session['login'] = Array('id' => '0', 'fullname' => 'anonymous','username' => 'anon','role' => 'anon');    
        $this->config->sessionVo->transformIn($this->session);    
    }

    public function viewUser() {
        ////unset($responseBag->get('flashMessages'));
        //$this->config->sessionVo->transformIn($this->session);
        //print_r($this->session); exit();
        $request['get']['id'] = $this->testDbService->userItem['id'];
        //$request['get']['node'] = $nodeName = 'root';
        $usecase = new ViewUserUsecase(); //$this->usecaseFactory->getUsecase($nodeName, 'view');
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();    
        //print_r($response); exit();
        $this->assertEquals($request['get']['id'], $response->userEntity->item['id'] );
        $this->assertEquals($this->testDbService->userItem['email'], $response->userEntity->item['email'] );

        //$this->config->sessionVo->transformOut($this->session);

//exit();
    }


    public function createUser() {
        //$this->config->sessionVo->transformIn($this->session);
        ////unset($responseBag->get('flashMessages'));

        $request['get'] = array();//['parentId'] = 1;
        //$request['get']['node'] = $nodeName = 'chapter';
        //$request['post']  = $this->chapterItem;
        //$usecase = $this->usecaseFactory->getUsecase($nodeName, 'create');
        $usecase = new CreateUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();        
        
        $this->assertEquals( '1', $response->userEntity->item['use_expire_date'] );
        //$this->config->sessionVo->transformOut($this->session);
    }

    public function storeUser() {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));
        $request['post']  = Array(
                    //'id' => 1,
                    'username' => 'admin27',
                    'fullname' => 'gemma alba',
                    'role' => 'admin',
                    'email' => 'langenberg@antenna-men.com',
                    'password' => 'carsonQW12!@'  ,
                    'remember_token' => '',
                    'creation_date' => '2015-01-31 09:39:32',
                    'modify_date' => '2015-01-31 11:48:57',
                    'expire_date' => '2015-01-31 11:48:57',
                    'use_expire_date' => 1
                );
        $usecase = new StoreUserUsecase();
        $responseBag =  $usecase->execute($request);   
        $response = $responseBag->transformOut();
        $this->assertEquals(4, $response->new_id);
        $this->assertEquals('Successfully stored the user.', $responseBag->get('flashMessages')['success'][0]);
        return $response->new_id;
    }

    /* store a new user: if you enter a username that is already taken, you get a warning */
    public function storeUserWithUsernameDoublure() {
            //$this->config->sessionVo->transformIn($this->session);
            //unset($responseBag->get('flashMessages'));
            $request['post']  = Array(
                        //'id' => 1,
                        'username' => 'su',
                        'fullname' => 'gina mona',
                        'role' => 'admin',
                        'email' => 'test@antenna-men.com',
                        'password' => 'carsonQW12!@'  ,
                        'remember_token' => '',
                        'creation_date' => '2015-01-31 09:39:32',
                        'modify_date' => '2015-01-31 11:48:57',
                        'expire_date' => '2015-01-31 11:48:57',
                        'use_expire_date' => 0
                    );
                $usecase = new StoreUserUsecase();
                $responseBag =  $usecase->execute($request);
                $response = $responseBag->transformOut();   
                //print_r($response);    
                $this->assertEquals( 'UserEntity: Cannot store user, username is already taken.', $responseBag->get('flashMessages')['error'][0] );


        }
        /*
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));
        $request['get']['id'] = $user_id;
        $request['post']  = Array(
            'username' => 'su',
            'email' => 'langenberg2@antenna-men.com',
            'fullname' => 'gemma alba',
            'role' => 'admin',
            'expire_date' => '2015-01-31 11:48:57',
            'password' => ''
        );
        try{
            $usecase = new UpdateUserUsecase();
            $responseBag =  $usecase->execute($request);
            $response = $responseBag->transformOut();
        } catch(GracianException $e) {
            $this->assertEquals($e->getUserMessage() , 'UserEntity: Cannot update user, username is already taken.' );
        }
        //$this->config->sessionVo->transformOut($this->session);
    }    */

    public function editUser($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $user_id;
        //$request['get']['node'] = $nodeName = 'chapter';
        //$request['post']  = $this->chapterItem;
        //$usecase = $this->usecaseFactory->getUsecase($nodeName, 'edit');
        $usecase = new EditUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();        
        
        //print_r($response); exit();
        $this->assertEquals($request['get']['id'], $response->userEntity->item['id'] );
        $this->assertEquals('1',  $response->userEntity->item['use_expire_date']);
        //$this->config->sessionVo->transformOut($this->session);

    }

    public function updateUser($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $user_id;
        $request['post']  = Array(
            'username' => 'admin2',
            'email' => 'langenberg2@antenna-men.com',
            'fullname' => 'gemma alba',
            'role' => 'admin',
            'expire_date' => '2015-01-31 11:48:57',
            'password' => ''
        );
        $usecase = new UpdateUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();      
        $this->assertEquals('Successfully updated the user.', $responseBag->get('flashMessages')['success'][0]);              
        //print_r($response); exit();
        //$this->assertEquals($response->new_id , 2 );
        //$this->assertEquals($response->sta tus  , 'success');
        //$this->assertEquals($this->config->sessionVo->flashStage['sta tus']  , 'success');

        //return $response->new_id;
        //$this->config->sessionVo->transformOut($this->session);
    }



    /* update an existing user: if you enter a username that is already taken, you get a warning */
    public function updateUserWithUsernameDoublure($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));
        $request['get']['id'] = $user_id;
        $request['post']  = Array(
            'username' => 'su',
            'email' => 'langenberg2@antenna-men.com',       
            //'email' => 'marcel@limonades.org',
            'fullname' => 'gemma alba',
            'role' => 'admin',
            'expire_date' => '2015-01-31 11:48:57',
            'password' => ''
        );
            $usecase = new UpdateUserUsecase();
            $responseBag =  $usecase->execute($request);
            $response = $responseBag->transformOut();     
            $this->assertEquals('UserEntity: Cannot update user, username is already taken.', $responseBag->get('flashMessages')['error'][0]);
        //$this->config->sessionVo->transformOut($this->session);
    }

    /* update an existing user: if you enter a username that is already taken, you get a warning */
    public function updateUserWithEmailDoublure($user_id) {        
        //echo 'updateUserWithEmailDoublure';
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));
        $request['get']['id'] = $user_id;
        $request['post']  = Array(
            'username' => 'admin3',
            'email' => 'marcel@limonades.org',
            'fullname' => 'gemma alba',
            'role' => 'admin',
            'expire_date' => '2015-01-31 11:48:57',
            'password' => ''
        );
        //try{
            $usecase = new UpdateUserUsecase();
            $responseBag =  $usecase->execute($request);
            $response = $responseBag->transformOut();
 
        //} catch(GracianValidationException $e) {
            $this->assertEquals('This email address is already taken', $responseBag->get('flashMessages')['error'][0] );
        //}
        //$this->config->sessionVo->transformOut($this->session);
    }      
    
    
    public function viewUpdatedUser($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $user_id;
        $usecase = new ViewUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
         
        $this->assertEquals( $user_id , $response->userEntity->item['id'] );
        $this->assertEquals('langenberg2@antenna-men.com', $response->userEntity->item['email']);
        //$this->assertGreaterThan(expected,  actual, message);
        $this->assertGreaterThan(59,  strlen($response->userEntity->item['password']));
        //$this->config->sessionVo->transformOut($this->session);
    }
                                       
     
    public function updateInvalidPassword($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        // get old pw
        $request['get']['id'] = $user_id;
        $usecase = new ViewUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        $old_pw = $response->userEntity->item['password'];
        // set new pw
        $request['post']  = Array(
            'username' => $response->userEntity->item['username'], //'username' => 'admin2',
            'email' => 'langenberg2@antenna-men.com',
            'password' => 'theupdatedInvalidPassword',
            'fullname' => 'gemma alba',
            'role' => 'admin',
            'expire_date' => '2015-01-31 11:48:57',
            'use_expire_date' => 'on'
        );
        $usecase = new UpdateUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($response); exit();        
        //$this->assertEquals('Successfully updated the user.', $responseBag->get('flashMessages')['success'][0] );
        //$this->assertEquals($response->new_id , 2 );
        //$this->assertEquals($response->sta tus  , 'success');       
        $expected_result = 'Password must contain upper and lower case letters, numbers and special characters (at least 2 of each), cannot contain spaces and must be between 8 and 72 characters.';
        $this->assertEquals($expected_result,  $response->validationErrors['password'][0]);  
        //$this->assertNotEquals($old_pw , $response->userEntity->item['password']);
        //$this->config->sessionVo->transformOut($this->session);
    }   
    
    public function updatePassword($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        // get old pw
        $request['get']['id'] = $user_id;
        $usecase = new ViewUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        $old_pw = $response->userEntity->item['password'];
        // set new pw
        $request['post']  = Array(
            'username' => $response->userEntity->item['username'], //'username' => 'admin2',
            'email' => 'langenberg2@antenna-men.com',
            'password' => 'theupdatedpassword12qw!@QW',
            'fullname' => 'gemma alba',
            'role' => 'admin',
            'expire_date' => '2015-01-31 11:48:57',
            'use_expire_date' => '0'   // dit is een  checkbox , als 0 dan uit-commentarieren ()
        );
        $usecase = new UpdateUserUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($response); exit();        
        $this->assertEquals('Successfully updated the user.', $responseBag->get('flashMessages')['success'][0] );

        //$this->assertEquals($response->new_id , 2 );
        //$this->assertEquals($response->sta tus  , 'success');     
        //$this->assertEquals('password The password must contain upper and lower case letters, numbers and special characters (at least 2 of each), cannot contain spaces and must be between 8 and 72 characters.',  $response->validationErrors['password']);  
        //$this->assertNotEquals($old_pw , $response->userEntity->item['password']);
        //$this->config->sessionVo->transformOut($this->session);
    }    
    
    public function loginUserWithNewPassword() {       
        
        // $session['login'] = Array('id' => '0', 'fullname' => 'anonymous','username' => 'anon','role' => 'anon');    
        //$this->config->sessionVo->transformIn($this->session);    
        
        ////unset($responseBag->get('flashMessages'));
        $request['post']['email'] = 'langenberg2@antenna-men.com';
        $request['post']['password'] = 'theupdatedpassword12qw!@QW';
        $usecase = new ValidateLoginUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();   
        //print_r($responseBag); exit(); 
        //$this->assertEquals($response->messages['success'][0] , 'User logged in successfully.' );
        $this->assertEquals('User logged in successfully.', $responseBag->get('flashMessages')['success'][0]);
        $this->assertEquals('langenberg2@antenna-men.com', $responseBag->get('login')['email']  );
        $this->assertEquals('0', $responseBag->get('login')['use_expire_date']  );
        
    }      

    public function deleteUser($user_id) {     
        $this->config->sessionVo->transformIn($this->session);  
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $user_id;
        //$request['get']['node'] = $nodeName = 'chapter';
        //$request['post']  = $this->chapterItem;
        //$usecase = $this->usecaseFactory->getUsecase($nodeName, 'edit');
        $usecase = new DeleteUserUsecase();
        $responseBag = $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($response); exit();
        $this->assertEquals($request['get']['id'], $response->userEntity->item['id'] );
        //$this->assertEquals($response->userEntity->item['use_expire_date'], '0' );
        //$this->config->sessionVo->transformOut($this->session);
    }


    public function destroyUser($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));
        $request['post']['id'] = $user_id;
        //$usecase = $this->usecaseFactory->getUsecase($nodeName, 'destroy');
        $usecase = new DestroyUserUsecase();
        $responseBag = $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($response); exit();
        //$this->assertEquals($response->sta tus, 'success');

        //$this->assertEquals($response->messages['success'][0], 'Successfully deleted the user.');
        $this->assertEquals('Successfully deleted the user.', $responseBag->get('flashMessages')['success'][0]);
        //print_r($responseBag->get('flashMessages')); exit();

        $this->assertEquals('/admin/user/index', $responseBag->get('redirect') );
        //$this->config->sessionVo->transformOut($this->session);
    }


    public function viewRemovedUser($user_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $user_id;
        $usecase = new ViewUserUsecase();
        //try{
          $responseBag =  $usecase->execute($request);
          $response = $responseBag->transformOut();    
          //print_r($response); exit();
        //} catch (\Exception $e) {
            //echo $e->getMessage();
            $this->assertEquals('No user found with this id', $responseBag->get('flashMessages')['error'][0] );
        //}
        //$this->config->sessionVo->transformOut($this->session);
    }



/*
EXCEPTIONS:
- try delete item with children
*/

// try adding a user with the same name



}
?>
