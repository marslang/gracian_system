<?php  

/*
* SYNTAX: assertEquals(mixed $expected, mixed $actual[, string $message = ''])
* COMMAND: phpunit application/usecase/authentication
*/      

use gracian_system\application\service\TestDbService;
use gracian_project\application\service\ConfigFactory;
use gracian_system\application\usecase\admin\authentication\ValidateLoginUsecase;
use gracian_system\application\usecase\admin\authentication\ValidateLoginunUsecase;
use gracian_system\application\usecase\admin\authentication\LogoutUsecase;
use gracian_system\application\usecase\admin\user\UpdateUserUsecase;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianAuthenticationException;

class ValidateLoginUsecaseTest extends \PHPUnit_Framework_TestCase
{

    private $session = array();

    //_____________________________________________________________________________________________
    public function __construct(){
		$configFactory = ConfigFactory::Instance('testConfig');
		$this->config = $configFactory->config;
        $this->testDbService = new TestDbService();
        $this->testDbService->truncateTestTable('user');
        $this->testDbService->addTestSuperUser();
    }


    //_____________________________________________________________________________________________
    public function testAll() {
        //$this->loginUserCorrectEmailPw();
        //$this->logoutUser();
        //$this->loginUserCorrectEmailPw();
        //$this->loginUserActiveNotExpired();
    }



    /** 1) Test with correct username and password: This is the most basic positive test cases, while using this user should successfully logged in.If correct username and password is not helping you to login in to your application then file a bug because this shows that something is wrong with application.
    */

    public function testLoginUserCorrectUnPw() {
        //echo 'loginUserCorrectEmailPw';
        //$this->config->sessionVo->transformIn($this->session);
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['username'] = 'su';
        $request['post']['password'] = 'carsonQW12!@';
//        try{
            $usecase = new ValidateLoginunUsecase();
            $responseBag =  $usecase->execute($request);
            $response = $responseBag->transformOut();
            //$this->assertEquals($response->messages['success'][0] , 'User logged in successfully.' );
            //print_r($response);
            //print_r($this->config->sessionVo->flashStage);
            $this->assertEquals(
            'User logged in successfully.',
            $responseBag->get('flashMessages')['success'][0] 
            // $this->config->sessionVo->flashStage['messages']['success'][0]  
            );
            
            $this->assertEquals(
            'marcel@limonades.org', 
            $responseBag->get('login')['email'] 
            //$this->config->sessionVo->putStage['login']['email']   
            );
            
            $this->assertEquals(
             '0', 
            $responseBag->get('login')['use_expire_date']   
            //$this->config->sessionVo->putStage['login']['use_expire_date']   
            );
            //$this->config->sessionVo->transformOut($this->session);
            //print_r($this->session);
//        } catch(GracianAuthenticationException $e) {
//            $this->assertEquals($this->config->sessionVo->flashStage['messages']['error'][0], 'Username cannot be empty');
//        }

    }

    /** 1) Test with correct username and password: This is the most basic positive test cases, while using this user should successfully logged in.If correct username and password is not helping you to login in to your application then file a bug because this shows that something is wrong with application.
    */
    public function testLoginUserEmptyUnCorrectPw() {
        $request['post']['username'] = '';
        $request['post']['password'] = 'carsonQW12!@';
        $usecase = new ValidateLoginunUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();             
        //print_r($response);
        $this->assertEquals('Intrusion error: username: Username cannot be empty', $responseBag->get('flashMessages')['error'][0]);

    }






/** 1) Test with correct email and password: This is the most basic positive test cases, while using this user should successfully logged in.If correct username and password is not helping you to login in to your application then file a bug because this shows that something is wrong with application.
*/
public function testLoginUserCorrectEmailPw() {
    //echo 'loginUserCorrectEmailPw';
    //$this->config->sessionVo->transformIn($this->session);
    //unset($this->config->sessionVo->flashStage['messages']);
    $request['post']['email'] = 'marcel@limonades.org';
    $request['post']['password'] = 'carsonQW12!@';
    $usecase = new ValidateLoginUsecase();
    $responseBag =  $usecase->execute($request);
    $response = $responseBag->transformOut();

    $this->assertEquals('User logged in successfully.', $responseBag->get('flashMessages')['success'][0]);
    $this->assertEquals('marcel@limonades.org', $responseBag->get('login')['email'] );
    $this->assertEquals('0' , $responseBag->get('login')['use_expire_date']);

}


    /** 2) Test with incorrect username or password: access should be denied
    (wrong email, right password)
    */
    public function testLoginUserWrongEmailCorrectPw() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'wrongemail@limonades.org';
        $request['post']['password'] = 'carsonQW12!@';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);   
        //} catch(GracianException $e) {
             //$this->assertEquals( $e->getUserMessage() , 'E-mail or password is invalid');
             $this->assertEquals('E-mail or password is invalid', $responseBag->get('flashMessages')['info'][0]);
        //}
    }
    // (right email, wrong password)
    public function testLoginUserCorrectEmailWrongPw() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marcel@limonades.org';
        $request['post']['password'] = 'wrongPW';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);
        //} catch(GracianException $e) {
             //$this->assertEquals( $e->getUserMessage() , 'E-mail or password is invalid');
             $this->assertEquals('E-mail or password is invalid', $responseBag->get('flashMessages')['info'][0]);

        //}
    }

    /**     3) Test with correct username and empty password:  In this case when user click on login button then a message should flash that says “Enter a password or something unexpected went wrong”
    */
    public function testLoginUserCorrectEmailEmptyPw() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marcel@limonades.org';
        $request['post']['password'] = '';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);
        //} catch(GracianException $e) {
            //print_r($this->config->sessionVo->flashStage);
            //$this->assertEquals( $e->getUserMessage() , 'E-mail or password is invalid');
            $this->assertEquals('Intrusion error: password: Password cannot be empty', $responseBag->get('flashMessages')['error'][0]);
        //}
    }

    /**      4) Test with empty username and correct password: Again an error message should appear to enter a valid email or username
    */
    public function testLoginUserEmptyEmailCorrectPw() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = '';
        $request['post']['password'] = 'carsonQW12!@';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);
        //} catch(GracianException $e) {
            $this->assertEquals('Intrusion error: email: E-mail cannot be empty', $responseBag->get('flashMessages')['error'][0]);
        //}
    }

    /** 5a) Verify the correct error messages like  Incorrect combination of user name and password. If you are getting anything like Incorrect username or Incorrect password then be conscious because you application is giving half the information to hacker and your application is in great danger.
        5b) For security point of view, in case of incorrect credentials user is displayed the message like "incorrect username or password" instead of exact message pointing at the field that is incorrect. As message like "incorrect username" will aid hacker in bruteforcing the fields one by one
    */
    public function testLoginUserWrongEmailWrongPw() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'wrongemai@limonades.org';
        $request['post']['password'] = 'wrongPW';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);
        //} catch(GracianException $e) {
            //print_r($this->config->sessionVo->flashStage);
            //$this->assertEquals( $e->getUserMessage() , 'E-mail or password is invalid');
            $this->assertEquals('E-mail or password is invalid', $responseBag->get('flashMessages')['info'][0]);
        //}
    }

    /** 13) Verify that validation message is displayed in case user exceeds the character limit of the user name and password fields
        password too lon
        */
    public function testLoginUserPasswordTooLong() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marie@limonades.org';
        $request['post']['password'] = 'carsonQW12!@ijochvuziuviuvhiushvuzsvohvohsohvohgjgufttTfgRD5hjUYbHjjgGdEKLmnhGcdchJnkIhT5edrt5tvBjkLmnHgvVVVshvosvohvusuovhuosvudvusvousvusvusdhvvosdhgugjgjhdgfdfddtdydbbdbddddjjddhdbddhdhddjdkkdkdijdhdysyhssksodiffbdfydudjdjdjdkdidduucdhduddyhddhdbbddbdndjddhdhdhdddhhdjdjdjdjdjdhdhdhhddjdjdjdjdjddhdydhdbdgdhvousvohhfufjfyffhfj';
        $usecase = new ValidateLoginUsecase();
        $responseBag =  $usecase->execute($request);
        $this->assertEquals('Intrusion error: password: Password cannot be more than 72 characters', $responseBag->get('flashMessages')['error'][0]);
    }

    /** email too long */
    public function testLoginUserEmailTooLong() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marie@limonadesabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz.org';
        $request['post']['password'] = 'carsonQW12!@';
        $usecase = new ValidateLoginUsecase();
        $responseBag =  $usecase->execute($request);
        $this->assertEquals('Intrusion error: email: E-mail cannot be more than 64 characters', $responseBag->get('flashMessages')['error'][0]);
    }

    /** email cannot contain spaces */
    public function testLoginUserEmailContainsSpaces() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marie j@limonades.org';
        $request['post']['password'] = 'carsonQW12!@';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);
        //} catch(GracianAuthenticationException  $e) {
            $this->assertEquals('E-mail or password is invalid', $responseBag->get('flashMessages')['info'][0]);
        //} catch(GracianIntrusionException  $e) {
        //    $this->assertEquals('E-mail cannot contain spaces', $responseBag->get('flashMessages')['error'][0]);
        //}
    }




    /** password cannot contain spaces */
    public function testLoginUserPasswordContainsSpaces() {
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marie@limonades.org';
        $request['post']['password'] = 'carson QW12!@';
        $usecase = new ValidateLoginUsecase();
        //try{
            $responseBag =  $usecase->execute($request);
        //} catch(GracianIntrusionException  $e) {
            $this->assertEquals('Intrusion error: password: Password cannot contain spaces', $responseBag->get('flashMessages')['error'][0]);
        //}
    }




    public function testLoginUserIgnoreExpireDate() {
        // use_expire_date = 0 , expire = 2015
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marcella@limonades.org';
        $request['post']['password'] = 'carsonQW12!@';
        //try{
            $usecase = new ValidateLoginUsecase();
            $responseBag =  $usecase->execute($request);
            $response = $responseBag->transformOut();
        //}catch(GracianException $e){
            $this->assertEquals('User logged in successfully.', $responseBag->get('flashMessages')['success'][0] );
        //}
        //$this->assertEquals($response->messages['success'][0] , 'User logged in successfully.' );
        //print_r($response);
        //print_r($this->config->sessionVo->flashStage);
        /* $this->assertEquals($this->config->sessionVo->flashStage['messages']['success'][0], 'User logged in successfully.');
        $this->assertEquals($this->config->sessionVo->putStage['login']['email']      , 'marcel@limonades.org' );
        $this->assertEquals($this->config->sessionVo->putStage['login']['use_expire_date']        , '1' );
        */
    }

    public function testLoginUserUseExpireDate() {
        // use_expire_date = 1 , expire = jan 2015
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['email'] = 'marcelino@limonades.org';
        $request['post']['password'] = 'carsonQW12!@';
        //try{
            $usecase = new ValidateLoginUsecase();
            $responseBag =  $usecase->execute($request);
            $response = $responseBag->transformOut();
            //print_r($responseBag->get('flashMessages')); exit();    
            //print_r($response);
        //}catch(GracianException $e){
            $this->assertEquals('User found but activity is expired', $responseBag->get('flashMessages')['info'][0] );
        //}
        //$this->assertEquals($response->messages['success'][0] , 'User logged in successfully.' );
        //print_r($response);
        //print_r($this->config->sessionVo->flashStage);
        /* $this->assertEquals($this->config->sessionVo->flashStage['messages']['success'][0], 'User logged in successfully.');
        $this->assertEquals($this->config->sessionVo->putStage['login']['email']      , 'marcel@limonades.org' );
        $this->assertEquals($this->config->sessionVo->putStage['login']['use_expire_date']        , '1' );
        */
    }


    /** 20) Verify that once logged in, clicking back button doesn't logout user
    */


    //__________________________________________________________________________
/*



    9) Verify https in url for login page:  S with Http mean secure http. If login is associated with http in url means you information to login in to application is not secure and anyone can access your information just by doing small effort. While HTTPS ensure encryption of information that is being sent to server from client end.
    10) Verify ID in url while processing your request :  keep track on ID associated with your request url and ID associated with request url should be dynamic not static otherwise this may help some hacker to nab your information.


// test manually
    6) Verify that back button is not able to push you to your logged in page just after you logout from your specific account:  This kind of test cases invoke the flaw associated with Session management. When session is not closed just after your log-out means any one can access you account if you have opened your specific login enabled account at any time just by clicking Back button in Browser.So one way to save your account from such misconduct is to close browser when ever you log-out from your account.
    8a) Verify the session timeout: This is most important test case for any finance related site.Session should time out if user is inactive for few minute. This is normally a sustainability test of session. If your application is not prompting for session timeout then think once this may be issue.
    8b) Verify the timeout of the login session
    11) Verify deletion of ID while browsing : Go to the place where cookies are saved and try to delete cookies when you are just browsing your account and try to find out the cookies that have your username and password because as soon as you delete that cookies you should be reached to login page. If you find the same cookies then try to change the numbers in cookies and should verify what is happening, hopefully corrupt cookies that have your real id should redirect you on login page once again even you haven’t deleted the cookies.
    12) Try to login when your cookies are disabled
    14) Verify that there is reset button to clear the field's text
    15) Verify if there is checkbox with label "remember password" in the login page
    19) Verify if the password can be copy-pasted or not
    16) Verify that the password is in encrypted form when entered


    21) Verify if XSS vulnerability work on login page


    22b) Verify if SQL Injection attacks works on login page
    22a) Check SQL injection: most devastating vulnerabilities to  impact a business, as it can lead to exposure of all of the sensitive information stored in an application’s database, including handy information such as usernames, passwords, names, addresses, phone numbers, and credit card details.
    So I would suggest including this test cases if you are going to test some banking and insurance related application.
    Most common SQL injection that is used  or ‘1’=’1, if this got executed then be ready for the loss of your important information. If means hacker can login without any problem to system or application.

    23b) Verify that there is limit on the total number of unsuccessful attempts
    23a) Verify account lock out: I would like to include this test case with priority, if user is using 3 or some specific number of time a wrong password then his/her account should be locked out and access should be allowed after certain assurance form filling or by calling customer care. This may help user from hackers hand.
    Verify simultaneous login to application on different browser: I think you all would be familiar with this in daily life if you would have used railway ticket booking site.
    Try some hit and trial username and password : before deploying application, username and password like Admin:Admin , Guest:Guest, some username :password, author:author  should be use to test but should be denied when application is deployed.

    */
}
?>
