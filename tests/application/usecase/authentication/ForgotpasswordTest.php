<?php  

/*
* SYNTAX: assertEquals(mixed $expected, mixed $actual[, string $message = ''])
* COMMAND: phpunit application/usecase/authentication
*/      

use gracian_system\application\service\TestDbService;
use gracian_project\application\service\ConfigFactory;
use gracian_system\application\usecase\admin\authentication\ForgotpasswordSubmitUsecase;    
use gracian_system\application\usecase\admin\authentication\ChangepasswordUsecase;
use gracian_system\application\usecase\admin\authentication\UpdatepasswordUsecase;
//use gracian_system\application\usecase\admin\user\UpdateUserUsecase;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianAuthenticationException;

class ForgotpasswordTest extends \PHPUnit_Framework_TestCase
{

    private $session = array();

    //_____________________________________________________________________________________________
    public function __construct(){
		$configFactory = ConfigFactory::Instance('testConfig');
		$this->config = $configFactory->config;
        $this->testDbService = new TestDbService();
        $this->testDbService->truncateTestTable('user');
        $this->testDbService->addTestSuperUser();
    }


    //_____________________________________________________________________________________________
    public function testAll() {
        $this->forgotPwSubmit();
        //$this->logoutUser();
        //$this->loginUserCorrectEmailPw();
        //$this->loginUserActiveNotExpired();
    }


    /*
    public function testUpdatepassword_noTokenFound() {
        //echo 'loginUserCorrectEmailPw';
        //$this->config->sessionVo->transformIn($this->session);
        //unset($this->config->sessionVo->flashStage['messages']);
        $request['post']['username'] = 'su';    
        $request['post']['password'] = 'carsonQW12!@';
        $request['post']['forgotpassword_token'] = 'qwertyuiop123456789';

        $usecase = new UpdatepasswordUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($responseBag);    

        $expected_result = 'No record found with this token. Go to the login page and follow the forgot passsword process again.';
        $this->assertEquals($expected_result, $responseBag->get('flashMessages')['error'][0] );
    }    
    */

    public function forgotPwSubmit() {
        $request['post']['email'] = 'marcel@limonades.org';    
        $usecase = new ForgotpasswordSubmitUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($responseBag);     
        $token = $responseBag->get('token');        
        /*
        NOTE: no message returned. a hacker can use it. you can log it (in the use case)
        */                                                                             
        
        // --- changepassword ---
        
        $request['get']['hashString'] = $token;         
        $usecase = new ChangepasswordUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($responseBag);    
        
        $email = $responseBag->get('email');  
        $token = $responseBag->get('token');  
 
                                               
        // --- UpdatepasswordUsecase ---      
        $request['post']['email'] = $email;    
        $request['post']['forgotpassword_token'] = $token;           
        
        // try with weak password  : error
        $request['post']['password'] = 'zxc';    
        $usecase = new UpdatepasswordUsecase();
        $responseBag =  $usecase->execute($request);
        //print_r($responseBag->response);     
        $expected_result =  array('password' => array('Password must contain upper and lower case letters, numbers and special characters (at least 2 of each), cannot contain spaces and must be between 8 and 72 characters.'));    
        $this->assertEquals($expected_result, (array) json_decode($responseBag->response['flashMessages']['error'][0]) );         
        
        // try with strong password  : success
        $request['post']['password'] = 'zxc!@#ZXC123';      
        $usecase = new UpdatepasswordUsecase();     
        $responseBag =  $usecase->execute($request);
        $this->assertEquals('Successfully changed the password.', $responseBag->response['flashMessages']['success'][0] );  
        //$expected_result = 'No record found with this token. Go to the login page and follow the forgot passsword process again.';
        //$this->assertEquals($expected_result, $responseBag->get('flashMessages')['error'][0] );      
        
        // try again, after success. removed token must prevent this
        $request['post']['password'] = 'zxc!@#ZxsXC123';    
        $usecase = new UpdatepasswordUsecase();
        $responseBag =  $usecase->execute($request);
        //print_r($responseBag->response);   
        $expected_result = 'No record found with this token. Go to the login page and follow the forgot passsword process again.';  
        $this->assertEquals($expected_result, $responseBag->response['flashMessages']['error'][0] );    
        
        // ---reset the user table ---                    
        $this->testDbService = new TestDbService();
        $this->testDbService->truncateTestTable('user');
        $this->testDbService->addTestSuperUser();       
    }         
        
 }
?>
