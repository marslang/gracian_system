<?php

/*
* SYNTAX: assertEquals(mixed $expected, mixed $actual[, string $message = ''])
* COMMAND: phpunit application/usecase/tag
*/

//use gracian_system\application\command\stree\ShowStreeCommand;
use gracian_system\application\service\TestDbService;

use gracian_system\application\usecase\admin\tag\ListTagUsecase;  
use gracian_system\application\usecase\admin\tag\CreateTagUsecase;
use gracian_system\application\usecase\admin\tag\StoreTagUsecase;
use gracian_system\application\usecase\admin\tag\EditTagUsecase;
use gracian_system\application\usecase\admin\tag\UpdateTagUsecase;
use gracian_system\application\usecase\admin\tag\DeleteTagUsecase;
use gracian_system\application\usecase\admin\tag\DestroyTagUsecase;  
   
//use gracian_system\application\command\admin\authentication\ValidateLoginCommand;
use gracian_system\application\usecase\admin\authentication\ValidateLoginUsecase;

/*
use gracian_system\application\command\admin\user\CreateUserCommand;
use gracian_system\application\command\admin\user\StoreUserCommand;
use gracian_system\application\command\admin\user\EditUserCommand;
use gracian_system\application\command\admin\user\UpdateUserCommand;
use gracian_system\application\command\admin\user\DeleteUserCommand;
use gracian_system\application\command\admin\user\DestroyUserCommand;
use gracian_system\application\command\admin\authentication\ValidateLoginCommand;
*/
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianAccessException;
use gracian_system\domain\exceptions\GracianException;

use gracian_project\application\service\ConfigFactory;
//use gracian_project\domain\service\NodeFactory;
//use gracian_project\application\service\CommandFactory;




class TagTest extends \PHPUnit_Framework_TestCase
{

    private $session = array();


    //_____________________________________________________________________________________________
    public function __construct(){

		//$configFactory = ConfigFactory::Instance('testConfig');
		$this->config = ConfigFactory::Instance('testConfig')->config;

        //$this->config->sessionVo->transformIn($httpRequest->session());
        //$this->nodeFactory = new NodeFactory();
        //$this->commandFactory = new CommandFactory();
        $this->testDbService = new TestDbService();
        //$this->config->sessionVo->transformIn($httpRequest->session());    
        
        $this->session['login'] = Array(
                    'id' => '1',
                    'fullname' => 'Mars',
                    'username' => 'su',
                    'role' => 'su'
                );    
        
        $this->config->sessionVo->transformIn($this->session);                    


    }


    //_____________________________________________________________________________________________
    public function testAll() {

        $this->testDbService->truncateTestTable('tag');
        $this->testDbService->addTestTag();       
          
        
        //$this->loginUser();  
        $this->listTags();
        $this->createTag();
        $tag_id = $this->storeTag();       
        $this->storeTagWithValidationException();
        $this->editTag($tag_id);      
        $this->updateTag($tag_id);       
        $this->validateTag($tag_id);       
        
        $this->deleteTag($tag_id);
        $this->destroyTag($tag_id);
        
        /*
        $this->updateUserWithUsernameDoublure($user_id);
        $this->updateUserWithEmailDoublure($user_id);
        $this->viewRemovedUser($user_id);
        $uid = $this->storeUserWithUsernameDoublure();
        */
        // change password
    }    

    public function listTags() {     

        $session['login'] = Array('id' => '1', 'fullname' => 'Mars','username' => 'su','role' => 'su');    
        $this->config->sessionVo->transformIn($session);      
                  
        $request['get'] = array();
        $usecase = new ListTagUsecase($request);
        $responseBag =  $usecase->execute($request); 
        $response = $responseBag->transformOut();   
        //print_r($response); exit();    
        $this->assertEquals('testtagname', $response->tagEntity->tagList[0]['name']);
        //$this->config->sessionVo->transformOut($this->session);
    }          

    public function createTag() {     
        //$this->config->sessionVo->transformIn($this->session);
        $request['get'] = array();
        $usecase = new CreateTagUsecase($request);
        $responseBag =  $usecase->execute($request);     
        $response = $responseBag->transformOut();       
        //print_r($response); 
        $this->assertEquals('1', $response->tagEntity->item['permission']['create']);
        //$this->config->sessionVo->transformOut($this->session);
    }   

    public function storeTag() {     
        //$this->config->sessionVo->transformIn($this->session);     
        $request['post']  = Array(
                    'name' => 'books',
                );          
        
        //$request['get'] = array();
        $usecase = new StoreTagUsecase($request);
        $responseBag =  $usecase->execute($request);     
        $response = $responseBag->transformOut();       
        $this->assertEquals('2', $response->new_id          );        
        $this->assertEquals('Successfully stored the tag.', $responseBag->get('flashMessages')['success'][0]);
        //$this->config->sessionVo->transformOut($this->session);    
        return $response->new_id;
    }            
    
    public function storeTagWithValidationException() {     
        //$this->config->sessionVo->transformIn($this->session);     
        $request['post']  = Array(
                    'name' => '' //books and magazines',
                );          
        //$request['get'] = array();
        $usecase = new StoreTagUsecase($request);
        $responseBag =  $usecase->execute($request);     
        $response = $responseBag->transformOut();       
        //print_r($response);
        $this->assertEquals('Name is required',  $response->validationErrors['name'][0]);        
        //$this->assertEquals('Name must contain more than 2 characters',  $response->validationErrors['name'][1]);        
        //$this->config->sessionVo->transformOut($this->session);
    }    
    
    public function editTag($tag_id) {   
        //echo "\n editTag($tag_id) \n"; 
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $tag_id;
        //$request['get']['node'] = $nodeName = 'chapter';
        //$request['post']  = $this->chapterItem;
        //$command = $this->commandFactory->getCommand($nodeName, 'edit');
        $usecase = new EditTagUsecase();
        $responseBag =  $usecase->execute($request);
        $response = $responseBag->transformOut();      
        $this->assertEquals($request['get']['id'], $response->tagEntity->item['id'] );
        //$this->assertEquals($response->userEntity->item['use_expire_date'], '1' );
        //$this->config->sessionVo->transformOut($this->session);
        
    }       
    
    public function updateTag($tag_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $tag_id;
        $request['post']  = Array(
            'name' => '', //'movies<b>bad',    
            'trouble' => ' a wrong field'
        );
        $usecase = new UpdateTagUsecase();
            
        $responseBag =  $usecase->execute($request);
        //try{
        //}catch(GracianValidationException $e){
        //    print_r($e->getUserMessage()); exit(); 
        //}
        $response = $responseBag->transformOut();     
        //print_r($response);  
        $this->assertEquals('Name is required',  $response->validationErrors['name'][0]);        
        //$this->assertEquals('Name must contain more than 2 characters',  $response->validationErrors['name'][1]);             
        //print_r($response); exit();
        //$this->assertEquals($response->new_id , 2 );
        //$this->assertEquals($response->sta tus  , 'success');
        //$this->assertEquals($this->config->sessionVo->flashStage['sta tus']  , 'success');      
        //$this->assertEquals('Successfully updated the tag.', $responseBag->get('flashMessages')['success'][0]        );
        

        //return $response->new_id;
        //$this->config->sessionVo->transformOut($this->session);
    }       
    
    public function validateTag($tag_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $tag_id;
        $request['post']  = Array(
            'name' => 'movies',
        );
        $usecase = new UpdateTagUsecase();
        $responseBag =  $usecase->execute($request);
        //$response = $responseBag->transformOut();
        //print_r($response); exit();
        //$this->assertEquals($response->new_id , 2 );
        //$this->assertEquals($response->sta tus  , 'success');
        //$this->assertEquals($this->config->sessionVo->flashStage['sta tus']  , 'success');      
        $this->assertEquals('Successfully updated the tag.', $responseBag->get('flashMessages')['success'][0]        );
        

        //return $response->new_id;
        //$this->config->sessionVo->transformOut($this->session);
    }         
    
    public function deleteTag($tag_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));

        $request['get']['id'] = $tag_id;
        //$request['get']['node'] = $nodeName = 'chapter';
        //$request['post']  = $this->chapterItem;
        //$command = $this->commandFactory->getCommand($nodeName, 'edit');
        $usecase = new DeleteTagUsecase();
        $responseBag = $usecase->execute($request);
        $response = $responseBag->transformOut();
        //print_r($response); exit();
        $this->assertEquals(2, $response->tagEntity->item['id']   );
        $this->assertEquals(0, $response->tagEntity->relationCount);
        //$this->config->sessionVo->transformOut($this->session);
    }        
    
    public function destroyTag($tag_id) {
        //$this->config->sessionVo->transformIn($this->session);
        //unset($responseBag->get('flashMessages'));
        $request['post']['id'] = $tag_id;
        //$command = $this->commandFactory->getCommand($nodeName, 'destroy');
        $usecase = new DestroyTagUsecase();
        $responseBag = $usecase->execute($request);     
        $response = $responseBag->transformOut();
        //print_r($response); exit();
        //$this->assertEquals($response->sta tus, 'success');

        $this->assertEquals('Successfully deleted the tag.', $responseBag->get('flashMessages')['success'][0]);
        //$this->assertEquals($responseBag->get('flashMessages')['success'][0], 'Successfully deleted the user.');
        //print_r($responseBag->get('flashMessages')); exit();

        $this->assertEquals( '/admin/tag/index', $response->redirect );
        //$this->config->sessionVo->transformOut($this->session);
    }            
                             
    

/* 
    public function viewUser() {
        //unset($responseBag->get('flashMessages'));
        $this->config->sessionVo->transformIn($this->session);
        //print_r($this->session); exit();
        $request['get']['id'] = $this->testDbService->userItem['id'];
        //$request['get']['node'] = $nodeName = 'root';
        $command = new ViewUserCommand(); //$this->commandFactory->getCommand($nodeName, 'view');
        $responseBag =  $command->execute($request);
        $response = $responseBag->transformOut();
        //print_r($response); exit();
        $this->assertEquals($response->userEntity->item['id']        , $request['get']['id'] );
        $this->assertEquals($response->userEntity->item['email'] , $this->testDbService->userItem['email']);

        $this->config->sessionVo->transformOut($this->session);

    }
*/
       /*
    public function createUser() {
        $this->config->sessionVo->transformIn($this->session);
        unset($responseBag->get('flashMessages'));

        $request['get'] = array();//['parentId'] = 1;
        //$request['get']['node'] = $nodeName = 'chapter';
        //$request['post']  = $this->chapterItem;
        //$command = $this->commandFactory->getCommand($nodeName, 'create');
        $command = new CreateUserCommand();
        $responseBag =  $command->execute($request);
        $response = $responseBag->transformOut();
        $this->assertEquals($response->userEntity->item['use_expire_date']        , '1' );
        $this->config->sessionVo->transformOut($this->session);
    }
 */     






}
?>
