<?php namespace gracian_system\infrastructure\helpers;           

use gracian_system\infrastructure\helpers\FormDate;  
use gracian_system\infrastructure\helpers\DateHelper;  

class FormdateHelper {

    public function __construct(){     
        $this->dateHelper = new DateHelper();
    }
        
    public function get_formdate($mysql_date, $prefix){
        $formdate = new FormDate();      
        $formdate->config['prefix'] = $prefix;
        $formdate->setLocale('en_US');
        $formdate->year['start'] = 1990;    
        $formdate->year['end'] = 2020;
        $formdate->month['values'] = 'string';        
        $formdate->year['extra'] =  ' class="form-control" ';
        $formdate->month['extra'] =  ' class="form-control" ';
        $formdate->day['extra'] =  ' class="form-control" ';
        $formdate->hour['extra'] =  ' class="form-control" ';
        $formdate->minute['extra'] =  ' class="form-control" ';
        return $this->set_formdate($formdate, $mysql_date);
    }
    
    public function set_formdate($formdate, $mysql_date){
        $date_arr = $this->dateHelper->date_from_mysql_to_array($mysql_date);
        $formdate->year['selected']   = $date_arr['year'];
        $formdate->month['selected']  = $date_arr['month'];
        $formdate->day['selected']    = $date_arr['day'];
        $formdate->hour['selected']   = $date_arr['hour'];
        $formdate->minute['selected'] = $date_arr['minute'];
        return $formdate;     
    }


}
