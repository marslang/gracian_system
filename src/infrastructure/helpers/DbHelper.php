<?php namespace gracian_system\infrastructure\helpers;  
            
class DbHelper{   
    //_____________________________________________________________________________________________      
    public function getDateTimeNow(){
        $nowUtc = new \DateTime( 'now',  new \DateTimeZone( 'UTC' ) );
        $nowUtc->setTimezone( new \DateTimeZone( 'Europe/Amsterdam' ) );    
        return $nowUtc->format('Y-m-d h:i:s');    
    }                                                                          
    
    //_____________________________________________________________________________________________ 
    /*
     * create unique dirname
     * hexadecimaal 2 posities levert 256 variaties    
    */
    function generate_subdir(){ 
        list($x, $y) = explode(" ", microtime());
        $subdir = substr(md5($x), 2, 2);
        return $subdir;
    }         
    
}