<?php namespace gracian_system\infrastructure\helpers;    

class CiFormHelper{        


// ------------------------------------------------------------------------


    /**
     * Drop-down Menu
     *
     * @param    mixed    $data
     * @param    mixed    $options
     * @param    mixed    $selected
     * @param    mixed    $extra
     * @return    string
     */      
     
    function form_dropdown($data = '', $options = array(), $selected = array(), $extra = '')
    {
        $defaults = array();

        if (is_array($data))
        {
            if (isset($data['selected']))
            {
                $selected = $data['selected'];
                // select tags don't have a selected attribute
                unset($data['selected']); 
            }

            if (isset($data['options']))
            {
                $options = $data['options'];
                // select tags don't use an options attribute
                unset($data['options']); 
            }
        }
        else
        {
            $defaults = array('name' => $data);
        }

        is_array($selected) OR $selected = array($selected);
        is_array($options) OR $options = array($options);

        // If no selected state was submitted we will attempt to set it automatically
        if (empty($selected))
        {
            if (is_array($data))
            {
                if (isset($data['name'], $_POST[$data['name']]))
                {
                    $selected = array($_POST[$data['name']]);
                }
            }
            elseif (isset($_POST[$data]))
            {
                $selected = array($_POST[$data]);
            }
        }

        $extra = $this->_attributes_to_string($extra);

        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select '.rtrim($this->_parse_form_attributes($data, $defaults)).$extra.$multiple.">\n";

        foreach ($options as $key => $val)
        {
            $key = (string) $key;

            if (is_array($val))
            {
                if (empty($val))
                {
                    continue;
                }

                $form .= '<optgroup label="'.$key."\">\n";

                foreach ($val as $optgroup_key => $optgroup_val)
                {
                    $sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
                    $form .= '<option value="'.$this->form_prep($optgroup_key).'"'.$sel.'>'
                        .(string) $optgroup_val."</option>\n";
                }

                $form .= "</optgroup>\n";
            }
            else
            {
                $form .= '<option value="'.$this->form_prep($key).'"'
                    .(in_array($key, $selected) ? ' selected="selected"' : '').'>'
                    .(string) $val."</option>\n";
            }
        }

        return $form."</select>\n";
    }   
    

// ------------------------------------------------------------------------


    /**
     * Form Prep
     *
     * Formats text so that it can be safely placed in a form field in the event it has HTML tags.
     *
     * @param    string|string[]    $str        Value to escape
     * @param    bool        $is_textarea    Whether we're escaping for a textarea element
     * @return    string|string[]    Escaped values
     */   
     
    function form_prep($str = '', $is_textarea = FALSE)
    {
        if (is_array($str))
        {
            foreach (array_keys($str) as $key)
            {
                $str[$key] = $this->form_prep($str[$key], $is_textarea);
            }

            return $str;
        }

        if ($is_textarea === TRUE)
        {
            return str_replace(array('<', '>'), array('&lt;', '&gt;'), stripslashes($str));
        }

        return str_replace(array("'", '"'), array('&#39;', '&quot;'), stripslashes($str));
    }  
    


// ------------------------------------------------------------------------


    /**
     * Parse the form attributes
     *
     * Helper function used by some of the form helpers
     *
     * @param    array    $attributes    List of attributes
     * @param    array    $default    Default values
     * @return    string
     */    
     
    function _parse_form_attributes($attributes, $default)
    {
        if (is_array($attributes))
        {
            foreach ($default as $key => $val)
            {
                if (isset($attributes[$key]))
                {
                    $default[$key] = $attributes[$key];
                    unset($attributes[$key]);
                }
            }

            if (count($attributes) > 0)
            {
                $default = array_merge($default, $attributes);
            }
        }

        $att = '';

        foreach ($default as $key => $val)
        {
            if ($key === 'value')
            {
                $val = $this->form_prep($val);
            }
            elseif ($key === 'name' && ! strlen($default['name']))
            {
                continue;
            }

            $att .= $key.'="'.$val.'" ';
        }

        return $att;
    }    
    

// ------------------------------------------------------------------------


    /**
     * Attributes To String
     *
     * Helper function used by some of the form helpers
     *
     * @param    mixed
     * @return    string
     */    
     
    function _attributes_to_string($attributes)
    {
        if (empty($attributes))
        {
            return '';
        }

        if (is_object($attributes))
        {
            $attributes = (array) $attributes;
        }

        if (is_array($attributes))
        {
            $atts = '';

            foreach ($attributes as $key => $val)
            {
                $atts .= ' '.$key.'="'.$val.'"';
            }

            return $atts;
        }

        if (is_string($attributes))
        {
            return ' '.$attributes;
        }

        return FALSE;
    }


}
