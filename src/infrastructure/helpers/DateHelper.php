<?php namespace gracian_system\infrastructure\helpers;    

class DateHelper{
                     

    function get_mysql_now($the_time='now'){
        $nowUtc = new \DateTime( $the_time,  new \DateTimeZone( 'UTC' ) );
        $nowUtc->setTimezone( new \DateTimeZone( 'Europe/Amsterdam' ) );    
        return $nowUtc->format('Y-m-d H:i:s');    
    }
 
    function date_from_mysql_to_array($datetime_string){
        // 2011-03-01 00:00:00
        $args = explode(' ', $datetime_string);
        $date_str = $args[0];
        $time_str = $args[1];
        $date_arr = explode('-', $date_str);
        $time_arr = explode(':', $time_str);
        $datetime_arr['year']     = $date_arr[0];
        $datetime_arr['month']     = $date_arr[1];
        $datetime_arr['day']     = $date_arr[2];
        $datetime_arr['hour']     = $time_arr[0];
        $datetime_arr['minute'] = $time_arr[1];
        $datetime_arr['second'] = $time_arr[2];
        return     $datetime_arr;
    }
    
    function date_from_post_to_mysql($post){
        if(isset($post['year'])){
            return $post['year'] . '-' . $post['month'] . '-' . $post['day']  . ' '.$post['hour'].':'.$post['minute'].':00';
        }else{
            return '0000-00-00 00:00:00';
        }
    }
    
}    
   