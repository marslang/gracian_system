<?php namespace gracian_system\infrastructure\validators;

use gracian_system\application\infrastructurePorts\ValidatorIF;     
use Valitron; 

class ValitronValidatorAdapter implements ValidatorIF {
    
    private $validationErrors = array();

    //_____________________________________________________________________________________________
    /*   
    NOTE: een geneste fields-array werkt niet. je moet de fields eerst platslaan voordat je ze aan valitron geeft         
    */     
    public function validate($fields, $schema){       
        $flattenFields = $this->flattenArray($fields);           
        $this->validator = new Valitron\Validator($flattenFields);     
        $this->addCustomRulesToValitron();  
        $this->feedSchemaToValitron($flattenFields, $schema);    
        $isValid = $this->validator->validate();
        if(!$isValid){
            $this->validationErrors = $this->transformOutValidationErrors($this->validator->errors());     
        }
        return $isValid;  
    }           
    
   //_____________________________________________________________________________________________
   public function flattenArray($theArray, $levels=1){    
        $flatArray = array(); 
        if(count($theArray) > 0){
            foreach($theArray as $k => $v){
                if(is_array($v)){   
                    foreach($v as $k2 => $v2){
                        $flatArray[$k . ':' . $k2] = $v2;
                    }
                }else{
                    $flatArray[$k] = $v;
                }
            }
        }
        return $flatArray;
    }
    
   
    //_____________________________________________________________________________________________
    private function transformOutValidationErrors($validationErrors){  
        $cleanValErr = array(); 
        foreach($validationErrors as $k => $v){   
            $kclean = str_replace('variable_fields:' , '', $k);
            foreach($v as $kk => $vv){
                $cleanValErr[$kclean][] = ucfirst(str_replace('Variable Fields:' , '', $vv));
            }
        } 
        return $cleanValErr;
    }      
    
    //_____________________________________________________________________________________________
    public function addCustomRulesToValitron(){     
           
        //- - - - - - - - - - - - - - - -
        $ruleName = 'mysqldate';
        $lambdaFunc = function($field, $value, array $params) {    
            $re = '{^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|11)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468][048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(02)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$}'; 
            return preg_match($re, $value);      
        };
        $errorMessage = 'date must match yyyy-mm-dd hh:mm:ss';
        $this->validator->addRule($ruleName, $lambdaFunc, $errorMessage);    
        
        //- - - - - - - - - - - - - - - -
        //PrintableASCII   "^[a-zA-Z0-9&#32;!&#34;#$%&amp;'()*+,\-.\/:;&#60;=>?@[\\\]^_`{|}~]+$" 
        //PrintableUnicode "^[a-zA-Z0-9\p{L}&#32;!&#34;#$%&amp;'()*+,\-.\/:;&#60;=>?@[\\\]^_`{|}~]+$"    
        $ruleName = 'printable'; //PrintableUnicode
        $lambdaFunc = function($field, $value, array $params) {    
            $re = "{^[a-zA-Z0-9\p{L}&#32;!&#34;#$%&amp;'()*+,\-.\/:;&#60;=>?@[\\\]^_`{|}~]+$}"; 
            return preg_match($re, $value);      
        };
        $errorMessage = 'must contain only printable characters';
        $this->validator->addRule($ruleName, $lambdaFunc, $errorMessage);       
        
        //- - - - - - - - - - - - - - - -
        $ruleName = 'title'; 
        $lambdaFunc = function($field, $value, array $params) {    
            $re = "/^[a-zA-Z0-9\p{L}!@#$%^&amp;\/()_+\-=,.'` ]+$/u";  // no: ~ " 
            return preg_match($re, $value);      
        };
        $errorMessage = 'can only contain letters, numbers spaces and dashes';
        $this->validator->addRule($ruleName, $lambdaFunc, $errorMessage);              
      
        //- - - - - - - - - - - - - - - -
        $ruleName = 'password4t2x'; 
        $lambdaFunc = function($field, $value, array $params) {    
            $r1='/[A-Z]/';  
            $r2='/[a-z]/';  
            $r3='/[!@#$%^&*()\-_=+{}\[\];:,<.>]/';  
            $r4='/[0-9]/';  
            $result = TRUE;
            if(preg_match_all($r1,$value, $o)<2){
                $result = FALSE; 
            }
            if(preg_match_all($r2,$value, $o)<2){
                $result = FALSE;  
            }
            if(preg_match_all($r3,$value, $o)<2){
                $result = FALSE;  
            }
            if(preg_match_all($r4,$value, $o)<2){
                $result = FALSE; 
            }
            if(strlen($value)<8){
                $result = FALSE;
            }
            if(strlen($value)>72){
                $result = FALSE;
            }
            if(strpos($value,' ') !== FALSE){
                $result = FALSE;
            }
            return $result;                
            
            //$re = "/^[a-zA-Z0-9\p{L}!@#$%^&amp;\/()_+\-=,.'` ]+$/u";  // no: ~ " 
            //return preg_match($re, $value);      
        };
        $errorMessage = 'must contain upper and lower case letters, numbers and special characters (at least 2 of each), cannot contain spaces and must be between 8 and 72 characters.';
        $this->validator->addRule($ruleName, $lambdaFunc, $errorMessage);              
              
        //- - - - - - - - - - - - - - - -
        $ruleName = 'filename'; 
        $lambdaFunc = function($field, $value, array $params) {    
            $re = "/^[a-zA-Z0-9\p{L}-_.` ]{0,255}$/u";  
            return preg_match($re, $value);      
        };
        $errorMessage = 'can only contain letters, numbers dashes and hyphens.';
        $this->validator->addRule($ruleName, $lambdaFunc, $errorMessage);              
                       
    }        

    //_____________________________________________________________________________________________
    private function feedSchemaToValitron($flattenFields, $schema){     
        foreach ($flattenFields as $field_name => $field_array){     
            $pieces = explode(':', $field_name);
            if(count($pieces) < 2){                
                if (!isset($schema[$field_name]['validators'])){
                    continue;                
                }
                $validators = $schema[$field_name]['validators'];    
            }else{
                if (!isset($schema[$pieces[0]][$pieces[1]]['validators'])){
                    continue;                
                }
                $validators = $schema[$pieces[0]][$pieces[1]]['validators'];     
            }   
           
            foreach ($validators as $validator_name => $validator_arr){   
                 $this->selectValidatorRule($validator_name, $field_name, $validator_arr);
            } 
                             
            
        }      
    }

    
    //_____________________________________________________________________________________________
    private function selectValidatorRule($validator_name, $field_name, $validator_arr){     
        if ($validator_name == "required"){
            $this->addRuleToValidator("required", $field_name);
        }   
        
        // Match another field
        if ($validator_name == "equals"){
            $this->addRuleToValidator("equals", $field_name, $validator_arr['field']);
        }
        // Negation of match another field
        if ($validator_name == "different"){
            $this->addRuleToValidator("different", $field_name, $validator_arr['field']);
        }                  

        if ($validator_name == "numeric"){
            $this->addRuleToValidator("numeric", $field_name);
        }                    

        if ($validator_name == "integer"){
            $this->addRuleToValidator("integer", $field_name);
        }     
        if ($validator_name == "array"){
            // For now, just check that it is an array.  Really we need a new validation rule here.
            $this->addRuleToValidator("array", $field_name);
        }                               
        
        if ($validator_name == "length"){
            $this->addRuleToValidator("length", $field_name, $validator_arr['value']);
        }  
        
        if ($validator_name == "lengthBetween"){
             $this->addRuleToValidator("lengthBetween", $field_name, $validator_arr['min'], $validator_arr['max']);  
        }                      
        
        if ($validator_name == "lengthMin"){
            $this->addRuleToValidator("lengthMin", $field_name, $validator_arr['value']);
        }                                      

        if ($validator_name == "lengthMax"){       
            $this->addRuleToValidator("lengthMax", $field_name, $validator_arr['value']);
        }  
        
        if ($validator_name == 'min'){
            $this->addRuleToValidator("min", $field_name, $validator_arr['value']);
        }               
        if ($validator_name == 'max'){
            $this->addRuleToValidator("max", $field_name, $validator_arr['value']);
        }                       
                        
        if ($validator_name == "email"){
            $this->addRuleToValidator("email", $field_name);
        }            

        if ($validator_name == "alpha_dash"){
            $this->addRuleToValidator("slug", $field_name);//, $validator_arr['field']);
        } 
                          
        // Check membership in array
        if ($validator_name == "in"){
            $this->addRuleToValidator("in", $field_name, $validator_arr['values'], true);    // Strict comparison
        }
        // Negation of membership
        if ($validator_name == "notIn"){
            $this->addRuleToValidator("notIn", $field_name, $validator_arr['values'], true);  // Strict comparison
        }     
                                                       
        
        
        // - - - - - custom validators: - - - - - - - -  

        if ($validator_name == "mysqldate"){   
            $this->addRuleToValidator("mysqldate", $field_name);  
        }     
                      
        if ($validator_name == "printable"){   
            $this->addRuleToValidator("printable", $field_name);  
        }    
        
        if ($validator_name == "title"){   
            $this->addRuleToValidator("title", $field_name);  
        }                           
                             
        if ($validator_name == "password4t2x"){   
            $this->addRuleToValidator("password4t2x", $field_name);  
        }                           
                                                
        if ($validator_name == "filename"){   
            $this->addRuleToValidator("filename", $field_name);  
        }                           

    }        
    
    //_____________________________________________________________________________________________
    private function addRuleToValidator($rule) {     
        // Weird way to adapt with Valitron's funky interface
        $params = array_merge(array_slice(func_get_args(), 0));   
        call_user_func_array([$this->validator,"rule"], $params);    
    }   
    
    //_____________________________________________________________________________________________       
    /*
    Array(    
        [id] => Array(
            [0] => Id is required
        )    
        [name] => Array (
            [0] => Name must be at least 1
            [1] => Name is not a valid email address       
        )
    ) 
    */
    public function getValidationErrorMessages(){   
         return $this->validationErrors;
    }

    /*
    //_____________________________________________________________________________________________
    private function transformIn($gracianSchema){  
        return $gracianSchema;
    } 
    */   

}
            