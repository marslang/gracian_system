<?php namespace gracian_system\infrastructure\sessionVo;

use gracian_system\application\infrastructurePorts\SessionVoIF;

/**
* The sessionVo is filled with the session on every request.
* The sessionVo is an immutable object (set on construction, read only)
*/
class Laravel5SessionVo implements SessionVoIF
{

    public $name = 'Laravel5SessionVo';
    private $data = array(); 


    //______________________________________________________
    public function transformIn($session){
        $this->data = $session->all();
        // if not logged in, set the login to anon.
        if(!isset($this->data['login'])){
            $this->data['login'] = Array('id' => 0,'fullname' => 'anon', 'username' => 'anon', 'role' => 'anon');
        }      
    }     

    //______________________________________________________   
    public function get($key , $default=array()){
       if(isset($this->data[$key])){
           return $this->data[$key];
       }else{
           return $default;
       }
    }  

    //______________________________________________________
    public function hasValidationErrors(){   
        if(isset($this->data['validationErrors'])){
            return TRUE;
        }
        return FALSE;
    }      


}
