<?php namespace gracian_system\infrastructure\sessionVo;

use gracian_system\application\infrastructurePorts\SessionVoIF;

/**
* The sessionVo is filled with the session on every request.
* The sessionVo is read only
* Do not write to the sessionVo
*
*/
class TestSessionVo implements SessionVoIF
{
    public $name = 'TestSessionVo';
    public $data = array(); 

    //______________________________________________________
    public function transformIn($session){
        $this->data = $session; 
    }  

    //______________________________________________________
    public function get($key , $default=array()){
       if(isset($this->data[$key])){
           return $this->data[$key];
       }else{
           return $default;
       }
    }     


    public function hasValidationErrors(){   
        if(isset($this->data['validationErrors'])){
            return TRUE;
        }
        return FALSE;
    }   
/*
    [flash] => Array
        (
            [old] => Array
                (
                    [0] => messages
                )

            [new] => Array
                (
                )

        )
        */    




}
