<?php namespace gracian_system\infrastructure\fileUpload;    

use gracian_system\application\infrastructurePorts\FileUploadIF;    
use gracian_system\domain\exceptions\GracianUploadException;    
 

class TestFileUploadAdapter implements FileUploadIF {   
    
    //_____________________________________________________________________________________________
    public function uploadFile($target_dir, $subdir, $fileToUpload){
        $fileUploadError = $this->save($target_dir, $subdir, $fileToUpload);
        if($fileUploadError != NULL){
            throw new GracianUploadException('GracianUploadException: ' . $fileUploadError);
        }
    }     
    
    //_____________________________________________________________________________________________
    /**
    * there is a new file to upload and it is successfull, so remove the old file (if there is one)
    */ 
    public function removeOldFile($target_dir, $subdir, $oldFilename, $newFilename) {
        if($newFilename != $oldFilename && $oldFilename != ''){
             $this->remove($target_dir, $subdir, $oldFilename);
        }
    }        
    
    //_____________________________________________________________________________________________
    public function removeFile($target_dir, $subdir, $filename) {
        $this->remove($target_dir, $subdir, $filename);
    }        

    //_____________________________________________________________________________________________ 
    public function exists($target_dir,  $subdir, $fileName){      
        $target_dir .= '/';    
        if($subdir != ''){
           $target_dir .=  $subdir . '/' ;  
        }
        $target_file = $target_dir . $fileName;
        if($fileName != '' && file_exists($target_file) ){     
            return TRUE;
        }               
        return FALSE;
    }   
                                                                    
    //_____________________________________________________________________________________________    
    /*       
    * move_uploaded_file() works only on files which are actually uploaded via php
    * read more: http://php.net/manual/en/function.move-uploaded-file.php
    * for testing, use the TestFileUploadAdapter
    */
    
    private function save($target_dir, $subdir, $fileToUpload){     
        $target_dir .= '/';    
        $errorString = null;   
        if($subdir != ''){
           $target_dir .=  $subdir . '/' ;  
        }
        $target_file = $target_dir . basename($fileToUpload["name"]);     
        if(!is_dir($target_dir)){            
            if (!mkdir($target_dir, 0777, true)) {
                return "TestFileUploadAdapter: Failed to create folder.";          
            }
        }
        
        if (file_exists($target_file)) {      
             $errorString = "TestFileUploadAdapter: File already exists.";                              
        }else{   
            if (!copy($fileToUpload["tmp_name"], $target_file)) {
                $errorString = "TestFileUploadAdapter: There was an error uploading your file.";                                 
            }         
        }
        return $errorString;    
    }    
    
    
    
    //_____________________________________________________________________________________________ 
    private function remove($target_dir,  $subdir, $fileName){       
        $target_dir .= '/';    
        if($subdir != ''){
           $target_dir .=  $subdir . '/' ;  
        }
        $target_file = $target_dir . $fileName;
        if($fileName != '' && file_exists($target_file) ){     
            unlink($target_file);
        } 
    }             
        
}