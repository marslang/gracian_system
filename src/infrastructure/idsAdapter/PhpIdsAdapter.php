<?php namespace gracian_system\infrastructure\idsAdapter;

use gracian_system\application\infrastructurePorts\IdsAdapterIF;
use gracian_system\domain\exceptions\GracianIntrusionException;



/**
 * PHPIDS
 * Requirements: PHP5, SimpleXML
 *
 * Copyright (c) 2010 PHPIDS group (https://phpids.org)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

use IDS\Init;
use IDS\Monitor;
use IDS\Filter\Storage;

class PhpIdsAdapter  implements IdsAdapterIF{      
    
    public $result;

    //_____________________________________________________________________________________________
    public function __construct($idsConfigPath, $idsLibPath, $idsCachePath){
        $this->idsConfigPath = $idsConfigPath;
        $this->idsLibPath = $idsLibPath;        
        $this->idsCachePath = $idsCachePath;        
        //echo  $this->idsConfigPath;
    }

    //_____________________________________________________________________________________________
    public function hasIntrusions(){
        if (!$this->result->isEmpty()) {
            return TRUE;
        }
        return FALSE;
    }

    //_____________________________________________________________________________________________
    public function getIntrusionReport(){
        return $this->result->__toString();
    }

    //_____________________________________________________________________________________________
    public function ids_detect($request = array()){
        try {
            /*
            * It's pretty easy to get the PHPIDS running
            * 1. Define what to scan
            *
            * Please keep in mind what array_merge does and how this might interfere
            * with your variables_order settings
            */

            $init = Init::init($this->idsConfigPath);         
            $init->config['General']['tmp_path'] = $this->idsCachePath;
            $init->config['General']['base_path'] = $this->idsLibPath; 
            $init->config['General']['use_base_path'] = true;
            $init->config['Caching']['caching'] = 'none';
            //print_r($init); exit();
            /*
            $ids = new Monitor($init);     
            // overload the Monitor-constructor   
            // PhpIdsMonitor extends  Monitor
            */
            $ids = new PhpIdsMonitor($init);
            $this->result = $ids->run($request);    
            return;

        } catch (\Exception $e) {
            /*
            * sth went terribly wrong - maybe the
            * filter rules weren't found?
            */
            printf(
                'An error occured in PhpIdsAdapter: %s <br>',
                $e->getMessage()
            );
        }

    }
}
