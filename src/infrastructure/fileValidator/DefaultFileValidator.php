<?php namespace gracian_system\infrastructure\fileValidator;

use gracian_system\application\infrastructurePorts\FileValidatorIF;


class DefaultFileValidator implements FileValidatorIF
{
   
    private $name = 'DefaultFileValidator';
    
    private $phpFileUploadErrors = array(
    '0' => 'There is no error, the file uploaded with success',
    '1' => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
    '2' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
    '3' => 'The uploaded file was only partially uploaded',
    '4' => 'No file was uploaded',
    '6' => 'Missing a temporary folder',
    '7' => 'Failed to write file to disk.',
    '8' => 'A PHP extension stopped the file upload.'
);



     /*
    [fileToUpload] => Array  (
            [name] => espinaler.jpg
            [type] => image/jpeg
            [tmp_name] => /tmp/phpiE00sS
            [error] => 0
            [size] => 110918
        )
    */

    //_____________________________________________________________________________________________
    public function validate($fileToUpload){    
        
        if($fileToUpload['error'] > 0) {
            return $this->name . ': ' . $this->phpFileUploadErrors[$fileToUpload['error']];
        }

        
        $imageFileType = strtolower(pathinfo($fileToUpload["name"],PATHINFO_EXTENSION));

        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            return $this->name . ': Only JPG, JPEG, PNG & GIF files are allowed.';
        }      

        // Check if image file is a actual image or fake image     
        //echo $fileToUpload["tmp_name"];
        $check = getimagesize($fileToUpload["tmp_name"]);
        if($check === false) {
            return $this->name . ': File is not an image.';
        }

        /*
        // TODO: Check if file already exists
        if (file_exists($target_file)) {
            $msg .=  "Sorry, file already exists.\n";
            $uploadOk = 0;
        }
        */

        // Check file size    
        // TODO: maak $maxFileSizeMB configureerbaar in gracian_prj_xmp
        // 2MB
        $maxFileSizeMB = 2;
        $maxFileSize = $maxFileSizeMB * 1000 * 1000; 
        if ($fileToUpload["size"] > $maxFileSize) {
            return $this->name . ': Your file is too large. Max file size is '.$maxFileSizeMB.'MB.';         
        }
        return NULL;
    }


    /**
     * --------------------------------------------------------------------------------------------
     * regex validation functions
    */

    /*
    PrintableASCII     "^[a-zA-Z0-9&#32;!&#34;#$%&amp;'()*+,\-.\/:;&#60;=>?@[\\\]^_`{|}~]+$" 
    PrintableUnicode   "^[a-zA-Z0-9\p{L}&#32;!&#34;#$%&amp;'()*+,\-.\/:;&#60;=>?@[\\\]^_`{|}~]+$" 

    */
    public function regexTitle($subject){
        $errorList = array();
        $pattern = "/^[a-zA-Z0-9\p{L}!@#$%^&amp;\/()_+\-=,.~'` ]{1,255}$/u";
        if(!preg_match($pattern, $subject)){
            $errorList[] = array('message' => 'Title can only contain letters, numbers spaces and dashes.');
        }
        return $errorList;
    }
    //_________________________________________________________________________
    public function regexBool($value){
        $errorList = array();
        if(!preg_match("/^(0|1)?$/", $value)){
            $errorList[] = array('message' => 'This value is not a valid boolean.');
        }
        return $errorList;
    }
    //_________________________________________________________________________
    public function regexInteger($value){
        $errorList = array();
        if(!preg_match("/^[0-9]{0,20}$/", $value)){
            $errorList[] = array('message' => 'This value is not a valid integer.');
        }
        return $errorList;
    }
    //_________________________________________________________________________
    // only letters and numbers and underscore, no spaces , no utf8
    public function regexName($value){ 
        $errorList = array();
         if(!preg_match("/^([a-zA-Z0-9]|_){0,255}$/", $value)){
            $errorList[] = array('message' => 'This value can only contain letters, numbers and underscore.');
        }
        return $errorList;
    }

    //_________________________________________________________________________
    public function regexRichtext($value){
        $errorList = array();   
        //return empty errorlist
        return $errorList;
    }

}
