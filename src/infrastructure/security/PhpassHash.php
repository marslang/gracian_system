<?php namespace gracian_system\infrastructure\security;

use gracian_system\domain\infrastructurePorts\HashAdapterIF;
use \Phpass\Hash;
use gracian_system\domain\exceptions\GracianException;

class PhpassHash implements HashAdapterIF
{

    /* NOTE: (In a real application, these should be in a configuration file included
    from the actual program code files instead. Alternatively, they may be configurable
    via the application itself, by an administrative user.)
    */   
 
    /*
    Base-2 logarithm of the iteration count used for password stretching
    hash_cost_log2 = 8
    Do we require the hashes to be portable to older systems (less secure)?
    hash_portable = FALSE
    */

    //_____________________________________________________________________________________________
    public function hash($password){
        //Now we can hash the password with:
        $hasher = new Hash();
        $hash = $hasher->hashPassword($password);
        if (strlen($hash) < 20){
            $this->fail('Failed to hash new password');
        } 
        unset($hasher);
        return $hash;
    }     

    //_____________________________________________________________________________________________
    /*  Hash
         adapter = Pbkdf2
         hmacKey = My53cr3tK3y
         hmacAlgo = sha512
     */
     public function checkPassword($password, $hashedPw) {
         $hasher = new Hash();
         return $hasher->checkPassword($password, $hashedPw);
     }

    //_____________________________________________________________________________________________
    private function fail($pub, $pvt = '')
    {
        $msg = $pub;
        if ($pvt !== ''){
            $msg .= ": $pvt";
        }
        throw new GracianException('PhpassHash: '. $msg);
    }

}
