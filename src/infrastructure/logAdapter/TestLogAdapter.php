<?php namespace gracian_system\infrastructure\logAdapter;

use gracian_system\application\infrastructurePorts\LogAdapterIF;

class TestLogAdapter implements LogAdapterIF {

    //______________________________________________________
    public function transformIn(){
    }
    //______________________________________________________
    public function transformOut(){
    }

/*
monolog manual:
https://github.com/Seldaek/monolog/blob/master/doc/01-usage.md#log-levels

Monolog supports the logging levels described by RFC 5424.
DEBUG (100): Detailed debug information.
INFO (200): Interesting events. Examples: User logs in, SQL logs.
NOTICE (250): Normal but significant events.
WARNING (300): Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
ERROR (400): Runtime errors that do not require immediate action but should typically be logged and monitored.
CRITICAL (500): Critical conditions. Example: Application component unavailable, unexpected exception.
ALERT (550): Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
EMERGENCY (600): Emergency: system is unusable.
*/

    //______________________________________________________
    public function logReport($type, $log_arr, $filePath){
        /*
        $log_json =  json_encode($log_arr);
        //$monolog = Log::getMonolog();
        $monolog = new Logger('gracian_log');
        $monolog->pushHandler(new StreamHandler($filePath, Logger::DEBUG));
        $monolog->pushHandler(new FirePHPHandler());
        // You can now use your logger
        $monolog->addWarning($type, $log_arr);
        */
    }


}
