<?php namespace gracian_system\infrastructure\mailer;

use gracian_system\application\infrastructurePorts\MailerIF;

class PhpMailerAdapter implements MailerIF {
    public $name = 'PhpMailerAdapter';

    /**
    * Send SmtpMail    
    * TODO: make the smpt host, port, username, password, etc configurable in config file in gracian_prj_xmp
    */
    public function sendMail($emailFrom, $nameFrom, $emailTo, $nameTo, $subject, $message){
        $mail = new \PHPMailer();

        $mail->ContentType = 'text/plain';
        $mail->IsHTML(false);
        $address = $emailTo;
        $mail->AddAddress($address, $nameTo);

        $mail->SetFrom($emailFrom, $nameFrom);
        $mail->AddReplyTo($emailFrom, $nameFrom);
        $mail->Subject = $subject;
        $mail->From = $emailFrom;

        // Very important: don't have lines for MsgHTML and AltBody
        $mail->Body = $message; 

        // telling the class to use SMTP
        $mail->IsSMTP(); 

        // SMTP server
        $mail->Host       = "smtp.mail.pcextreme.nl"; 

        // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only    
        // 3 = Enable verbose debug output  
        $mail->SMTPDebug  = 0;                     

        // enable SMTP authentication
        $mail->SMTPAuth   = true;                  

        $mail->Port       = 587;                    

        // SMTP account username
        $mail->Username   = "marcel@limonades.org"; 

        // SMTP account password
        $mail->Password   = "carson62";    

        if(!$mail->Send()) {
          echo "Mailer Error: " . $mail->ErrorInfo;
        } 

    }

}
