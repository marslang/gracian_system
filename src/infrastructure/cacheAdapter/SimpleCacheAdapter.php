<?php namespace gracian_system\infrastructure\cacheAdapter;

use gracian_system\application\infrastructurePorts\CacheAdapterIF;
//use gracian_system\domain\exceptions\GracianIntrusionException;

/*
this isn`t an adapter, but it is the cache functionality itself. 
see gracian_site_slim / BaseSiteController -> render() for the proper usage.

usage: 
in RootController
    return $this->render('bootstrap_blog/main_layout');     // cache this page for default seconds (see config)
    //return $this->render('bootstrap_blog/main_layout', 50); // cache this page 50 second
    //return $this->render('bootstrap_blog/main_layout', 0);  // don't cache this page     
    
in BaseController
    public function render($template, $ttl=null){      
        $cachefileName =  md5( $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        if( $this->config->cacheAdapter->hasCachedItem($cachefileName, $ttl) ){    
            //echo 'Page is from cache. ';
            $html = $this->config->cacheAdapter->getCachedItem($cachefileName);   
            $responseObj = $this->view->render($this->httpResponse, 'cache_stub', array() );   
            $responseObj->write($html);
        }else{   
            //echo 'Page is fresh item. ';  
            $responseObj = $this->view->render($this->httpResponse, $template, $this->data);   
            $html = (string)$responseObj->getBody();     
            $this->config->cacheAdapter->setCachedItem($cachefileName, $html, $ttl);  
        }  
        return $responseObj;            
    }   
*/

class SimpleCacheAdapter  implements CacheAdapterIF{        
    
    var $config = array();
    var $cachedir = '';    
    //var $debug = false; // the debug flag is in the  $$this->config['debug'] (in project/infrastructure/config/simplecacheConfig)

    //_____________________________________________________________________________________________
    public function __construct($cacheConfigFile, $cacheDir){
        $this->setConfig($cacheConfigFile, $cacheDir);      
        $this->init();     
    }      
    
    //_____________________________________________________________________________________________
    private function setConfig($cacheConfigFile, $cacheDir){
        include $cacheConfigFile;
        $this->config = $cacheConfigProperties;      
        $this->cachedir = $cacheDir;
    }       
    
    
    //_____________________________________________________________________________________________
    private function init(){     
       	if(!is_dir($dir = $this->cachedir)){       	    
			mkdir($dir);  
       	}
    }    
    

    //_____________________________________________________________________________________________
    public function hasCachedItem($key, $ttl=null){         
        if($this->config['caching'] == FALSE){  // caching is desabled
            return FALSE;
        }  
        $hashedFilename = $this->hashFilename($key);
        $cachefile = $this->cachedir . '/'. $hashedFilename;     
        //echo $ttl . ' - ';
        if($ttl === null){
            $ttl = $this->config['ttl'];
        }           
        if($ttl === 0){     
            if($this->config['debug']) echo ' Page wil not be cached. ';    
            return FALSE;
        }
        if (file_exists($cachefile)){     
            $sec = filemtime($cachefile) - (time() - $ttl);   
        }else{
            $sec = 0;
        }  
        
        if($this->config['debug']){
            echo 'ttl = ' . $ttl . ' - ';
            if ($sec > 0){
                printf(' This cached page is  %s seconds old.' , $ttl - $sec);    
                printf(' Wait %s seconds for a refreshed page.' , $sec);
            }     
            if (file_exists($cachefile)){  
                echo ' Page is present in cache ';
                if( time() - $ttl < filemtime($cachefile) ){
                    echo ' and is not expired. ';
                }else{
                    echo ' but is expired. ';
                }
            }else{
                echo ' Page is not found in cache. ';
            }                
        }
        $result = FALSE;
        if (file_exists($cachefile)){  
            if( time() - $ttl < filemtime($cachefile) ){
                $result = TRUE;   
            }else{
                $result = FALSE;                     
            }
        }else{
            $result = FALSE;
        }   
        return $result;
    }
    //_____________________________________________________________________________________________
    public function getCachedItem($key){     
        $hashedFilename = $this->hashFilename($key);  
        return $this->getFileContent($this->cachedir . '/'. $hashedFilename);          
    }     
    
    //_____________________________________________________________________________________________
    /*
    * $ttl : cache time in seconds
    * if ttl === null, the config value is used, 
    * if ttl === 0, the cache is disabled, 
    * if ttl > 0 the page is cached for ttl seconds
    */
    public function setCachedItem($key, $data, $ttl=null){     
        // if ttl === 0, then don`t cache
        if($ttl === 0){   
            return;
        }    
        //if($ttl === null){     $ttl = $this->config['ttl'];  }  
        $cachefile = $this->cachedir . '/'. $this->hashFilename($key);      
        $this->storeFileContent($cachefile, $data . "\n<!--  $key  -->\n"  );  
    }      
    
    
    //_____________________________________________________________________________________________
    private function hashFilename($filename){
         $cachefileName =  md5( $filename );     
         return $cachefileName;   
    }
    
    //_____________________________________________________________________________________________
	private function storeFileContent($file, $data)
	{
		
		if(file_exists($file)){		    
			unlink($file);
		}
		
		$return = FALSE;
		// Lock file, ignore warnings as we might be creating this file
		$fpt = @fopen($file, 'rb');
		@flock($fpt, LOCK_EX);

		// php.net suggested I should use wb to make it work under Windows
		$fp=fopen($file, 'wb+');
		if(!$fp){
			// Strange! We are not able to write the file!
			//Zoo\Cache::log("Failed to open for write of $file");
		} else {
			fwrite($fp, $data, strlen($data));
			fclose($fp);
			$return = TRUE;
			//Zoo\Cache::log("Wrote cache to file: $file");
		}
		// Release lock
		@flock($fpt, LOCK_UN);
		@fclose($fpt);
		return $return;
	}             

 
    //_____________________________________________________________________________________________
	private function getFileContent($file)
	{		
		// Open file
		if(($fp = @fopen($file, 'rb')) === FALSE){
			return FALSE;
		}
		// Get a shared lock
		flock($fp, LOCK_SH);
		$data = file_get_contents($file);
		// Release lock
		flock($fp, LOCK_UN);
		fclose($fp);
		return $data;
	}         
        

 
}
