<?php namespace gracian_system\infrastructure\responseBag;

use gracian_system\application\infrastructurePorts\ResponseBagIF;     

class ArrayResponseBag implements ResponseBagIF {
    public $name = 'ArrayResponseBag';   
    private $response;
    // private entityData



    public function set($key , $value){   
       $this->response[$key] = $value;
    }                                

    public function getResponse(){
        return $this->response;
    }       
    


}