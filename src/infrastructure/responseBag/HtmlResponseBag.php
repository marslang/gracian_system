<?php namespace gracian_system\infrastructure\responseBag;

use gracian_system\application\infrastructurePorts\ResponseBagIF;

class HtmlResponseBag implements ResponseBagIF {
    public $name = 'HtmlResponseBag';
    public $response;
 
    /**
    * wordt gebruikt in de gracian_admin Controllers
    */
    public function hasValidationErrors(){   
        if(isset($this->response['validationErrors'])){
            return TRUE;
        }
        return FALSE;
    } 

    //___________________________________________________________________________________
    /**
    * wordt gebruikt in AuthenticationController
    */
    public function hasLoginError(){   
        if(isset($this->response['loginError'])){
            return TRUE;
        }
        return FALSE;
    }      
      
    //___________________________________________________________________________________
    public function appendToFlashMessages($key , $value){
        if(!isset($this->response['flashMessages'])){ 
            $this->response['flashMessages'] = array();
        }
        if(!isset($this->response['flashMessages'][$key])){ 
            $this->response['flashMessages'][$key] = array();
        }
        $this->response['flashMessages'][$key][] =  $value;
    }       
    
    //___________________________________________________________________________________    
    /**
    * wordt gebruikt in de gracian_admin/Controllers
    */
    public function hasFlashMessages($type){
        if(isset($this->response['flashMessages'][$type])
        && count($this->response['flashMessages'][$type]) > 0){
            return TRUE;
        }
        return FALSE;
    }         

    //___________________________________________________________________________________
    public function get($key){
        if(isset($this->response[$key] )) {
            return $this->response[$key];
        }
        return null;
    }

    //___________________________________________________________________________________
    public function set($key , $value){
       $this->response[$key] = $value;
    }

    //___________________________________________________________________________________
    public function transformOut($transformType = 'default'){          
        //print_r( $this->response);
        $result = null;       
        /*
        if($transformType == 'site'){    
            $response = (object)  $this->response;  
            $result['login'] = $response->login;    
            $result['messages'] = $response->messages;
            $result['current']['nodeName'] = $response->itemNodeData->nodeName;  
            $result['current']['permission'] = $response->itemNodeData->item['permission'];    
            $result['current']['item'] = $response->itemNodeData->item;   
            foreach($result['current']['item']['variable_fields'] as $k => $v){
                $result['current']['item'][$k] = $v;
            }
            unset($result['current']['item']['orphan_fields']);
            unset($result['current']['item']['formdate_publish_start_date']);
            unset($result['current']['item']['variable_fields']);
            $result['current']['crumbItem'] = $response->itemNodeData->crumbItem;
            $result['crumbPath'] = $response->itemNodeData->crumb;   
            $result['siblings'] = $response->itemNodeData->siblingItems;
            //foreach($response->itemNodeData->siblingItems as $k => $v){   
            //    $result['siblings'][$k]['item'] = $v;
            //}
            $result['children'] = array();
            foreach($response->itemNodeData->childrenCollection as $k => $v){
                 $result['children'][$k] = array();
                 foreach($v->siblingItems as $kk => $vv){
                     $result['children'][$k][$kk]['id']  = $vv['id'];
                     $result['children'][$k][$kk]['publish'] = $vv['publish'];
                     $result['children'][$k][$kk]['title'] = $vv['title'];    
                     $result['children'][$k][$kk]['node_name'] = $vv['node_name'];   
                     foreach($vv['variable_fields'] as $kkk => $vvv){
                         $result['children'][$k][$kk][$kkk] = $vvv;
                     }   
                     $result['children'][$k][$kk]['permission'] = $vv['permission']; 
                     $result['children'][$k][$kk]['file_url'] = $vv['file_url']; 
                 }
            }  
            //foreach($response->tree->childrenCollection as $k => $v){
            $result['tree'] = $response->itemNodeData->tree;
            
        }else{  
        */
            $result = (object) $this->response;  
        //}
        return $result;
    }


}
