<?php  namespace gracian_system\infrastructure\responseBag;

use gracian_project\application\service\ConfigFactory;

/**
 * This file contains the response factory.
 *
 * @package    gracian_system
 * @subpackage infrastructure
 * @author     ml
*/

class ResponseBagFactory{

    /**
     * return a responseBag based in the responseType
     *
     * @param string $responseType
     * @return an implementation of a ResponseBagIF
    */

    public function getResponseBag( $responseType){
            if($responseType == 'html'){   return  new HtmlResponseBag(); }
            if($responseType == 'array'){   return  new ArrayResponseBag();  }
    }


}
