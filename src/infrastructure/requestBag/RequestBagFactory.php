<?php  namespace gracian_system\infrastructure\requestBag;        
      
class RequestBagFactory{       
    
    public function getRequestBag( $requestType ){    
            if($requestType == 'html'){   
                return  new HtmlRequestBag(); 
            }       
            if($requestType == 'array'){   
                return  new ArrayRequestBag();  
            }
    }       
 
  
}