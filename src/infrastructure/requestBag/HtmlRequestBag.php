<?php namespace gracian_system\infrastructure\requestBag;

use gracian_system\application\infrastructurePorts\RequestBagIF;
use gracian_system\infrastructure\helpers\FormdateHelper;

class HtmlRequestBag implements RequestBagIF {
    public $name = 'HtmlRequestBag';
    public $request = array(); 

    //______________________________________________________
    public function transformIn($request){    
        // NOTE: transform the formdate-* fields back to a single date field         
        $the_array = $request;
        $cleaned_array = array();
        $dates = array(); 
        foreach($the_array as $k => $v){       
            // its a get or a post 
            if(is_array($v)){    
                 foreach($v as $k2 => $v2){
                    if(strpos($k2, 'formdate') === FALSE){
                        // if key k2 is not formdate-* then save it in the cleaned array
                        $cleaned_array[$k][$k2] = $v2;
                    }else{     
                        // yes key k2 is formdate-* with value ( * = day, month, year, hour or minute)   
                        $this->transformFormdate($k2, $v2, $dates);
                    }
                }    
            }else{
                $cleaned_array[$k][$k2] = $v2;
            }
        }    
        if(count($dates) > 0){            
            foreach($dates as $k => $v){
                $cleaned_array['post'][$k] = $v['year'] . '-' . $v['month'] . '-' . $v['day'] . ' ' . $v['hour'] . ':' . $v['minute'] . ':00';
            }
        }

        // remove the fields: token , submit
        if(isset($cleaned_array['post']['_token'])){
            unset($cleaned_array['post']['_token']);
        }
        if(isset($cleaned_array['post']['submit'])){
            unset($cleaned_array['post']['submit']);
        }

        // check the   publish
        $this->request = $cleaned_array; 
    }

    //______________________________________________________
    private function transformFormdate($k2, $v2, &$dates){
        $pieces = explode('-', $k2);
        if(count($pieces) == 3){
            if(!isset($dates[$pieces[1]])){
                $dates[$pieces[1]] = array();
            }
            $dates[$pieces[1]][$pieces[2]] = $v2;
        }         
    }
    
    //______________________________________________________
    public function transformOut(){
       return $this->request;
    }

    //______________________________________________________
    public function hasFileToUpload(){    
        $result = FALSE;
        if(isset($this->request['files']['fileToUpload'])  && count($this->request['files']['fileToUpload']) > 0  && $this->request['files']['fileToUpload']['size'] > 0) {   
            $result = TRUE; 
        }
        return $result; 
    }

}
