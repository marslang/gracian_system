<?php namespace gracian\infrastructure\requestBag;

use gracian_system\application\infrastructurePorts\RequestBagIF;      
use gracian_system\infrastructure\helpers\FormdateHelper;      

/* TODO:
* maak ArrayRequestBag gelijk aan HtmlRequestBag
* maak RequestBagIF gelijk aan
*/

class ArrayRequestBag implements RequestBagIF {    
    public $name = 'ArrayRequestBag';         
    public $request = array(); 

    public function __construct($request) {    
        $this->request = $request; 
        $this->transform();   
    }     
    
    private function transform(){    
        if(isset($this->request['post']['_token'])){
            unset($this->request['post']['_token']);
        } 
        if(isset($this->request['post']['submit'])){
            unset($this->request['post']['submit']);
        }       
    }  
    
    public function getRequest(){    
       return $this->request;
    }

}