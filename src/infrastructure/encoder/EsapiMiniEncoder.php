<?php namespace gracian_system\infrastructure\encoder;    

//use gracian_project\application\service\ConfigFactory;
//use generalTools; 
use gracian_system\domain\service\LambdaArrayRecursive;     
use gracian_system\application\infrastructurePorts\EncoderIF;    
use gracian_system\domain\exceptions\GracianIntrusionException;    

//require_once realpath(__DIR__ . '/../../../../gracian_lib/custom/esapiMini/EncoderProxy.php');

use esapimini\EncoderProxy;

class EsapiMiniEncoder implements EncoderIF
{       
    private $encoder = null;
    private $error_list = null;   
    public $data = null;            
    
    //_____________________________________________________________________________________________
    public function __construct(){      

        $this->encoderProxy = new EncoderProxy();                

        //$configFactory = ConfigFactory::Instance();
        //$this->config = ConfigFactory::Instance()->config;         
        //require_once($this->config['gracian LibPath'] . "/generalTools/autoload.php");        
        //$this->lambdaArrayRecursive = new \generalTools\LambdaArrayRecursive();       
        
    }        
    
    //_____________________________________________________________________________________________
    public function canonicalizeArray($inputArray){      
        //print_r($inputArray);
        $lambdaFunc = function($arg1) {   
            return $this->canonicalize($arg1); 
        };      
        $lar = new LambdaArrayRecursive($inputArray , $lambdaFunc);    
        $result = $lar->parseArray();     
        //print_r($result);
        return $result;
    }  
    
    //_____________________________________________________________________________________________   
    /*
    * trim de value ook even
    */
    public function canonicalize($input){   
        $result = $this->encoderProxy->canonicalize($input);   
        if(isset($result['error'])){
           throw new GracianIntrusionException($result['error']['userMessage'], $result['error']['logMessage']);
        }else{
           return trim($result['input']);
        }     
    }
      
    
    /*
    //_____________________________________________________________________________________________
    public function canonicalizeArrayOld($inputArray){    
        //echo 'zzz';  
        //print_arr($inputArray); exit();
        
        foreach($inputArray as $k => $v) // 1
        {    
            if(is_array($inputArray[$k])) // 2
            { 
                if(count($inputArray[$k]) > 0) // 3
                {  
                    foreach($inputArray[$k] as $k2 => $v2) // 4
                    {  
                        if(is_array($inputArray[$k][$k2]))  // 5
                        { 
                            if( count($inputArray[$k][$k2]) > 0 ) // 6
                            {    
                                foreach($inputArray[$k][$k2] as $k3 => $v3) // 7
                                {   
                                    if(is_array($inputArray[$k][$k2][$k3])) // 8
                                    {   
                                        if( count($inputArray[$k][$k2][$k3]) > 0) // 9
                                        {    
                                            foreach($inputArray[$k][$k2][$k3] as $k4 => $v4) // 10
                                            {  
                                                $inputArray[$k][$k2][$k3][$k4] = $this->canonicalize($inputArray[$k][$k2][$k3][$k4]);  
                                            }  // 10
                                        } // 9 if   
                                    }  // 8 if 
                                    else  // 8a
                                    {
                                        $inputArray[$k][$k2][$k3] = $this->canonicalize($inputArray[$k][$k2][$k3]);      
                                    } // 8a  else
                                } // 7 foreach
                            } // 6 if count 
                        } // 5 is_array
                        else  // 5a
                        {
                            $inputArray[$k][$k2] = $this->canonicalize($inputArray[$k][$k2]);
                        } // 5a 
                    } // 4 foreach                    
                } // 3 if count
            }  // 2 is array
            else  // 2a 
            {
                $inputArray[$k] = $this->canonicalize($inputArray[$k]);
            } // 2a if(is_array($inputArray[$k]))
        } // 1 foreach($inputArray as $k => $v) 
        return $inputArray;
    }
    */
    /*
    public function getErrorList(){
        
    }  
    */  
    
}