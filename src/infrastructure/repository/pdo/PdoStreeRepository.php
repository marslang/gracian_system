<?php namespace gracian_system\infrastructure\repository\pdo;

use gracian_system\domain\infrastructurePorts\StreeRepositoryIF;
use gracian_project\application\service\ConfigFactory;     
use gracian_system\domain\exceptions\GracianException;


/*
always return an Entity Object so the interactors can work with it.
*/

class PdoStreeRepository extends PdoBaseRepository implements StreeRepositoryIF {

    //_____________________________________________________________________________________________
    public function getItemField($fieldname, $id){
        $record = array();
        $sql="SELECT :fieldname FROM stree WHERE id=:id";
        $stmt = $this->pdo->prepare($sql);
        // bindParam: Automatically sanitized by PDO
        $stmt->bindParam(':fieldname',$fieldname,\PDO::PARAM_STR);
        $stmt->bindParam(':id',$id,\PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        if(count($result) > 0){
            $record = $result[0];
        }else{
            throw new GracianException('No item found with this id');
        }
        return $record[$fieldname];
    }

    //_____________________________________________________________________________________________
    public function getItem($id, $publish=false){
        $record = array();
        if($publish){
            $sql="SELECT * FROM stree WHERE id=:id AND publish=1";
        }else{
            $sql="SELECT * FROM stree WHERE id=:id ";
        }
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id',$id,\PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        if(count($result) > 0){
            $record = $result[0];
        }else{
            throw new GracianException('b No item found with this id ' . $id );
        }
        return $record;
    }

    //_____________________________________________________________________________________________
    public function getItemForCrumb($id){
        $record = array();
        $sql="SELECT id, parent_id, title FROM stree WHERE id=:id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id',$id,\PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        if(count($result) > 0){
            $record = $result[0];
        }else{
            throw new GracianException('c No item found with this id ' . $id);
        }
        return $record;
    }

    //_____________________________________________________________________________________________
    public function getItemsOfParentOfNode($id, $nodeName, $publish=false){
       return $this->listOfNodeOfParent($nodeName, $id, $publish);
    }

    //_____________________________________________________________________________________________
    public function listOfNodeOfParent($nodeName, $id, $publish=false){
        $list = array();
        if($publish){
            $sql="SELECT * FROM stree WHERE node_name=? AND parent_id=? AND publish=1";
        }else{
            $sql="SELECT * FROM stree WHERE node_name=? AND parent_id=?";
        }
        $q = $this->pdo->prepare($sql);
        $q->execute(array($nodeName, $id));
        $q->setFetchMode(\PDO::FETCH_ASSOC);
        while($r = $q->fetch()){
          $list[] = $r;
        }
        return $list;
    }

    //_____________________________________________________________________________________________
    public function listChildrenOfParent($parent_id){
        $list = array();
        $sql="SELECT * FROM stree WHERE parent_id=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($parent_id));
        $q->setFetchMode(\PDO::FETCH_ASSOC);
        // fetch
        while($r = $q->fetch()){
          $list[] = $r;
        }
        return $list;
    }

    //_____________________________________________________________________________________________
    public function countChildrenOfParent($parent_id){
        $list = array();
        $sql="SELECT id FROM stree WHERE parent_id=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($parent_id));
        $q->setFetchMode(\PDO::FETCH_ASSOC);
        // fetch
        while($r = $q->fetch()){
          $list[] = $r;
        }
        return count($list);
    }
 

    //_____________________________________________________________________________________________
    public function store($fields_to_store){
        $paramsNameValues = array();
        foreach($fields_to_store as $k => $v){ 
            $paramsNameValues[':'.$k] = $v;
        }

        $paramNames_arr = array();
        foreach($fields_to_store as $k => $v){ 
            $paramNames_arr[] = ':'.$k;
        }
        $paramNames = implode(',', $paramNames_arr);

        $fieldNames_arr = array();
        foreach($fields_to_store as $k => $v){ 
            $fieldNames_arr[] = $k;
        }
        $fieldNames = implode(',', $fieldNames_arr);

        try {

            $sql = "INSERT INTO stree ({$fieldNames}) VALUES ({$paramNames})";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($paramsNameValues);
            $id = $this->pdo->lastInsertId();
            $this->updateFieldForId($id, 'order_nr', $id);
            return $id;
        }catch(PDOException $e) {     
            throw new GracianException(
                'An Error occurred in PdoStreeRepository::store() ' . E_USER_ERROR,  
                'Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR
            ); 
            // "Tip: check the table fields. maybe there is a new field in the sql statement that was not yet created in the table.";exit();
        }
    }
 
    //_____________________________________________________________________________________________
    public function update($id, $fields_to_store){
        $names_arr = array();
        foreach($fields_to_store as $k => $v){ 
            $names_arr[] = $k . ' = ? ';
        }
        $names_str = implode(', ', $names_arr);

        $values_arr = array();
        foreach($fields_to_store as $k => $v){ 
            $values_arr[] = $v; 
        }
        $values_arr[] = $id;
        try{
            $sql = "UPDATE stree SET  {$names_str} WHERE id = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($values_arr);
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
        catch(GracianException $e) {
            echo ' Error: ' . $e->getMessage();
        }
    }

    //_____________________________________________________________________________________________
    public function updateFieldForId($id, $fieldName, $fieldValue){
        try{
            $sql = "UPDATE stree SET
            $fieldName = ?
            WHERE id = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array(
                $fieldValue,
                $id
              )
            );
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
    }

    //_____________________________________________________________________________________________
    public function destroy($id){
        $sql = "DELETE FROM stree WHERE id =  :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
        $stmt->execute();
    }

}
