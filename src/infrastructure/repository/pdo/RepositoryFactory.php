<?php  namespace gracian_system\infrastructure\repository\pdo;

use gracian_system\infrastructure\repository\pdo\PdoStreeRepository;
use gracian_system\infrastructure\repository\pdo\PdoUserRepository;    
use gracian_system\infrastructure\repository\pdo\PdoForgotpasswordRepository;
use gracian_system\infrastructure\repository\pdo\PdoTagRepository;
use gracian_project\application\service\ConfigFactory;

class RepositoryFactory{

    public function getRepository( $repositoryName){

//            if($repositoryName == 'stree'){   return  new PdoStreeRepository($dbConn);  }

            if($repositoryName == 'stree'){ return  new PdoStreeRepository(); }

            if($repositoryName == 'user'){   return  new PdoUserRepository();  }
               
            if($repositoryName == 'forgotpassword'){   return  new PdoForgotpasswordRepository();  }

            if($repositoryName == 'tag'){   return  new PdoTagRepository();  }

    }


}
