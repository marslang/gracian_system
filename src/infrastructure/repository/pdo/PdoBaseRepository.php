<?php namespace gracian_system\infrastructure\repository\pdo;  

use gracian_system\domain\infrastructurePorts\BaseRepositoryIF;  
use PDO;            
use gracian_system\infrastructure\helpers\dbHelper;          
use gracian_project\application\service\ConfigFactory;         

/*
always return an Entity Object so the interactors can work with it.
*/

class PdoBaseRepository implements BaseRepositoryIF {   
    
    protected $pdo = null;      
    protected $connection = null;     
    
    //_____________________________________________________________________________________________      
    public function __construct(){      
        $this->config = ConfigFactory::Instance()->config;         
        $this->pdo = $this->config->dbConn;     
    }      
         
    //_____________________________________________________________________________________________      
    public function truncateTestTable($tableName){
        if($this->config->env == 'test' && $this->isTestDb()){
            $sql = 'TRUNCATE ' . $tableName; 
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();              
        }
    }    
    
    //_____________________________________________________________________________________________      
    private function isTestDb(){     
        $pos = strpos($this->config->db['name'], '-test');     
        if($pos === false ){
            // bevat geen string -test 
            return false; 
        } else {
            //  bevat de string -test 
            return true; 
        }  
    }     
   
  


}     
  