<?php namespace gracian_system\infrastructure\repository\pdo;

use gracian_system\domain\infrastructurePorts\BaseRepositoryIF;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianValidationException;
use PDO;
use gracian_system\infrastructure\helpers\dbHelper;


/*
always return an Entity Object so the interactors can work with it.
*/

class PdoUserRepository extends PdoBaseRepository {

    private $table = 'user';






        //_____________________________________________________________________________________________
    public function listAll(){
        $list = array();
        $sql="SELECT * FROM user ";
        $q = $this->pdo->prepare($sql);
        $q->execute();
        $q->setFetchMode(\PDO::FETCH_ASSOC);
        // fetch
        while($r = $q->fetch()){
          $list[] = $r;
        }
        return $list;
    }


    //_____________________________________________________________________________________________

    public function readPasswordByUsername($username){
        // hash = *; // In case the user is not found
        $hashedPw = '*';

        $sql="SELECT password FROM {$this->table} WHERE username=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($username));
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $result = $q->fetchAll();
        unset($q);
        if(count($result) > 0){
            $hashedPw = $result[0]['password'];
        }
        return $hashedPw;
    }


    //_____________________________________________________________________________________________
    public function readPasswordByEmail($email){
        // hash = * // In case the user is not found
        $hashedPw = '*';
        $sql="SELECT password FROM {$this->table} WHERE email=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($email));
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $result = $q->fetchAll();
        unset($q);
        if(count($result) > 0){
            $hashedPw = $result[0]['password'];
        }
        return $hashedPw;
    }

    //_____________________________________________________________________________________________
    public function readByUsernamePassword($username, $password){

        $record = array();
        $sql="SELECT id, fullname, username, role, email, use_expire_date, expire_date FROM {$this->table} WHERE username=? AND password=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($username, $password));
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $result = $q->fetchAll();
        unset($q);
        if(count($result) > 0){
            return $result;
        }
        return $record;
    }

    //_____________________________________________________________________________________________
    public function readByEmailPassword($email, $password){
        $record = array();
        $sql="SELECT id, fullname, email, role, fullname, use_expire_date, expire_date  FROM {$this->table} WHERE email=? AND password=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($email, $password));
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $result = $q->fetchAll();
        unset($q);
        if(count($result) > 0){
            return $result;
        }
        return $record;
    }

    //_____________________________________________________________________________________________
    /*
    TODO:
    bouw procondition in:
    ik had in de Mysql db een veld toegevoegd (subdir)
    een dag later wilde ik de unit tests weer draaien maar kreeg een algemene foutmelding:
    Fatal error: Call to a member function execute() on boolean in /home/vagrant/Code/Laravel/gracian/infrastructure/pdoDB/StreePdoRepository.php on line 413

    Tip: check the table fields. maybe there is a new field in the sql statement that was not yet created in the table.

    */






    //_____________________________________________________________________________________________
    /*
    How to close a PDO handle
    Actually you have to unset/nullify not only the db handle, but any statement handles too.
    Indeed, because those objects reference the DB handle.
    true story, found that one out the hard way :)
    Oh, and another gotcha: you have to nullify the pointer in all scopes!

    */
    function __destruct(){
        unset($this->pdo);
    }



    //_____________________________________________________________________________________________
    public function store($item){
        try {
            // PDO query
            $sql = "INSERT INTO {$this->table} (
                id,
                username,
                email,
                password,
                fullname,
                role,
                remember_token,
                creation_date,
                modify_date,
                expire_date,
                use_expire_date) VALUES (
                    :id,
                    :username,
                    :email,
                    :password,
                    :fullname,
                    :role,
                    :remember_token,:creation_date,:modify_date,:expire_date,:use_expire_date)";
            $stmt = $this->pdo->prepare($sql);

            $stmt->execute(array(':id'=>NULL,
                                 ':username'=>$item['username'],
                                 ':email'=>$item['email'],
                                 ':password'=>$item['password'],
                                 ':fullname'=>$item['fullname'],
                                 ':role'=>$item['role'],
                                 ':remember_token'=>$item['remember_token'],
                                 ':creation_date'=>$item['creation_date'],
                                 ':modify_date'=>$item['modify_date'],
                                 ':expire_date'=>$item['expire_date'],
                                 ':use_expire_date'=>$item['use_expire_date']
                                 ));

        }catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
        // return  the id
        return $this->pdo->lastInsertId();
    }

    //_____________________________________________________________________________________________
    public function read($id){
        $record = array();
        $sql="SELECT * FROM {$this->table} WHERE id=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($id));
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $result = $q->fetchAll();
        unset($q);
        if(count($result) > 0){
            $record = $result[0];
        }else{
            throw new GracianException('No user found with this id');
        }
        return $record;
    }

    //_____________________________________________________________________________________________
    public function updateExcludingPassword($item){

        try{
            $sql = "UPDATE {$this->table} SET
            username = ? ,
            email = ?,
            modify_date = ?,
            expire_date = ?,
            fullname = ? ,
            role = ? ,
            use_expire_date = ?
            WHERE id = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array(
              $item['username'],
              $item['email'],
              $item['modify_date'],
              $item['expire_date'],
              $item['fullname'],
              $item['role'],
              $item['use_expire_date'],
              $item['id']
            ));
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        } catch(\Exception $e){
          if($this->doesEmailExist($item['email'])){
            throw new GracianException( 'This email address is already taken' );
          }else{
            throw new GracianException( $e->getMessage() );
          }
        }
    }

    //_____________________________________________________________________________________________
    private function doesEmailExist($email){
      $sql="SELECT * FROM {$this->table} WHERE email=?";
      $q = $this->pdo->prepare($sql);
      $q->execute(array($email));
      $q->setFetchMode(PDO::FETCH_ASSOC);
      $result = $q->fetchAll();
      unset($q);
      if(count($result) > 0){
          return TRUE;
      }
      return FALSE;
    }


    //_____________________________________________________________________________________________
    public function updateIncludingPassword($item){
        try{
            $sql = "UPDATE {$this->table} SET
            username = ? ,
            email = ?,
            password = ?,
            modify_date = ?,
            expire_date = ?,
            fullname = ? ,
            role = ? ,
            use_expire_date = ?
            WHERE id = ?";     
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array(
              $item['username'],
              $item['email'],
              $item['password'],
              $item['modify_date'],
              $item['expire_date'],
              $item['fullname'],
              $item['role'],
              $item['use_expire_date'],
              $item['id']
            ));
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }catch(\Exception $e){
          throw new GracianValidationException( $e->getMessage() );
        }
    }


    //_____________________________________________________________________________________________
    public function updatePasswordForId($id, $password){

        try{
            $sql = "UPDATE {$this->table} SET  password = ?  WHERE id = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array($password,  $id));
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
    }


    //_____________________________________________________________________________________________
    public function destroy($id){
        $sql = "DELETE FROM {$this->table} WHERE id = :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }


    //_____________________________________________________________________________________________
    public function countRecordsWithSameEmail($email){
        $sql = "SELECT id  FROM {$this->table} WHERE email = :email ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        return count($result);
    }

    //_____________________________________________________________________________________________
    public function fetchIdWithEmail($email){
        $sql = "SELECT id  FROM {$this->table} WHERE email = :email ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);

        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        return $result[0]['id'];
    }


    //_____________________________________________________________________________________________
    public function fetchOthersWithUsername($id, $username){
        $sql = "SELECT id  FROM {$this->table} WHERE username = :username  AND id != :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        return $result;
    }

}
