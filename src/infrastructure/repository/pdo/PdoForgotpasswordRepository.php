<?php namespace gracian_system\infrastructure\repository\pdo;

use gracian_system\domain\infrastructurePorts\ForgotpasswordRepositoryIF;  
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianErrorException;
use gracian_system\domain\exceptions\GracianValidationException;
use PDO;
use gracian_system\infrastructure\helpers\dbHelper;


/*
always return an Entity Object so the interactors can work with it.
*/

class PdoForgotpasswordRepository extends PdoBaseRepository implements ForgotpasswordRepositoryIF{

    private $table = 'forgotpassword';


    public function cleanup(){
        $sql = "DELETE FROM {$this->table} WHERE creation_date < NOW() - INTERVAL 1 DAY ";     
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();             
    }

    public function store($id_hash, $now ,$userId){

        try {
            $sql = "INSERT INTO {$this->table} (id_hash, creation_date, user_id)
                    VALUES (:id_hash,:creation_date,:user_id)";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array(':id_hash'=>$id_hash,':creation_date'=>$now,':user_id'=>$userId));

        }catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
    }


    //_____________________________________________________________________________________________
    public function fetchWithHash($hash){
        $sql = "SELECT *  FROM {$this->table} WHERE id_hash = :id_hash ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id_hash', $hash, PDO::PARAM_STR);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        if(count($result) > 0){
            return $result[0];
        }else{
            throw new GracianErrorException('No record found with this token. Go to the login page and follow the forgot passsword process again.');
        }
    }


    //_____________________________________________________________________________________________
    public function destroy($id_hash){
        $sql = "DELETE FROM {$this->table} WHERE id_hash = :id_hash";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id_hash', $id_hash, PDO::PARAM_STR);
        $stmt->execute();
    }

    //_____________________________________________________________________________________________
    public function destroyItemsOfId($user_id){
        $sql = "DELETE FROM {$this->table} WHERE user_id = :user_id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->execute();
    }



}
