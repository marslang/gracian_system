<?php namespace gracian_system\infrastructure\repository\pdo;

use gracian_system\domain\infrastructurePorts\BaseRepositoryIF;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\infrastructure\helpers\dbHelper;
use PDO;


/*
always return an Entity Object so the interactors can work with it.
*/

class PdoTagRepository extends PdoBaseRepository {

    private $table = 'tag';

    //_____________________________________________________________________________________________
    public function listAll(){
        $list = array();
        $sql="SELECT * FROM {$this->table} ";
        $q = $this->pdo->prepare($sql);
        $q->execute();
        $q->setFetchMode(\PDO::FETCH_ASSOC);
        while($r = $q->fetch()){
          $list[] = $r;
        }
        return $list;
    }
      
    //_____________________________________________________________________________________________         
    // TODO: Deze kan naar de PdoBaseRepository
    public function updateFieldForId($id, $fieldName, $fieldValue){
        try{
            $sql = "UPDATE {$this->table} SET
            $fieldName = ?
            WHERE id = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array(
                $fieldValue,
                $id
              )
            );
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
    }    



    //_____________________________________________________________________________________________
    public function store($item){

        try {
            $sql = "INSERT INTO {$this->table} (
                id,
                name
                ) VALUES (
                    :id,
                    :name
                )";
            $stmt = $this->pdo->prepare($sql);

            $stmt->execute(array(':id'=>NULL,
                                 ':name'=>$item['name']
                                 ));

        }catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
        return $this->pdo->lastInsertId();
    }       
    
    //_____________________________________________________________________________________________
    public function fetchOthersWithName($id, $name){
        $sql = "SELECT id  FROM {$this->table} WHERE name = :name  AND id != :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();
        unset($stmt);
        return $result;
    }       

    //_____________________________________________________________________________________________
    public function read($id){
        $record = array();
        $sql="SELECT * FROM {$this->table} WHERE id=?";
        $q = $this->pdo->prepare($sql);
        $q->execute(array($id));
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $result = $q->fetchAll();
        unset($q);
        if(count($result) > 0){
            $record = $result[0];
        }else{
            throw new GracianException('No tag found with this id');
        }
        return $record;
    }   
    
    //_____________________________________________________________________________________________
    public function update($item){

        try{
            $sql = "UPDATE {$this->table} SET
            name = ? 
            WHERE id = ?";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute(array(
              $item['name'],
              $item['id']
            ));
        } catch(PDOException $e) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $e->getMessage(), E_USER_ERROR);
        }
    }   
    
    //_____________________________________________________________________________________________
    public function destroy($id){
        $sql = "DELETE FROM {$this->table} WHERE id = :id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }   
              
    //_____________________________________________________________________________________________
    public function destroyRelations($tag_id){
        $sql = "DELETE FROM tag_relation WHERE tag_id = :tag_id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':tag_id', $tag_id, PDO::PARAM_INT);
        $stmt->execute();
    }        
    
    //_____________________________________________________________________________________________
    public function countRelations($tag_id){
        $sql = "SELECT count(*) FROM tag_relation WHERE tag_id = :tag_id";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':tag_id', $tag_id, PDO::PARAM_INT);
        $stmt->execute();   
        // return the number of rows 
        return $stmt->fetchColumn(); 
    }         
         
}
