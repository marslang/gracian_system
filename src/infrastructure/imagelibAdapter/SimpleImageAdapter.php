<?php namespace gracian_system\infrastructure\imagelibAdapter;    

//require_once realpath(__DIR__ . '/../../../../gracian_lib/custom/SimpleImage/src/abeautifulsite/SimpleImage.php');

use gracian_system\domain\infrastructurePorts\ImagelibAdapterIF;    
use abeautifulsite\SimpleImage;

class SimpleImageAdapter implements ImagelibAdapterIF
{       
    private $imageLib = null;
    
    //_____________________________________________________________________________________________
    public function resize($imageFilenameSrc, $imageFilenameDst, $width, $height){      
        //echo 'aaa';
        //try {
            $img = new SimpleImage($imageFilenameSrc);
            //$img->flip('x')->rotate(90)->best_fit(320, 200)->sepia()->save($imageFilenameDst);
            $img->best_fit($width, $height)->save($imageFilenameDst);
        //} catch(Exception $e) {
        //    echo 'Error: ' . $e->getMessage();
        //}
    }  
    
}