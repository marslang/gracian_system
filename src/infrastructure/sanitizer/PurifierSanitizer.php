<?php namespace gracian_system\infrastructure\sanitizer;

use gracian_system\application\infrastructurePorts\SanitizerIF;      
use gracian_system\domain\service\LambdaArrayRecursive2Args;     
use brisanitizer\Sanitizer;         

class PurifierSanitizer implements SanitizerIF
{
    /*
    TIP: htmlPurifier has no namespaces, so insert a slash before the class
    */

    //_____________________________________________________________________________________________
    public function __construct( $cache_path, $gracianLibCustomPath){
        //require_once($gracianLibCustomPath . "/brisanitizer/autoload.php");        
        $this->cache_path =  $cache_path;   
        //$this->brisanitizer = new \Brisanitizer\Sanitizer();       
        $this->brisanitizer = new Sanitizer();       
    }     
    
    
    public function sanitize($fields, $schema){        
        
        $fields = $this->whitelistFields($fields, $schema);       
        $sanitizedFields = array();  
          
        $lambdaFunc = function($fieldValue, $sanitizerArray) {       
            if($sanitizerArray == null){
               return $this->sanitizeValue($fieldValue, 'purge'); 
            }else{                
                if(count($sanitizerArray['sanitizers']) > 0){   
                    $newValue = $fieldValue;
                    foreach($sanitizerArray['sanitizers'] as $sanitizerName => $sanitizerArgs) {       
                        $newValue = $this->sanitizeValue($newValue, $sanitizerName);       
                    }
                    return $newValue; 
                }
            }
        };   
        
        if(count($fields) > 0){     
            $lar = new LambdaArrayRecursive2Args($fields, $schema, $lambdaFunc);    
            $sanitizedFields = $lar->parseArray();   
        }  
         
        return $sanitizedFields;
    }     
    
    //_____________________________________________________________________________________________     
    /* 
    * filter out all the fields that are not in the schema (commonFields  )  
    * (variableFields and orphanFields are keys in commonFields)
    */  
    private function whitelistFields($fields, $schema){     
        $whitelistFields = array();
        foreach ($schema as $field_name => $field){        
            if(isset($fields[$field_name])){
                $whitelistFields[$field_name] = $fields[$field_name];        
            }
        }   
        return  $whitelistFields;              
    }      
 

    //_____________________________________________________________________________________________        
    public function sanitizeValue($dirty_input, $rule){     
        // here a triple === otherwise a 0 will be mistaken for a ''          
        if($dirty_input === ''){ 
            return '';
        }     
        if($rule === 'raw'){      
            return $dirty_input;
        }    
        
        $target = $this->getTargetSanitizer($rule);  
        
        if($target['lib'] == 'brisanitizer'){  
            $clean_input = $this->brisanitizer->sanitizeField($dirty_input, $rule);
        }    
                    
        if($target['lib'] == 'purifier'){
            $purifier = new \HTMLPurifier($target['config']);
            $clean_input = $purifier->purify( $dirty_input);
        } 
        
        return $clean_input;            
    }  

    //_____________________________________________________________________________________________
    private function getTargetSanitizer($sanitization_rule){    
        $target = array();
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('Cache.SerializerPath',  $this->cache_path .'/HTMLPurifier/DefinitionCache/Serializer');
        switch ($sanitization_rule)
        {
            // purge and removeAllCssHtml are the same
            case 'purge': 
            case 'escape':
            case 'raw':
            case 'nospaces':
                $target['lib'] = 'brisanitizer';  
                break;          
                                                            
            case 'removeAllCssHtml':   
                $target['lib'] = 'purifier';
                $config->set('Core.Encoding', 'utf-8');
                $config->set('HTML.Allowed', '');
                $config->set('CSS.AllowedProperties', array());
                break;    
                    

            case 'title':
                $target['lib'] = 'purifier';
                $config->set('AutoFormat.AutoParagraph', false);
                $config->set('AutoFormat.Linkify',false);
                $config->set('Core.Encoding', 'utf-8');
                $config->set('HTML.Allowed', 'b,strong,em,i');
                $config->set('CSS.AllowedProperties', array());
                break;

            case 'comment':
                $target['lib'] = 'purifier';
                $config = HTMLPurifier_Config::createDefault();
                $config->set('Core.Encoding', 'utf-8');
                $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
                $config->set('HTML.Allowed', 'p,a[href|title],abbr[title],acronym[title],b,strong,blockquote[cite],code,em,i,strike');
                $config->set('AutoFormat.AutoParagraph', TRUE);
                $config->set('AutoFormat.Linkify', TRUE);
                $config->set('AutoFormat.RemoveEmpty', TRUE);
                break;

            case 'body':
                $target['lib'] = 'purifier';
                $config->set('Core.Encoding', 'utf-8');
                $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
                $config->set('HTML.Allowed', 'p,a[href|title],abbr[title],acronym[title],b,strong,blockquote[cite],code,em,i,br');
                $config->set('AutoFormat.AutoParagraph', TRUE);
                $config->set('AutoFormat.Linkify', TRUE);
                $config->set('AutoFormat.RemoveEmpty', TRUE);
                break;

            case 'embed':
                $target['lib'] = 'purifier';
                //partially clean: yes html, no css  , no script
                $config->set('Core.Encoding', 'utf-8');
                $config->set('HTML.SafeObject', true);
                $config->set('HTML.SafeEmbed', true);
                //allow iframes from trusted sources
                $config->set('HTML.SafeIframe', true);
                //allow YouTube and Vimeo
                $config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%');
                // allow flash
                $config->set('Output.FlashCompat', 'true');
                $config->set('HTML.Allowed', 'div[style],p[style],a[href|title],pre,b,strong,em,i'
                .',table[style],tr[style],td[style],br'
                .',img[alt|src|height|width]'
                .',iframe[src|height|width|frameborder|style]'
                .',object[codebase|width|height|id],param[name|value],embed[src]'
                );
                break;

            case FALSE:
                $target['lib'] = 'purifier';
                // use the default- totally clean: no html, no css
                $config->set('Core.Encoding', 'utf-8');
                $config->set('HTML.Doctype', 'XHTML 1.0 Strict');
                $config->set('HTML.Allowed', '');
                $config->set('CSS.AllowedProperties', array());
                break;

            default:
                echo 'Error: something wrong in PurifierSanitizer.getConfig('.$sanitization_rule.') The rule ' .  $sanitization_rule . ' does not exist.'; 
                exit();
        }  
        
        if($target['lib'] == 'purifier'){ $target['config'] = $config;  }    
        return  $target;

    }   

}
