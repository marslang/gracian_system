<?php namespace gracian_system\application\applicationPorts;

interface DbConnectionServiceIF {

    public function getDbConnection($db);
    
}