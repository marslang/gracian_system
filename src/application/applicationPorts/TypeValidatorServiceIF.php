<?php namespace gracian_system\application\applicationPorts;

interface TypeValidatorServiceIF {
   
    public function isInteger($value);    
    public function isString($value);

}                           