<?php namespace gracian_system\application\applicationPorts;

interface UsecaseIF {
                               
    public function execute($request);
    
}        
