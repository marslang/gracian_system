<?php namespace gracian_system\application\applicationPorts;

interface IdsServiceIF {

    public function idsDetect($request);           
    public function hasIntrusions();
     
}        

