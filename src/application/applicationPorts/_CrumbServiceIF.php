<?php namespace gracian_system\application\applicationPorts;

interface CrumbServiceIF {
    public function getCrumb($node, $id=null);
}