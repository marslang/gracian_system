<?php namespace gracian_system\application\applicationPorts;

interface PermissionServiceIF {

    public function assertPermission($entity , $subaction);
    
}        
