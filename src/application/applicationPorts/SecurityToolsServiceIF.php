<?php namespace gracian_system\application\applicationPorts;

interface SecurityToolsServiceIF {
                               
    public function crypto_rand_secure($min, $max);
    public function getToken($length);
    
}        
           