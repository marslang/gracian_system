<?php namespace gracian_system\application\service;        
                                                      
use gracian_system\application\applicationPorts\ValidationServiceIF;    
//use gracian_system\application\applicationPorts\SanitizationServiceIF;      
use gracian_system\domain\exceptions\GracianValidationException;     
use gracian_system\domain\exceptions\GracianAssertException;     

use Respect\Validation\Exceptions\ValidationExceptionInterface;   


class ValidationService extends BaseService implements ValidationServiceIF  {
    
    public function __construct(){
        parent::__construct();
        $this->validator = $this->config->validator;   
    }        

    //_____________________________________________________________________________________________
    public function validateInputFields($fields, $schema){   
        //precondition: 
        if(is_object($fields)){
            throw new GracianAssertException('preconditionError: ValidationService::validateInputFields($fields, $schema). $fields is an object but should be an array.');
        }   
        $isValid = $this->validator->validate($fields, $schema);   
        if( !$isValid ){     
            //print_arr($this->validator->getValidationErrorMessages()); exit();
            throw new GracianValidationException(json_encode($this->validator->getValidationErrorMessages()));
        }   
        return $isValid;
    }           
   
}
?>
