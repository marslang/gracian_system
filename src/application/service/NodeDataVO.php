<?php  namespace gracian_system\application\service;          

class NodeDataVO{    
   
    /*     
   // hier geen library-class van maken.
   put the data inside this class, so the class has acess to all the dtata.
   so it can answer the quetion; hasImage()
   */  

    private $config = null;    
    private $nodeData = null;    
    private $login = null;
    private $messages = null;
    
    //_____________________________________________________________________________________________
    public function __construct($response_data, $config){
        // only transform the first level keys to an array
        $this->config = $config;      
        $this->nodeData = (array) $response_data->itemNodeData;      
        $this->login = (array) $response_data->login;     
        $this->messages = (array) $response_data->messages;     
            //$this->nodeData = $this->objectToArray($response_data);       
        //$this->nodeData = json_decode(json_encode($response_data), true);
            //print_r($this->nodeData['login']);     
        //print_r($this->nodeData);
    }    
    /*
    private function objectToArray ($object) {
        if(!is_object($object) && !is_array($object))
            return $object;
        return array_map('$this->objectToArray', (array) $object);
    }          
    */    
    /*
    * #################################################################
    *     site 
    * #################################################################
    */
      
    /*
    * #################################################################
    *     admin 
    * #################################################################
    */

    public function item($key){  
       return $this->nodeData['item'][$key];
    }   

    
    public function id($key){  
       return $this->nodeData[$key]['id'];
    }   

    public function nodeName($key){  
       return $this->nodeData[$key]['node_name'];
    }      
    
    public function a_update($key){       
        $result = '';
        $item = $this->nodeData[$key];
        if($item['permission']['update']){ 
            $result = '<a href="'.$this->config->app['admin_url'] .'/edit/'.$item['node_name'].'/'.$item['id'].'" class="btn btn-default btn-sm" role="button">EDIT</a>';
        }
        return $result;
    }            
    
    public function a_delete($key){       
        $result = '';
        $item = $this->nodeData[$key];
        if($item['permission']['delete']){ 
            $result = '<a href="'.$this->config->app['admin_url'] .'/delete/'.$item['node_name'].'/'.$item['id'].'" class="btn btn-default btn-sm" role="button">DELETE</a>';
        }
        return $result;
    }

        
    public function title($data_key){   
       return $this->nodeData[$data_key]['title'];  
    }    
    

    public function image($key){ 
        $result = '';    
        $item = $this->nodeData[$key];     
        if(isset($item['file_abs_url']) && $item['file_abs_url'] != ''){
            $result = sprintf( '<img src="%s" style="height:80px;margin:5px;">', $item['file_abs_url']);
        }       
        return $result;  
    }   
    
    public function hasModuleViewPath(){
        if($this->nodeData['moduleViewPath'] !=''){  
            return true;
        }else{
            return false;
        }   
    }
       
    public function viewTemplate(){
        return $this->nodeData->viewTemplate;
    }         
    
    public function moduleViewTemplate($relativePath){
        return $this->nodeData['moduleViewPath'] . '/' . $relativePath . '/admin/' . $this->nodeData['viewTemplate'] . '.php';    
    }

    public function formTemplate(){
        return $this->nodeData->formTemplate;
    }         
    
    public function moduleFormTemplate($relativePath){
        return $this->nodeData['moduleViewPath'] . '/' . $relativePath . '/admin/' . $this->nodeData['formTemplate'] . '.php';    
    } 
               
    public function inputField_args($key, $type=''){
        if($type == 'date'){
            $result = ['label' => $key,  'value' => get_humandate_for_language( $this->nodeData['item'][$key], 'en') ]; 
            //$result = ['label' => $key,  'value' => $this->nodeData['item'][$key] ];
        }elseif($type == ''){
            $result = ['label' => $key,  'value' => $this->nodeData['item'][$key] ];
        }
        return $result;
    }                                                 

    public function formAction($action){    
        $result = '';
        if($action == 'update'){       
            $result = $this->config->app['admin_url'].'/update/'.$this->nodeData['item']['node_name'].'/'.$this->nodeData['item']['id'];
        }    
        if($action == 'store'){       
            $result = $this->config->app['admin_url'].'/store/'.$this->nodeData['item']['node_name'].'/'.$this->nodeData['item']['parent_id'];
        }    
        if($action == 'destroy'){       
            $result = $this->config->app['admin_url']. '/destroy/node';
        }    
      
        return $result;
    }
  
    public function orphanFields(){   
        return $this->nodeData['item']['orphan_fields'];
    }   
    
    public function isFilledOrphanField($key){      
        $isFilledOrphan = false;
        if(!array_key_exists($key, $this->nodeData['variableFieldNames'])){
             if($this->nodeData['item']['orphan_fields'][$key] != ''){  
                 // toon veld alleen als het content heeft     
                 $isFilledOrphan = true;    
             }
         }       
         return $isFilledOrphan;  
                
    }    
    
    public function hasVariableFields(){  
        $hasVariableFields = false;
        if(isset($this->nodeData['item']['variable_fields']) && count($this->nodeData['item']['variable_fields']) > 0){
            $hasVariableFields = true;
        }
        return $hasVariableFields;
    }   
    
    public function variableFields(){  
        return $this->nodeData['item']['variable_fields'];
    }      
         
    public function childCount(){  
        return $this->nodeData['childCount'];
    }      
     
                
    public function a_view($label){   
        $result = sprintf('<a href="%s/view/%s/%s">%s</a>',
        $this->config->app['admin_url'],
        $this->nodeData['item']['node_name'] ,
        $this->nodeData['item']['id'], $label);          
        return $result;
    }      
     
       
}