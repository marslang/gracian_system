<?php  namespace gracian_system\application\service;

use gracian_project\application\service\ConfigFactory;

class BaseService{

    function __construct(){
        $this->config = ConfigFactory::Instance()->config;
    }

}
