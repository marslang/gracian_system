<?php namespace gracian_system\application\service;

use gracian_system\application\applicationPorts\FileServiceIF;   
use gracian_system\domain\exceptions\GracianUploadException;      
use gracian_system\domain\exceptions\GracianFileValidationException;     

class FileService extends BaseService implements FileServiceIF{     
    
    private $fileValidationError = NULL;
   
    public function performFileUpload($fileToUpload, $subdir, $oldFilename=''){          
         //make the filename unique.
        $token = strtolower($this->config->securityToolsService->getToken(11));       
        $newFilename = $token . '-' . $this->sanitizeFileName($fileToUpload["name"]);
        $this->assertFilename($newFilename);   
        $fileToUpload["name"] = $newFilename;
        $this->validateFileToUpload($fileToUpload);
        //$this->config->fileUploadAdapter->uploadFile($this->config->app['file_ upload_abs_path'], $subdir, $fileToUpload);  
        $this->config->fileUploadAdapter->uploadFile($this->config->app['media_abs_path'] . '/original', $subdir, $fileToUpload);
        if($oldFilename != ''){
            //$this->config->fileUploadAdapter->removeOldFile($this->config->app['file_ upload_abs_path'], $subdir, $oldFilename, $newFilename);
            $this->config->fileUploadAdapter->removeOldFile($this->config->app['media_abs_path'] . '/original', $subdir, $oldFilename, $newFilename);
        }
        return $newFilename;
    }  
    

    //_____________________________________________________________________________________________
    private function sanitizeFilename($input){
        $input = $this->config->encoder->canonicalize($input);
        $input = $this->config->sanitizer->sanitizeValue($input, 'removeAllCssHtml');

        // / and .. in the user provided file name can be harmful. So you should get rid of these by something like:
        $input = str_replace('..', '', $input);
        $input = str_replace('/',  '', $input);
        $input = str_replace(' ',  '_', $input);
        $input = str_replace('&',  '_', $input);

        // Remove anything which isn't a word, whitespace, number
        // or any of the following characters -_~,;:[]().
        $input = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).])", '', $input);
        // Remove any runs of periods
        $input = preg_replace("([\.]{2,})", '', $input);

        return $input;
    }    
    
    //_____________________________________________________________________________________________
    private function assertFilename($value){   
        $name = 'filename';    
        $value_arr = explode('.', $value);   
        if(count($value_arr) < 2){
           throw new GracianFileValidationException('validation error: ' .  'filename must contain a dot and an extension');     
        }                   

        $msg = $this->regexFile($value);
        if($msg != ''){
            throw new GracianFileValidationException('validation error: ' . $msg);     
        }
    }       
    
    //_________________________________________________________________________
    private function regexFile($subject){
        $message = '';
        $pattern = "/^[a-zA-Z0-9\p{L}-_.` ]{0,255}$/u";
        if(!preg_match($pattern, $subject)){
            $message = 'File can only contain letters, numbers dashes and hyphens.';
        }
        return $message;
    }            
    
    //_____________________________________________________________________________________________
    private function validateFileToUpload($fileToUpload) {  
        $this->fileValidationError = NULL;
        $this->fileValidationError = $this->config->fileValidator->validate($fileToUpload);
        if($this->fileValidationError != NULL){
            throw new GracianFileValidationException('validation errors: ' . $this->fileValidationError);
        }
    }        
    
}
?>

