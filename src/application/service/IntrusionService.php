<?php namespace gracian_system\application\service;   

use gracian_system\application\applicationPorts\IntrusionServiceIF;   
//use Respect\Validation\Exceptions\ValidationExceptionInterface;    
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianValidationException;

class IntrusionService extends BaseService implements IntrusionServiceIF{    
    
        private $schema = array();


  public function __construct(){     
      parent::__construct();   
      $this->sanitizer = $this->config->sanitizer;     
      $this->validator = $this->config->validator;  
                  
      $this->schema['id']['sanitizers']['removeAllCssHtml'] = Array();  
      $this->schema['id']['validators']['required'] = Array();   
      $this->schema['id']['validators']['integer'] = Array();    
      $this->schema['parentId']['sanitizers']['removeAllCssHtml'] = Array();  
      $this->schema['parentId']['validators']['required'] = Array();   
      $this->schema['parentId']['validators']['integer'] = Array();    
      $this->schema['node']['sanitizers']['removeAllCssHtml'] = Array();  
      $this->schema['node']['validators']['required'] = Array();   
      $this->schema['node']['validators']['alpha_dash'] = Array();    
  }

    //_____________________________________________________________________________________________
    private function handleIntrusionError($name, $msg){
        throw new GracianIntrusionException('Intrusion error: ' . $name . ': ' . $msg );
    }  


    //_____________________________________________________________________________________________
    public function sanitizeValidate($fields){     

        $fields = $this->sanitizeFields($fields, $this->schema);  
        $isValid = $this->validateFields($fields, $this->schema, 'Id or Node is not valid.');  
        return $fields;           
    }      
    
    //_____________________________________________________________________________________________
    public function sanitizeValidateValue($name, $value){    
        $schema[$name] = $this->schema[$name];
        $fields[$name] = $value;
        $fields = $this->sanitizeFields($fields, $schema);  
        $isValid = $this->validateFields($fields, $schema, $name . ' is not valid.');  
        return $fields[$name];            
    }
         
    
    //_____________________________________________________________________________________________
    private function sanitizeFields($fields, $schema){
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);   
        return $fieldsWhitelist;
    }                  
    
    //_____________________________________________________________________________________________
    private function validateFields($fields, $schema, $errMsg){ 
        $isValid = $this->validator->validate($fields, $schema);   
        if( !$isValid ){           
            $validationErrors = $this->validator->getValidationErrorMessages();     
            //print_r($validationErrors); exit();    
            if(count($validationErrors) > 0){
                foreach($validationErrors as $k => $v){
                    $name = $k;
                    foreach($v as $kk => $vv){
                        $errMsg = $vv;
                        break;
                    }   
                    break;
                }
            }
             $this->handleIntrusionError($name, $errMsg);              
        }   
        return $isValid;
    }         
             
}
?>
