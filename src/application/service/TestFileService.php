<?php namespace gracian_system\application\service;

class TestFileService extends BaseService{

        //_____________________________________________________________________________________________
    public function __construct(){
        parent::__construct();
    }

    //_____________________________________________________________________________________________
    public function removeTestMediaFile($subDir, $fileName) {   
        //$targetDir = $this->config->app['file_ upload_abs_path'];     
        $targetDir = $this->config->app['media_abs_path'] . '/original';
        // build in a security mechanism, so you can't remove the production media or dev media by accident
        if($this->config->env == 'test' && $this->isMediaTestFolder()){      
            if($this->config->fileUploadAdapter->exists($targetDir,  $subDir, $fileName))
            $this->config->fileUploadAdapter->removeFile($targetDir, $subDir, $fileName);   
            //$this->config->app['file_ upload_abs_path']
        }    
        
    }    
    
    //_____________________________________________________________________________________________
    public function isMediaTestFolder(){          
        //$pos = strpos($this->config->app['file_ upload_abs_path'], 'gracian_system/tests/media');    
        $pos = strpos($this->config->app['media_abs_path'] . '/original', 'gracian_system/tests/media');
        if ($pos === false) {
            //echo "The string '$findme' was not found in the string '$mystring'";   
            echo 'Precondition Error: TestFileService::isMediaTestFolder(). You are not in the media test folder. Execution is terminated.';
        }else{
            return true;
        }
    }    
      
        
}                                      