<?php namespace gracian_system\application\service;       

use gracian_system\application\applicationPorts\SeoServiceIF;      

class SeoService implements SeoServiceIF {

    //_____________________________________________________________________________________________


//____________________________________________________________________
    public function setHtmlMetaData($item_name='current_item'){

        //$this->data[$item_name]['meta_keywords']
        $lang = $this->mars->sess_get('lang');

        $keywords = $this->config->item('project_title');

        $fields = Array( 'title_'.$lang    , 'meta_keywords_'.$lang);

        foreach($fields as $k => $v){
            if(isset($this->data[$item_name][$v]) && $this->data[$item_name][$v] != ''){
                $keywords .= ',' . str_replace(' ', ',', $this->data[$item_name][$v]);
            }
        }


        $keywords = str_replace(',,' , ',',  $keywords);
        $keywords_arr = explode(',', $keywords);
        $trans = array_flip($keywords_arr);
        $keywords_arr = array_flip($trans);

        $this->data['meta']['keywords'] =  'fresco, muurschildering, schilderen, ' . implode(', ', $keywords_arr);


        $this->data['meta']['description'] = $this->data[$item_name]['meta_description_'.$lang];
        if($this->data['meta']['description'] == ''){
            if(isset($this->data[$item_name]['introduction_'.$lang])
                && $this->data[$item_name]['introduction_'.$lang] != ''){
                $this->data['meta']['description'] = parse_for_list_view($this->data[$item_name]['introduction_'.$lang], 180);
            }
        }
        if($this->data['meta']['description'] == ''){
            if(isset($this->data[$item_name]['bodytext_'.$lang])
                && $this->data[$item_name]['bodytext_'.$lang] != ''){
                $this->data['meta']['description'] = parse_for_list_view($this->data[$item_name]['bodytext_'.$lang], 180);
            }
        }
        $this->data['meta']['description'] = str_replace("\n", " ", $this->data['meta']['description']);
        $this->data['meta']['title'] = $this->data[$item_name]['title_'.$lang];

    }




}
?>
