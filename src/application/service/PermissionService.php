<?php namespace gracian_system\application\service;
                                                        
use gracian_system\application\applicationPorts\PermissionServiceIF;    
use gracian_system\domain\exceptions\GracianAuthorizationException;
use gracian_system\domain\exceptions\GracianException;


class PermissionService extends BaseService implements PermissionServiceIF {

    private $actionList = array(
        'create' => 'create',
        'store' => 'create',
        'view' => 'read',
        'show' => 'read',
        'update' => 'update',
        'edit' => 'update',
        'delete' => 'delete',
        'destroy' => 'delete'
    );


    //_____________________________________________________________________________________________   
    /*
    * just read the value from the $entity->item['permission'][$action]
    * if it reads false, throw an error
    * if it reads true, do nothing, let it pass      
    * NB: permission is set in Entity->fetchItem()        
    *    $this->item['permission'] = $this->authorizationService->collectPermissionsForItem($this, $user, $this->item['owner_id'], $this->item['id']);
    *
    */
    public function assertPermission($entity , $subaction){     
        $this->hasPermission($entity);
        $action = $this->actionList[$subaction];
        $nodeName = $entity->nodeName;
        if(isset($entity->item['id'])){
            $id = $entity->item['id'];
        }else{
            $id = $entity->item['parent_id'];
        }
        $segment = "$subaction/$nodeName/$id";
        if($entity->item['permission'][$action] === false){
            $err = 'AuthorizationException:' . 'You have no permission to '.$subaction.' this item ('.$segment.')';
            throw new GracianAuthorizationException($err);  
        }
    }
    //_____________________________________________________________________________________________
    private function hasPermission($entity){
        if(!isset($entity->item)){
            throw new GracianException('assert_permission: entity->item not found.');
        }
        if(!isset($entity->item['permission'])){
            throw new GracianException('assert_permission: entity->item[permission] not found.');
        }
    } 

}
?>
