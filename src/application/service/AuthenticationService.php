<?php namespace gracian_system\application\service;     

use gracian_system\domain\exceptions\GracianIntrusionException;       

//use gracian_system\application\applicationPorts\SanitizationServiceIF;  

class AuthenticationService extends BaseService { 

    public function __construct(){
        parent::__construct();
        $this->encoder = $this->config->encoder;
        $this->sanitizer = $this->config->sanitizer;
    }      
  /*  
    public function sanitizeInputFields($fields, $schema){
       // $fields = $this->encoder->canonicalizeArray($fields);     
        //print_arr($fields);  
        $fieldsWhitelist = $this->sanitizer->sanitize($fields, $schema);   
        return $fieldsWhitelist;
    }   
  */       
  
      //_____________________________________________________________________________________________
    private function handleIntrusionError($name, $msg){
        throw new GracianIntrusionException('Intrusion error: ' . $name . ': ' . $msg );
    }   
    
    //_____________________________________________________________________________________________
    public function sanitizeId($id) {
        //$id = $this->encoder->canonicalize($id);
        $id = $this->sanitizer->sanitizeValue($id, 'removeAllCssHtml');
        $id = str_replace(" ","",trim($id));
        return $id;
    }

    //_____________________________________________________________________________________________
    public function sanitizeEmail($email) {
        //$email = $this->encoder->canonicalize($email);
        $email = $this->sanitizer->sanitizeValue($email, 'removeAllCssHtml');
        $email = str_replace(" ","",trim($email));
        return $email;
    }

    //_____________________________________________________________________________________________
    public function sanitizeName($name) {
        //$name = $this->encoder->canonicalize($name);
        $name = $this->sanitizer->sanitizeValue($name, 'removeAllCssHtml');
        $name = trim($name);
        return $name;
    }

    //_____________________________________________________________________________________________
    public function sanitizePassword($pw) {
       // $pw = $this->encoder->canonicalize($pw);
        return $pw;
    }

    //_____________________________________________________________________________________________
    public function sanitize($value) {
        //$value = $this->encoder->canonicalize($value);
        $value = $this->sanitizer->sanitizeValue($value, 'removeAllCssHtml');
        return $value;
    }        
    
    
    //_____________________________________________________________________________________________
    public function assertEmail($value){      
        $name = 'email';
        if($value == ''){
            $this->handleIntrusionError($name, 'E-mail cannot be empty');     
        }
        if(strlen($value) > 64){
            $this->handleIntrusionError($name, 'E-mail cannot be more than 64 characters');
        }
        if (strpos($value,' ') !== false) {
            $this->handleIntrusionError($name, 'E-mail cannot contain spaces');
        }
    }    
    
    //_____________________________________________________________________________________________
    public function assertPassword($value){    
        $name = 'password';  
        if($value == ''){
            $this->handleIntrusionError($name, 'Password cannot be empty');
        }
        if(strlen($value) > 72){
            $this->handleIntrusionError($name, 'Password cannot be more than 72 characters');

        }
        if (strpos($value,' ') !== false) {
            $this->handleIntrusionError($name, 'Password cannot contain spaces');
        }
    }

    //_____________________________________________________________________________________________
    public function assertUsername($value){  
        $name = 'username'; 
        if($value == ''){
            $this->handleIntrusionError($name, 'Username cannot be empty');
        }
        if(strlen($value) > 72){
            $this->handleIntrusionError($name, 'Username cannot be more than 72 characters');

        }
        if (strpos($value,' ') !== false) {
            $this->handleIntrusionError($name, 'Username cannot contain spaces');
        }
    }
    
    
    //_____________________________________________________________________________________________
    public function assertHashString($value){ 
        if(!preg_match("/^[a-zA-Z0-9\p{L}]+$/", $value)){
             $this->handleIntrusionError('hash', 'Error: AuthenticationService: hash string is not valid');  
        }  
    }


}
