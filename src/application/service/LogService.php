<?php namespace gracian_system\application\service;

use gracian_system\application\applicationPorts\LogServiceIF;  
use gracian_system\domain\exceptions\GracianIntrusionException;

class LogService extends BaseService implements LogServiceIF {

    //_____________________________________________________________________________________________
    public function __construct(){
        parent::__construct();
        $this->logAdapter = $this->config->logAdapter;
    }

    //_____________________________________________________________________________________________
    public function logIntrusionReport($report, $request){
        $now = getdate();
        $date_str = $now['year'] . '-' .$now['mon'] . '-' .$now['mday'];
        $filePath = $this->config->app['gracian_log_path'] .'/intrusion/intrusion_'.$date_str.'.log';
        if(isset($_SERVER['REMOTE_ADDR'])){
            $log[] = array(
                //'type' => 'IntrusionException',
                //'date' => $now_str,
                'source_ip' => $_SERVER['REMOTE_ADDR'],
                'server_name' => $_SERVER["SERVER_NAME"],
                'request_uri' => $_SERVER["REQUEST_URI"],
                'request' => $request,
                'description' => $report
            );
        }else{ 
            // called from console (unittest)
            $log[] = array('request' => $request,'description' => $report);
        }
        $this->logAdapter->logReport('IntrusionException',$log, $filePath);
    }   
    
    //_____________________________________________________________________________________________
    public function logException($e, $request){   
        $description = "\n\n";             
        $exceptionName = $this->stripPath(get_class($e)); 
        $parentClass = $this->stripPath(get_parent_class($e));    
        $description .= 'parentClass = ' . $parentClass . "\n";     
        if($parentClass == 'GracianException'){
            $description .= 'usr-msg: ' . $e->getUserMessage() . "\n";
            $description .= 'log-msg: ' . $e->getLogMessage() . "\n";                
        }else{
            $description .= $e->getMessage() . "\n";      
        }
        $description .= str_replace($this->config->app['gracian_abs_path'], '', $e->getTraceAsString());   
        $description .= "\n\n";
        $now = getdate();
        $date_str = $now['year'] . '-' .$now['mon'] . '-' .$now['mday'];
        $filePath = $this->config->app['gracian_log_path'] .'/exception/exception_'.$date_str.'.log';
        if(isset($_SERVER['REMOTE_ADDR'])){
            $log[] = array(
                //'type' => 'IntrusionException',
                //'date' => $now_str,
                'source_ip' => $_SERVER['REMOTE_ADDR'],
                'server_name' => $_SERVER["SERVER_NAME"],
                'request_uri' => $_SERVER["REQUEST_URI"],
                'request' => $request,
                'description' => $description
            );
        }else{ 
            // called from console (unittest)
            $log[] = array('request' => $request,'description' => $description);
        } 
          
        $this->logAdapter->logReport($exceptionName,$log, $filePath);
    }  
    
    //_____________________________________________________________________________________________
    private function stripPath($path){
        $pieces = explode("\\", $path) ;
        return$pieces[count($pieces)-1];             
    }
    
    /*
    //_____________________________________________________________________________________________
    public function logGracianExceptionReport($e, $request){    
        $description = "\n\n" . $e->getUserMessage() . "\n";
        $description = $e->getLogMessage() . "\n";
        $description .= str_replace($this->config->app['gracian_abs_path'], '', $e->getTraceAsString());   
        
        $now = getdate();
        $date_str = $now['year'] . '-' .$now['mon'] . '-' .$now['mday'];
        $filePath = $this->config->app['gracian_log_path'] .'/exception/exception_'.$date_str.'.log';
        if(isset($_SERVER['REMOTE_ADDR'])){
            $log[] = array(
                //'type' => 'IntrusionException',
                //'date' => $now_str,
                'source_ip' => $_SERVER['REMOTE_ADDR'],
                'server_name' => $_SERVER["SERVER_NAME"],
                'request_uri' => $_SERVER["REQUEST_URI"],
                'request' => $request,
                'description' => $description
            );
        }else{ 
            // called from console (unittest)
            $log[] = array('request' => $request,'description' => $description);
        }  
        $exceptionClassName = get_class($e); 
        $this->logAdapter->logReport($exceptionClassName, $log, $filePath);
    }                
    */
}
