<?php namespace gracian_system\application\service;        
                                                      


class EnvIniService   {
    
     

    //_____________________________________________________________________________________________

/*
    function get_env_value(){
        // get .env     
        $env_path = '';  
        $relative_path = '';
        $found = false;    
        $count = 0;
        while($found == false){   
            if($count > 7){
                echo 'cannot find .env; process terminated';   
                exit();
                break;
            } 
            $count++;
            $relative_path .= '/..';              
            $tmp_path = __DIR__ . $relative_path  .'/.env';     
            if(file_exists ( $tmp_path )){  
                $env_path = realpath($tmp_path);     
                $found = true;
            }    
        }   
        $env_value = trim(file_get_contents($env_path));   
        return $env_value;    
    }  
*/
    /**
    * get env_development.ini (or env_staging.ini)
    *
    */     
    function get_ini_properties($env_value){      
        $ini_file = '';  
        $relative_path = '';
        $found = false;    
        $count = 0;
        while($found == false){   
            if($count > 7){
                echo 'cannot find .env_' . $env_value . '.ini; process terminated (gracian_site_slim/public/index.php)';   
                exit();
            } 
            $count++;
            $relative_path .= '/..';              
            $tmp_path = __DIR__ . $relative_path  .'/env_' . $env_value . '.ini';//'/src/settings.php';     
            if(file_exists ( $tmp_path )){  
                $ini_file = realpath($tmp_path);     
                $found = true;
            }    
        }      
        $ini_array = parse_ini_file($ini_file);     
        return $ini_array;
    }    

    /**    
    * $project_name = gracian_project_xmp
    * $env_value = development / staging / production
    */ 
    /*
    function get_basePath($project_name, $env_value, $framework_name, $framework_detect){
        // development  
        $basePath = realpath(__DIR__ . '/..'); 
        //                               
        $env_path = '/'.$project_name.'/'.$env_value;
        if( !file_exists ( __DIR__ . '/..' . $framework_detect)){    
            $relative_path = '';
            $found = false;    
            $count = 0;
            while($found == false){   
                if($count > 7){
                    echo 'cannot find '.$framework_name.$framework_detect.'; process terminated ('.$framework_name.'/public/index.php)';
                    break;
                } 
                $count++;
                $relative_path .= '/..';              
                //$path = __DIR__ . $relative_path . '/gracian/gracian_site_slim/src/settings.php';
                $tmp_path = __DIR__ . $relative_path . $env_path .'/gracian/'.$framework_name;//'/src/settings.php';
                //echo '<br>' . $tmp_path .  '/src/settings.php' . '<br>';
                if(file_exists ( $tmp_path . $framework_detect)){  
                    $basePath = realpath($tmp_path);     
                    $found = true;
                }    
            }
        }       
        //echo  $basePath ; exit();  
        return $basePath;    
    }  
    */
           
}
?>   

