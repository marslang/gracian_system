<?php  namespace gracian_system\application\service;     
                                                 
use gracian_system\application\applicationPorts\CrumbServiceIF;   
use gracian_project\domain\service\NodeFactory;      
use gracian_system\domain\exceptions\GracianException;      
  

class CrumbService implements CrumbServiceIF {         
    
    public $crumbPath = array();   
    
    //_____________________________________________________________________________________________ 
    public function __construct(){     
        $this->nodeFactory = new NodeFactory();  
    }

    //_____________________________________________________________________________________________    
    public function getCrumb($node, $id=null){     
        // if id is filled (not null), then you are calling this method from the outside. 
        // in that case, first fetch the crumbItem and set it in the node
        if($id != null){ 
            $node->setItemForCrumb( $id ); 
        }    
        
        //precondition
        if(!isset($node->crumbItem['id'])){
            throw new GracianException('CrumbService: node has empty crumbItem. probably you called getCrumb() without an id.');
        }
               
        $rootCrumb = array('id' => 1,  'title' => 'root', 'node_name' => 'root');
        $nodeCrumb = array('id' => $node->crumbItem['id'],  'title' =>  $node->crumbItem['title'], 'node_name' =>  $node->nodeName);
        
        if($node->nodeName == 'root'){
             array_unshift($this->crumbPath, $rootCrumb);
             return  $this->crumbPath;
        } 
          
        array_unshift($this->crumbPath, $nodeCrumb);
        $parentId =  $node->crumbItem['parent_id'];
        $parentNode = $this->nodeFactory->getNode($node->parentNodeName);  
        $parentNode->setItemForCrumb( $parentId );   
        // when you call this function recursive, then call it without the id 
        $this->getCrumb($parentNode); 
        return $this->crumbPath;
    }        
 
  
}