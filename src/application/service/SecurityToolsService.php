<?php namespace gracian_system\application\service;       

use gracian_system\application\applicationPorts\SecurityToolsServiceIF;      

class SecurityToolsService implements SecurityToolsServiceIF {

    //_____________________________________________________________________________________________
    /**
    * Below is the strongest function I could make that satisfies the criteria (This is an implemented version of Erik's answer).
    * 
    * crypto_rand_secure($min, $max) works as a drop in replacement for rand() or mt_rand. It uses openssl_random_pseudo_bytes to help create a random number between $min and $max.
    * 
    * getToken($length) creates an alphabet to use within the token and then creates a string of length $length.
    * 
    * source - http://us1.php.net/manual/en/function.openssl-random-pseudo-bytes.php#104322
    */
    public function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0){
            return $min;           
        } 
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; 
        $bits = (int) $log + 1; 
        $filter = (int) (1 << $bits) - 1; 
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; 
        } while ($rnd >= $range);
        return $min + $rnd;   
    }

    public function getToken($length){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
        }
        return $token;
    }
    //_____________________________________________________________________________________________


}
?>
