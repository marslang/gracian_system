<?php  namespace gracian_system\application\service;          

class FieldHelper{         

    
    public function render($key, $fieldtype, $fields, $subArrayName= ''){   
        // check if the field is part of a subarray
        if($subArrayName == ''){
            $fieldTagName = $key;
        }else{
            $fieldTagName = $subArrayName . '['.$key.']';
            $fields = $fields[$subArrayName];
        }
        
        // exceptions on  the default rendering
        if($fieldtype == 'date_select'){
            return $this->render_date_select($key, $fieldTagName, $fields);
        }

        if($fieldtype == 'uploadfile'){
            return $this->render_uploadfile($key, $fieldTagName, $fields);
        }    
        
        // the default rendering
        if(isset($fields[$key])){  $value =  $fields[$key]; }else{  $value = ''; }   
        return view(
            'fields/' . $fieldtype, 
            [ 'label' => $key, 
              'key' => $fieldTagName, 
              'value' => $fields[$key] 
            ]
        );    
                        
    }    
    
    
    private function render_date_select($key, $fieldTagName, $fields){      
        return view(
            'fields/formdate_select', 
            [ 'label' => $key, 
              'key' => $fieldTagName, 
              'formdate' => $fields['formdate_' . $key]
            ]
        );  
    }    
    
    // syntax: echo $fieldHelper->render('uploadfile', 'filename', $response->itemNodeData->item);     
    private function render_uploadfile($key, $fieldTagName, $fields){      
        //$key = 'filename' ;   
        if(isset($fields[$key])){  $value =  $fields[$key];  }else{  $value = ''; }    
        return view(
            'fields/uploadfile', 
            [ 'label' => $key, 
            'file_upload_name' => 'fileToUpload', 
            'key' => $fieldTagName,  
            'value' => $value
            ]
        );   
    }      
    
    

}