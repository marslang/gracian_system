<?php namespace gracian_system\application\service;
                                                      
use gracian_system\application\applicationPorts\IdsServiceIF;   
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\application\service\LogService;          
use gracian_system\domain\service\LambdaArrayRecursive;     


class IdsService extends BaseService implements IdsServiceIF{

    //_____________________________________________________________________________________________
    public function __construct(){
        parent::__construct();
        $this->logService = new LogService();
        $this->idsAdapter = $this->config->idsAdapter;
    }    
    
    //_____________________________________________________________________________________________
    public function hasIntrusions(){    
        return $this->idsAdapter->hasIntrusions();
    }

    //_____________________________________________________________________________________________
    /**
    *  request :    REQUEST     GET        POST      COOKIE
    */   
    public function idsDetect($request){   
        $request = $this->parse_accolade_tags_to_ids_safe($request);  
        try{
            $this->idsAdapter->ids_detect($request);
        }catch(\Exception $e){
            echo $e->getMessage(); 
            exit();
        }
        if($this->idsAdapter->hasIntrusions()){
            try{
                $report = $this->idsAdapter->getIntrusionReport(); 
                
                $this->logService->logIntrusionReport($report, $request);
            }catch(\Exception $e){
                echo $e->getMessage();
                exit();
            }
            throw new GracianIntrusionException('ids intrusion detected', $this->idsAdapter->getIntrusionReport());
        }
    }   
   
    /**
    *  voordat de request door de ids gaat, haal de dubbele quotes uit de accolade-tags , anders krijg je een ids error.
    */
    function parse_accolade_tags_to_ids_safe($request){   
        $lambdaFunc = function($arg1) {   
            return $this->parse_accolade_tags_to_safe2($arg1); 
        };   
        if(isset($request['post'])){     
            $post = $request['post'] ;
            $lar = new LambdaArrayRecursive($post, $lambdaFunc);    
            $request['post'] = $lar->parseArray();   
        }      
       return $request;
    }       
    

            
    function parse_accolade_tags_to_safe2($s){
    	$s = str_replace('&quot;', '"', $s);
    	$s = str_replace('&nbsp;', ' ', $s);
        $s = str_replace('<br', ' <br', $s);
    	$s = str_replace('\"', '"', $s);	
    	$preg_fromto = array(
    	"/\{a href=\"(.*?)\" title=\"(.*?)\"\}/is" => "{a href=,,$1,, title=,,$2,,}",	
    	"/\{url=\"(.*?)\" title=\"(.*?)\"\}/is" => "{url=,,$1,, title=,,$2,,}",
    	"/\{link id=\"(.*?)\" title=\"(.*?)\"\}/is" => "{link id=,,$1,, title=,,$2,,}",	
    	"/\{id=\"(.*?)\" title=\"(.*?)\"\}/is" => "{id=,,$1,, title=,,$2,,}",    
    	);
    	$s = preg_replace(array_keys($preg_fromto), array_values($preg_fromto), $s);  
    	return $s;
    }      

   

}
