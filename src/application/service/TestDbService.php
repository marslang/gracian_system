<?php namespace gracian_system\application\service;

class TestDbService extends BaseService{

        //_____________________________________________________________________________________________
    public function __construct(){
        parent::__construct();

        $this->rootItem = Array(
            'id' => 1,
            'parent_id' => 0,
            'publish' => 1,
            'publish_start_date' => '2015-01-31 00:00:00',
            'node_name' => 'root',
            'subdir' => 'aa',
            'title' => 'Root Entity',
            'creation_date' => '2015-01-31 09:39:32',
            'modify_date' => '2015-01-31 11:48:57',
            'variable_fields_json' => ''
        );

        $this->chapterItem = Array(
            'id' => 2,
            'parent_id' => 1,
            'publish' => 1,
            'publish_start_date' => '2015-01-31 00:00:00',
            'node_name' => 'chapter',
            'subdir' => 'aa',
            'title' => 'Chapter Item',
            'creation_date' => '2015-01-31 09:39:32',
            'modify_date' => '2015-01-31 11:48:57',
            'variable_fields_json' => ''
        );

        $this->pageItem = Array(
            'id' => 3,
            'parent_id' => 2,
            'publish' => 1,
            'publish_start_date' => '2015-01-31 00:00:00',
            'node_name' => 'page',
            'subdir' => 'aa',
            'title' => 'Page Item',
            'creation_date' => '2015-01-31 09:39:32',
            'modify_date' => '2015-01-31 11:48:57',
            'variable_fields_json' => ''
        );

        $this->userItem = Array(
            'id' => 1,
            'username' => '',
            'fullname' => 'mars lang',
            'role' => 'admin',
            'email' => 'marcel@limonades.org',
            'password' => $this->config->hashAdapter->hash('carsonQW12!@')  ,
            'remember_token' => '',
            'creation_date' => '2015-01-31 09:39:32',
            'modify_date' => '2015-01-31 11:48:57',
            'expire_date' => '2025-12-31 11:48:57',
            'use_expire_date' => 0
        );

        $this->userItemNotExpired = $this->userItem;
        $this->userItemNotExpired['id'] = 2;
        $this->userItemNotExpired['email'] = 'marcella@limonades.org';
        $this->userItemNotExpired['use_expire_date'] = 0;
        $this->userItemNotExpired['expire_date'] = '2015-01-01 11:48:57';

        $this->userItemExpired = $this->userItem;
        $this->userItemExpired['id'] = 3;
        $this->userItemExpired['email'] = 'marcelino@limonades.org';
        $this->userItemExpired['use_expire_date'] = 1;
        $this->userItemExpired['expire_date'] = '2015-01-01 11:48:57';

        $this->tagItem = Array(
            'id' => 1,
            'name' => 'testtagname' 
        );   

    }

    //_____________________________________________________________________________________________
    public function addTestStreeRoot() {
        $repository = $this->config->repositoryFactory->getRepository('stree');
        $new_id = $repository->store($this->rootItem);
        $repository->updateFieldForId($new_id, 'id', 1);
   }

    //_____________________________________________________________________________________________
    public function addTestStreeChapter() {
        $repository = $this->config->repositoryFactory->getRepository('stree');
        $new_id = $repository->store($this->chapterItem);
        //$repository->updateFieldForId($new_id, 'id', 1);
   }

    //_____________________________________________________________________________________________
    public function addTestStreePage() {
        $repository = $this->config->repositoryFactory->getRepository('stree');
        $new_id = $repository->store($this->pageItem);
        //$repository->updateFieldForId($new_id, 'id', 1);
   }

    //_____________________________________________________________________________________________
    public function addTestSuperUser() {
        $repository = $this->config->repositoryFactory->getRepository('user');
        $this->userItem['username'] = 'su';
        $this->userItem['role'] = 'su';
        $new_id = $repository->store($this->userItem);
        $this->userItem['username'] = '';
        $new_id = $repository->store($this->userItemNotExpired);
        $new_id = $repository->store($this->userItemExpired);
    }     
    
    //_____________________________________________________________________________________________
    public function addTestTag() {
        $repository = $this->config->repositoryFactory->getRepository('tag');
        $new_id = $repository->store($this->tagItem);
        $repository->updateFieldForId($new_id, 'id', 1);
   }         

    //_____________________________________________________________________________________________
    //truncateTable
    public function truncateTestTable($tableName){
            $repository = $this->config->repositoryFactory->getRepository('stree');
            $repository->truncateTestTable($tableName);
    }

}
?>
