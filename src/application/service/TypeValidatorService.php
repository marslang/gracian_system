<?php namespace gracian_system\application\service;    

use gracian_system\application\applicationPorts\TypeValidatorServiceIF;    

class TypeValidatorService implements TypeValidatorServiceIF{   
    



    //_____________________________________________________________________________________________    
    /**                                    
     * check if value is an integer 
     * the checklist is a blacklist. if it passes the blacklist, it returns true
     * @param int $value the value to check 
     * @param int $maxDigits the max number of digits. 20 is the max. after 20 it gives the wrong answer
     * @return boolean tue if it is an integer
     */  
    public function isInteger($value, $maxDigits=-1) {   
                 
        if(!is_numeric($value)){ 
            return FALSE;
        }
          
        if($maxDigits < 0){
            if(!preg_match("/^[0-9]+$/", $value)){ 
                return FALSE; 
            } 
        } 
        
        if($maxDigits > 0){
            if(!preg_match("/^[0-9]{0,20}$/", $value)){ 
                return FALSE; 
            }   
        } 
        
        if($maxDigits == 0){ 
            return FALSE; 
        }  
        
        return TRUE;
    }    
    
    //_________________________________________________________________________
    public function isString($value) {   
                 
        //if(!preg_match("/^([a-zA-Z]|_){3,20}$/", $value)){     
        if(!preg_match("/^([a-zA-Z]|_)+$/", $value)){
            return FALSE;
        }  
        return TRUE;  
          

    }        
        
}