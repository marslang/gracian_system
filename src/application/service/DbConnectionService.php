<?php  namespace gracian_system\application\service;

use gracian_system\application\applicationPorts\DbConnectionServiceIF;   
use gracian_system\domain\exceptions\GracianException;

class DbConnectionService implements DbConnectionServiceIF{



    public function getDbConnection($db){
        if(!isset($db['dsn'])){
            throw new GracianException(
                'gracian_system:DbConnectionService: No db[dsn] found.',
                'gracian_system/DbConnectionService::getDbConnection:GracianException:  db[dsn] not set.'
            );
        }

        if($db['dsn'] == 'mysql' && $db['type'] == 'pdo'){
            $host = $db['host'];
            $user = $db['user'];
            $pass = $db['pass'];
            $name = $db['name'];
            try{
                $pdoDb = new \PDO('mysql:host='.$host.';dbname='.$name.';charset=utf8',$user,$pass);
                /* NOTE: By default PDO is not in a state that will display errors.
                you need to provide the following in your DB connection
                see: http://us2.php.net/manual/en/pdo.setattribute.php */
                $pdoDb->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                return  $pdoDb;
            }catch (\Exception $e){
                echo 'DbConnectionService: ' . $e->message();
            }  
        }
        if($db['dsn'] == 'sqlite::memory:'){
            try {
                return new \PDO($db['dsn']);
            }
            catch (\PDOException $e){
                echo 'DbConnectionService: ' . $e->message();
            }
        }

    }

}
