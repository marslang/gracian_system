<?php  namespace gracian_system\application\usecase\base;


use gracian_project\application\service\ConfigFactory;
use gracian_system\infrastructure\requestBag\RequestBagFactory;
use gracian_system\infrastructure\responseBag\ResponseBagFactory;   

use gracian_system\application\service\PermissionService;
use gracian_system\application\service\IdsService;
use gracian_system\application\service\LogService;
use gracian_system\application\service\IntrusionService;
use gracian_system\application\service\ValidationService;     
use gracian_system\application\service\FileService;   
                                                                
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianIntrusionException;
use gracian_system\domain\exceptions\GracianAuthenticationException;    
use gracian_system\domain\exceptions\GracianAuthorizationException;   
use gracian_system\domain\exceptions\GracianWarningException;  



class BaseUsecase{

    function __construct(){
        $configFactory = ConfigFactory::Instance();
        $this->config = $configFactory->config;
        $this->requestBagFactory = new RequestBagFactory();
        $this->requestBag = $this->requestBagFactory->getRequestBag($this->config->requestType);
        $this->responseBagFactory = new ResponseBagFactory();
        $this->responseBag = $this->responseBagFactory->getResponseBag($this->config->responseType);
        $this->permissionService = new PermissionService();
        //$this->sanitizationService = new SanitizationService();
        $this->idsService = new IdsService();
        $this->logService = new LogService();
        $this->intrusionService = new IntrusionService();
        $this->validationService = new ValidationService();     
        $this->fileService = new FileService();        
        $anon = Array('id' => 0,'fullname' => 'anon', 'username' => 'anon', 'role' => 'anon');  
        $this->responseBag->set('login' , $this->config->sessionVo->get('login', $anon) );
    }

    public function execute($request){    
        $request2 = $this->config->encoder->canonicalizeArray($request);  
        $this->requestBag->transformIn($request2);   
        $this->idsService->idsDetect($this->requestBag->request);

    }

}
