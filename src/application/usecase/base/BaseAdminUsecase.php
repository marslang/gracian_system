<?php  namespace gracian_system\application\usecase\base;

use gracian_system\domain\exceptions\GracianAccessException;

class BaseAdminUsecase extends BaseUsecase{

    /**
    *  NOTE: haal altijd de flashMessages op uit de sessie  en zet ze in de responseBag als messages
    *  messages zullen getoond worden in de view      
    *
    */   
    function __construct(){
        parent::__construct();      
        $this->responseBag->set('messages', $this->config->sessionVo->get('messages', array()));   

    }
    
    /*
    *  don't remove this function. keep it explicit.
    */
    public function execute($request){
        parent::execute($request);     
        if($this->responseBag->get('login')['role'] == 'anon'){
            // laat de catcher van de exception de message in de responseBag-FlashMessages zetten
            throw new GracianAccessException('You have no permission to view this page (not logged in).');
        }          
    }  
}
