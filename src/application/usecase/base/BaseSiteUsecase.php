<?php  namespace gracian_system\application\usecase\base;

use gracian_system\domain\exceptions\GracianAccessException;

class BaseSiteUsecase extends BaseUsecase{

    function __construct(){
        parent::__construct();          
        $this->responseBag->set('messages', $this->config->sessionVo->get('messages', array()));   
    }      
    
    /*
    *  don't remove this function. keep it explicit.
    */     
    public function execute($request){
        parent::execute($request);   
    }



}
