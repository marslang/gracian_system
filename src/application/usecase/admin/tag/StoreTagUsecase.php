<?php  namespace gracian_system\application\usecase\admin\tag;      

use gracian_system\application\usecase\base\BaseAdminUsecase;     
use gracian_project\domain\model\tag\PrjTagEntity;    
use gracian_system\domain\exceptions\GracianValidationException;

/**
* Update = saving a form send by Edit
*/

class StoreTagUsecase extends BaseAdminUsecase{



    function execute($theRequest){       
        try{     
            parent::execute($theRequest);
            $tagEntity = new PrjTagEntity();
            $fields['name'] = $this->requestBag->request['post']['name'];
            $fields = $this->config->sanitizer->sanitize($fields, $tagEntity->getSchema()); 
            $isValid = $this->validationService->validateInputFields($fields, $tagEntity->getSchema());//'store') );  
            $tagEntity->setEmptyItem();
            $this->permissionService->assertPermission($tagEntity, 'store');
            $newId = $tagEntity->storeFields($fields);
            $this->responseBag->set('new_id', $newId);
            $this->responseBag->appendToFlashMessages('success', 'Successfully stored the tag.');
        } catch (GracianValidationException $e) {     
            // de merge met db-fields gebeurt in de CreateTagUseCase              
            $this->responseBag->set('fields', $fields); 
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch(\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally{
            return $this->responseBag;
        }                 
    }

}
