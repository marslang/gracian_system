<?php  namespace gracian_system\application\usecase\admin\tag;      

use gracian_system\application\usecase\base\BaseAdminUsecase;     
use gracian_project\domain\model\tag\PrjTagEntity;                


/**
* Edit = presenting a form to change the content of an existing item
*
*/

class EditTagUsecase extends BaseAdminUsecase{

    function execute($theRequest){
        try{
            parent::execute($theRequest);
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['id']);
            $tagEntity = new PrjTagEntity();
            $tagEntity->fetchItem($id);
            $this->permissionService->assertPermission($tagEntity, 'edit');
            if($this->config->sessionVo->hasValidationErrors()){      
                $tagEntity->mergeItemWithChangedFormfields($this->config->sessionVo->get('fields'));
                $this->responseBag->set('validationErrors', $this->config->sessionVo->get('validationErrors'));
            }
            $this->responseBag->set('tagEntity', $tagEntity->transformOut());   
        } catch(GracianException $e) {  
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        } catch (\Exception $e) {    
            $this->responseBag->appendToFlashMessages('error',  $e->getMessage());    
        }finally{
            return $this->responseBag;                
        }                    
    }

}
