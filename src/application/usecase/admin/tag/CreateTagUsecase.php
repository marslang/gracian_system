<?php  namespace gracian_system\application\usecase\admin\tag;

use gracian_system\application\usecase\base\BaseAdminUsecase;    
use gracian_project\domain\model\tag\PrjTagEntity;


/**
* Create = presenting a form to add a tag
* the request is empty.
* there are no input argument
*/

class CreateTagUsecase extends BaseAdminUsecase{

    function execute($theRequest){
        try{    
            parent::execute($theRequest);
            $tagEntity = new PrjTagEntity();
            $tagEntity->setEmptyItem();
            $this->permissionService->assertPermission($tagEntity, 'create');   
            if($this->config->sessionVo->hasValidationErrors()){      
                $tagEntity->mergeItemWithChangedFormfields($this->config->sessionVo->get('fields'));
                $this->responseBag->set('validationErrors', $this->config->sessionVo->get('validationErrors'));
            }
            $this->responseBag->set('tagEntity', $tagEntity->transformOut());  
        } catch(GracianException $e) { 
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error',  $e->getMessage());    
        }finally{
            return $this->responseBag;
        }
    }

}
