<?php  namespace gracian_system\application\usecase\admin\tag;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_project\domain\model\tag\PrjTagEntity;

class ListTagUsecase extends BaseAdminUsecase{

    function execute($theRequest){
        try{   
            parent::execute($theRequest);
            $tagEntity = new PrjTagEntity();
            $tagEntity->listAll();   
            $this->responseBag->set('tagEntity', $tagEntity->transformOut());   
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) { 
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally{
            return $this->responseBag;
        }          
    }
}
