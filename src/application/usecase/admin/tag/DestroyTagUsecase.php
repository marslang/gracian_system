<?php  namespace gracian_system\application\usecase\admin\tag;     

/*  
NOTE about 'use'
the use statement must be declared in the file itself
if it is declared in the superclass (BaseUsecase) , then it does not exist   
so repeat all the Gracian*exceptions that you want to use, here.


NOTE about Gracian*Exceptions.
it is enough to catch only the GracianException.
All the GracianSubExceptions  will be caught also.
Unless you want to handle the exception in a different way, then cath it befor the GracianException
*/ 

use gracian_system\application\usecase\base\BaseAdminUsecase;     
use gracian_project\domain\model\tag\PrjTagEntity;           
//use gracian_system\domain\exceptions\GracianAuthorizationException;    


class DestroyTagUsecase extends BaseAdminUsecase{



    function execute($theRequest){       
        try{
            parent::execute($theRequest);      
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['post']['id']);
            $tagEntity = new PrjTagEntity();
            $tagEntity->fetchItem($id);
            $this->permissionService->assertPermission($tagEntity, 'destroy');    
            // destroys also all relations
            $tagEntity->destroyItem($id);  
            $this->responseBag->appendToFlashMessages('success', 'Successfully deleted the tag.');
            $this->responseBag->set('redirect', '/admin/tag/index');
        //}catch (GracianValidationException $e) {     
        //    //$this->responseBag->set('validationErrors', $tagEntity->validationVO->transformOut());   
        //    
        //    $this->responseBag->set('validationErrors',  array('field' => $e->getUserMessage()) );
        //    $this->responseBag->set('fields', $fields);                
        //    $this->responseBag->set('redirect', '/admin/tag/delete'  . $id);            
        //} catch(GracianAuthorizationException $e){
        //    $this->responseBag->appendToFlashMessages('error', 'GrAuEx-' . $e->getUserMessage());
        } catch(GracianException $e) { 
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());            
        }catch(\Exception $e){ 
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());   
        }finally{            
            return $this->responseBag;          
        }               
    }

}
