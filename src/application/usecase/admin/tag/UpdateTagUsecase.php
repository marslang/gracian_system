<?php  namespace gracian_system\application\usecase\admin\tag;       

use gracian_system\application\usecase\base\BaseAdminUsecase;     
use gracian_project\domain\model\tag\PrjTagEntity;     
use gracian_system\domain\exceptions\GracianValidationException;


class UpdateTagUsecase extends BaseAdminUsecase{             

    /*
    TODO: id zit in get, user-name zit in post
    kan een hacker de id veranderen zodat zijn user-name-pw aan een andere user gekoppeld zijn
    */        
    
    function execute($theRequest){      
        try{
            parent::execute($theRequest);
            $fields['id'] = $this->requestBag->request['get']['id'];
            $fields['name'] = $this->requestBag->request['post']['name'];
            $tagEntity = new PrjTagEntity();  
            $fields = $this->config->sanitizer->sanitize($fields, $tagEntity->getSchema()); 
            $isValid = $this->validationService->validateInputFields($fields, $tagEntity->getSchema());  
            $tagEntity->fetchItem($fields['id']);
            $this->permissionService->assertPermission($tagEntity, 'update');
            $tagEntity->updateItem($fields);
            $this->responseBag->appendToFlashMessages('success', 'Successfully updated the tag.');
        } catch (GracianValidationException $e) {    
            // de merge met db-fields gebeurt in de EditTagUseCase              
            $this->responseBag->set('fields', $fields);
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally{
            return $this->responseBag;     
        }                
    }

}
