<?php  namespace gracian_system\application\usecase\admin\tag;      

use gracian_system\application\usecase\base\BaseAdminUsecase;     
use gracian_project\domain\model\tag\PrjTagEntity;      


class DeleteTagUsecase extends BaseAdminUsecase{



    function execute($theRequest){       
        try{            
            parent::execute($theRequest);
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['id']);
            $tagEntity = new PrjTagEntity();
            $tagEntity->fetchItem($id);    
            $tagEntity->setRelationCount($id);
            $this->permissionService->assertPermission($tagEntity, 'delete');
            $this->responseBag->set('tagEntity', $tagEntity->transformOut());
        } catch(GracianException $e) {   
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());     
        } catch (\Exception $e) {    
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());     
        } finally{            
            return $this->responseBag;      
        }               
    }

}
