<?php  namespace gracian_system\application\usecase\admin\error;

use gracian_system\application\usecase\base\BaseSiteUsecase;
use gracian_system\domain\exceptions\InputException;
use gracian_system\domain\exceptions\GracianException;
                                       
// this must inherit from BaseSiteUsecase becaus anon users must view this too.
class ViewErrorUsecase extends BaseSiteUsecase{



    /**
    * in IntrusionService
    * $this->config->sessionVo->setToFlashStage('sta tus', 'hasIntrusionErrors');
    * $this->config->sessionVo->setToFlashStage('intrusionErrors', array($name => $msg));
    *
    * intrusionErrors found?
    * if the Usecase returned a IntrusionException and redirected back to this usecase
    * hier wordt de session flash message in de response geplaatst
    * dit gebeurt in controller->edit (maar ook hier) $responseBag->merge With Messages($this->config->sessionVo->getMessages());
    */

    function execute($theRequest){
        parent::execute($theRequest);
        //if(isset($this->config->sessionVo->data['messages'])){
        //    $this->responseBag->set('messages', $this->config->sessionVo->data['messages']);
        //}
        return $this->responseBag;
    }
}
