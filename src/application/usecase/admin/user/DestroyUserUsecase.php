<?php  namespace gracian_system\application\usecase\admin\user;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_project\domain\model\user\PrjUserEntity;

class DestroyUserUsecase extends BaseAdminUsecase{

    function execute($theRequest){
        try{
            parent::execute($theRequest);   
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['post']['id']);
            $userEntity = new PrjUserEntity();
            $userEntity->fetchItem($id);
            $this->permissionService->assertPermission($userEntity, 'destroy');
            $userEntity->destroyItem($id);
            $this->responseBag->appendToFlashMessages('success', 'Successfully deleted the user.');
            $this->responseBag->set('redirect', '/admin/user/index');
        //}catch(GracianIntrusionException $e) {
        //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());    
        //}catch (GracianValidationException $e) {
        //    //$this->responseBag->set('validationErrors', $userEntity->validationVO->transformOut());     
        //    $this->responseBag->set('validationErrors',  array('field' => $e->getUserMessage()) );
        //
        //    $this->responseBag->set('fields', $fields);                
        //    $this->responseBag->set('redirect', '/admin/user/delete'  . $id);            
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());            
        }catch(\Exception $e){
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());   
        } finally{
            return $this->responseBag;
        }
    }

}
