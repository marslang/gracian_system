<?php  namespace gracian_system\application\usecase\admin\user;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\domain\exceptions\GracianValidationException;

/**
* Update = saving a form send by Edit
*/

class StoreUserUsecase extends BaseAdminUsecase{



    function execute($theRequest){      
        try{
            parent::execute($theRequest);       
            
            $fields['username'] = $this->requestBag->request['post']['username'];
            $fields['email'] = $this->requestBag->request['post']['email'];
            $fields['password'] = $this->requestBag->request['post']['password'];
            $fields['fullname'] = $this->requestBag->request['post']['fullname'];
            $fields['role'] = $this->requestBag->request['post']['role'];
            $fields['expire_date'] = $this->requestBag->request['post']['expire_date'];
              
            // precondition voor de test-cases  
            if(isset($this->requestBag->request['post']['use_expire_date']) && $this->requestBag->request['post']['use_expire_date'] == 0){ 
                unset($this->requestBag->request['post']['use_expire_date']);  
            }
            // voeg de checkbox = 0 toe als de checkbox ontbreekt
            if(isset($this->requestBag->request['post']['use_expire_date'])){ 
                $fields['use_expire_date'] = 1; 
            }else{ 
                $fields['use_expire_date'] = 0; 
            }    
            
            $userEntity = new PrjUserEntity();     
            $fields = $this->config->sanitizer->sanitize($fields, $userEntity->getSchema()); 
            $isValid = $this->validationService->validateInputFields($fields, $userEntity->getSchema());//'store') );  
            
            $userEntity->setEmptyItem();
            $this->permissionService->assertPermission($userEntity, 'store');
            $newId = $userEntity->storeFields($fields);
            $this->responseBag->set('new_id', $newId);
            $this->responseBag->appendToFlashMessages('success', 'Successfully stored the user.');           
        } catch (GracianValidationException $e) {
            $this->responseBag->set('fields', $fields);       
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));                          
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch(\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally{
            return $this->responseBag;
        }          
    }

}
