<?php  namespace gracian_system\application\usecase\admin\user;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_project\domain\model\user\PrjUserEntity;


class CreateUserUsecase extends BaseAdminUsecase{

    function execute($theRequest){  
        try{
            parent::execute($theRequest);
            $userEntity = new PrjUserEntity();
            $userEntity->setEmptyItem();
            $this->permissionService->assertPermission($userEntity, 'create');   
            if($this->config->sessionVo->hasValidationErrors()){   
                $userEntity->mergeItemWithChangedFormfields($this->config->sessionVo->get('fields'));
                $this->responseBag->set('validationErrors', $this->config->sessionVo->get('validationErrors'));
            }
            $this->responseBag->set('userEntity', $userEntity->transformOut());        
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error',  $e->getMessage());    
        } finally {
            return $this->responseBag;     
        }
    }


}
