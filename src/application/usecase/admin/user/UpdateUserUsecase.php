<?php  namespace gracian_system\application\usecase\admin\user;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\domain\exceptions\GracianException;  
use gracian_system\domain\exceptions\GracianValidationException;

class UpdateUserUsecase extends BaseAdminUsecase{

    /*
    TODO: id zit in get, user-name zit in post
    kan een hacker de id veranderen zodat zijn user-name-pw aan een andere user gekoppeld zijn

    DONE: zorg dat user-name altijd uniek is.  ALTER TABLE user ADD UNIQUE(user-name)
    */
    function execute($theRequest){
        try{
            parent::execute($theRequest);      
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['id']);  
            $fields['id'] = $id;
            $fields['username'] = $this->requestBag->request['post']['username'];
            $fields['email'] = $this->requestBag->request['post']['email'];
            if($this->requestBag->request['post']['password'] != ''){
                $fields['password'] = $this->requestBag->request['post']['password'];
            }
            $fields['fullname'] = $this->requestBag->request['post']['fullname'];
            $fields['role'] = $this->requestBag->request['post']['role'];
            $fields['expire_date'] = $this->requestBag->request['post']['expire_date'];   
            // precondition voor de test-cases  
            if(isset($this->requestBag->request['post']['use_expire_date']) && $this->requestBag->request['post']['use_expire_date'] == 0){ 
                unset($this->requestBag->request['post']['use_expire_date']);  
            }            
            if(isset($this->requestBag->request['post']['use_expire_date'])){
                $fields['use_expire_date'] = 1; 
            }else{ 
                $fields['use_expire_date'] = 0; 
            }  
            
            $userEntity = new PrjUserEntity();       
            $fields = $this->config->sanitizer->sanitize($fields, $userEntity->getSchema()); 
            $isValid = $this->validationService->validateInputFields($fields, $userEntity->getSchema());    
            
            $userEntity->fetchItem($id);
            $this->permissionService->assertPermission($userEntity, 'update');      
            $userEntity->updateItem($fields);
            $this->responseBag->appendToFlashMessages('success', 'Successfully updated the user.');      
        } catch (GracianValidationException $e) {     
            $this->responseBag->set('fields', $fields);            
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally{
            return $this->responseBag;
        }
        
    }

}
