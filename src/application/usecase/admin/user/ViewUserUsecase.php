<?php  namespace gracian_system\application\usecase\admin\user;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_system\domain\exceptions\InputException;
use gracian_system\domain\exceptions\GracianException;
use gracian_project\domain\model\user\PrjUserEntity;

class ViewUserUsecase extends BaseAdminUsecase{

    function execute($theRequest){  
        try{
            parent::execute($theRequest);   
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['id']);              
            $userEntity = new PrjUserEntity();
            $userEntity->fetchItem($id);
            $this->permissionService->assertPermission($userEntity, 'view');
            $this->responseBag->set('userEntity', $userEntity->transformOut());                 
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) { 
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally {            
            return $this->responseBag;    
        }    
    }
}
