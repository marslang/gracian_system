<?php  namespace gracian_system\application\usecase\admin\user;

use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\application\service\RepositoryFactory;
use gracian_system\domain\exceptions\InputException;
use gracian_system\domain\exceptions\GracianException;


/**
*  View = presenting an item in the cms with its children and relations, etc
*  common usecases are in gracian_system
*  custom usecases can also be in gracian_project_example
*/

class ListUserUsecase extends BaseAdminUsecase{

    function execute($theRequest){     
        try{
            parent::execute($theRequest);
            $userEntity = new PrjUserEntity();
            $userEntity->listAll();
            $this->responseBag->set('userEntity', $userEntity->transformOut());   
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) { 
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally {
            return $this->responseBag;
        }
    }
}
