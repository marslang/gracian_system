<?php  namespace gracian_system\application\usecase\admin\node;

use gracian_project\domain\service\NodeFactory;
use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_system\domain\service\CrumbService;        
use gracian_system\application\service\logService;  
use gracian_system\domain\exceptions\InputException;
use gracian_system\domain\exceptions\GracianException;

/**
*  View = presenting an item in the cms with its children and relations, etc
*  common usecases are in gracian_system
*  custom usecases can also be in gracian_project_example
*/

class ViewNodeUsecase extends BaseAdminUsecase{

    public function __construct(){
        parent::__construct();
        $this->nodeFactory = new NodeFactory();     
        $this->logService = new LogService();   
    }

    public function execute($theRequest){      
        try{
            parent::execute($theRequest);     
            
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['id']);
            $nodeName = $this->intrusionService->sanitizeValidateValue('node', $this->requestBag->request['get']['node']); 
            $node = $this->nodeFactory->getNode($nodeName);
            $node->fetchItem($id);      
            $node->fetchChildren($id);
            $node->fetchCrumb($id);  
            $this->permissionService->assertPermission($node, 'view');   
            $this->responseBag->set('itemNodeData', $node->transformOut());   
                        /* 
                        $crumbService = new CrumbService();
                        $crumb = $crumbService->getCrumb($node, $id);
                        $this->responseBag->set('crumb',  $crumb);  
                        */
                        /*
                        //} catch(GracianAccessException $e) {   
                        //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
                        //} catch(GracianIntrusionException $e) {
                        //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
                        //} catch(GracianAuthorizationException $e){
                        //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());     
                        */  
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());    
            $this->logService->logException($e, $this->requestBag->request);
        } catch (\Exception $e) { 
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());   
            $this->logService->logException($e, $this->requestBag->request );    
        } finally { 
            return $this->responseBag;    
        }   

    }
}       


