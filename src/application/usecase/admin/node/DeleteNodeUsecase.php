<?php  namespace gracian_system\application\usecase\admin\node;

use gracian_project\domain\service\NodeFactory;
use gracian_system\application\usecase\base\BaseAdminUsecase;     
use gracian_system\domain\exceptions\GracianException;

/**
* Delete = presenting a dialog page to delete an item. This usecase also checks if it is allowed an possible (no children)
*/

class DeleteNodeUsecase extends BaseAdminUsecase{

    function __construct(){
        parent::__construct();
        $this->nodeFactory = new NodeFactory();
    }

    public function execute($theRequest){
        try{
            parent::execute($theRequest);       
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['id']);
            $nodeName = $this->intrusionService->sanitizeValidateValue('node', $this->requestBag->request['get']['node']);
            $node = $this->nodeFactory->getNode($nodeName);
            $node->fetchItem($id);
            $node->setChildCount($id);
            $this->permissionService->assertPermission($node, 'delete');
            // a delete cannot have validation errors
            $this->responseBag->set('itemNodeData', $node->transformOut());
        //} catch(GracianIntrusionException $e) {
        //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        //} catch(GracianAuthorizationException $e){
        //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch(GracianException $e) {     
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally {  
            return $this->responseBag;      
        }   
    }

}
