<?php  namespace gracian_system\application\usecase\admin\node;

use gracian_project\domain\service\NodeFactory;
use gracian_system\application\usecase\base\BaseAdminUsecase;

/**
* Create = presenting a form to add an existing item
*
*/

class CreateNodeUsecase extends BaseAdminUsecase{

    function __construct(){
        parent::__construct();
        $this->nodeFactory = new NodeFactory();
    }

    public function execute($theRequest){
        try{
            parent::execute($theRequest);      
            
            $parentId = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['parentId']);
            $nodeName = $this->intrusionService->sanitizeValidateValue('node', $this->requestBag->request['get']['node']);    
            $node = $this->nodeFactory->getNode($nodeName);
            $node->setEmptyItem( $parentId);
            $this->permissionService->assertPermission($node, 'create');
            // if the StoreUsecase returned a ValidationException and redirected back to the Create form
            if($this->config->sessionVo->hasValidationErrors()){ 
                $node->mergeItemWithChangedFormfields($this->config->sessionVo->get('fields'));
                $this->responseBag->set('validationErrors', $this->config->sessionVo->get('validationErrors'));
            }
            $this->responseBag->set('itemNodeData', $node->transformOut());  
                    /*   
                    //} catch(GracianIntrusionException $e) {
                    //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
                    //} catch(GracianAuthorizationException $e){
                    //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());     
                    */
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        } finally {              
            return $this->responseBag;    
        }   
    }

}
