<?php  namespace gracian_system\application\usecase\admin\node;

use gracian_project\domain\service\NodeFactory;
use gracian_system\application\usecase\base\BaseAdminUsecase;

/**
* Show = presenting an item on the website. This is with filters like: publish, pubstartdate, permission, parent.publish
*
*/

class ShowNodeUsecase extends BaseAdminUsecase{

    function __construct(){
        parent::__construct();
        $this->nodeFactory = new NodeFactory();
    }

    public function execute($theRequest){
        try{
            parent::execute($theRequest);

            echo 'not implemented';
            exit();

        } catch(\Exception $e){
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
            
        }  finally {
            return $this->responseBag;       
            
        }  
    }

}
