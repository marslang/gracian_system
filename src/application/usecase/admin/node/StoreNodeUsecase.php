<?php  namespace gracian_system\application\usecase\admin\node;

use gracian_project\domain\service\NodeFactory;
use gracian_system\application\usecase\base\BaseAdminUsecase;
use gracian_system\domain\exceptions\GracianValidationException;         
use gracian_system\domain\exceptions\GracianFileValidationException;     

/**
* Store = saving a form send by Create
*/

class StoreNodeUsecase extends BaseAdminUsecase{

    function __construct(){
        parent::__construct();
        $this->nodeFactory = new NodeFactory();
    }

    public function execute($theRequest){
        try{
            parent::execute($theRequest);      
            $parentId = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['get']['parentId']);
            $nodeName = $this->intrusionService->sanitizeValidateValue('node', $this->requestBag->request['get']['node']);    
            
            $node = $this->nodeFactory->getNode($nodeName);
            $fields = $this->requestBag->request['post'];     
            $fields = $this->config->sanitizer->sanitize($fields, $node->getSchema()); 
            $isValid = $this->validationService->validateInputFields($fields, $node->getSchema());    

            $node->setEmptyItem( $parentId);
            $this->permissionService->assertPermission($node, 'store');   
            if ($this->requestBag->hasFileToUpload()){     
                $fields['filename'] = $this->fileService->performFileUpload($this->requestBag->request['files']['fileToUpload'], $node->item['subdir']);
            }   
            $newId = $node->storeFields($parentId, $fields);
            $this->responseBag->set('new_id', $newId);
            $this->responseBag->appendToFlashMessages('success', 'Successfully stored the item.');     
            
        } catch (GracianValidationException $e) {
            $this->responseBag->set('fields', $fields);       
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));                                              
        }catch (GracianFileValidationException $e) {   
            $this->responseBag->set('validationErrors',  array('file' => $e->getUserMessage()) );
            $this->responseBag->set('fields', $fields);                        
        } catch (GracianUploadException $e) {    
            $this->responseBag->set('validationErrors',  array('file' => $e->getUserMessage()) );
            $this->responseBag->set('fields', $fields);                   
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch(\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        }  finally {  
            return $this->responseBag;       
        }   
            
    }


}
