<?php  namespace gracian_system\application\usecase\admin\node;

use gracian_project\domain\service\NodeFactory;
use gracian_system\application\usecase\base\BaseAdminUsecase;    
use gracian_system\domain\exceptions\GracianWarningException;    

/**
* Destroy = This usecase destroys the item and presents a confirm message
*/

class DestroyNodeUsecase extends BaseAdminUsecase{

    function __construct(){
        parent::__construct();
        $this->nodeFactory = new NodeFactory();
    }

    public function execute($theRequest){
        try{
            parent::execute($theRequest);     
            $id = $this->intrusionService->sanitizeValidateValue('id', $this->requestBag->request['post']['id']);
            $nodeName = $this->intrusionService->sanitizeValidateValue('node', $this->requestBag->request['post']['node']); 
            $node = $this->nodeFactory->getNode($nodeName);
            $node->fetchItem($id);     
            $this->permissionService->assertPermission($node, 'destroy');    
            $node->assertZeroChildren($id);
            $this->responseBag->set('redirect', array(
                    'action' => 'view',
                    'node' => $node->parentNodeName,
                    'id' => $node->item['parent_id']
                )
            );
            $node->destroyItem($id);        
            $this->responseBag->appendToFlashMessages('success', 'Successfully deleted the item.');
                    //}catch(GracianIntrusionException $e) {
                    //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());    
                    //} catch(GracianAuthorizationException $e){
                    //    $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        }catch (GracianValidationException $e) {     
            //$this->responseBag->set('validationErrors', $node->validationVO->transformOut());      
            $this->responseBag->set('validationErrors',  array('field' => $e->getUserMessage()) );
            $this->responseBag->set('fields', $fields);                
            $this->responseBag->set('redirect', '/admin/delete/' . $nodeName . '/' . $id);            
        } catch(GracianWarningException $e){    
            $this->responseBag->appendToFlashMessages('warning', $e->getUserMessage());      
            $this->responseBag->set('redirect', '/admin/delete/' . $nodeName . '/' . $id);    
        } catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        }catch(\Exception $e){
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());
        }  finally {  
            return $this->responseBag;   
        }   

    }
}
