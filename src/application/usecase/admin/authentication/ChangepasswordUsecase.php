<?php  namespace gracian_system\application\usecase\admin\authentication;

use gracian_system\domain\model\forgotpassword\ForgotpasswordEntity;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\application\usecase\base\BaseSiteUsecase;      

use gracian_system\application\service\AuthenticationService;    

class ChangepasswordUsecase extends BaseSiteUsecase{

    function __construct(){        
        parent::__construct();  
        $this->authenticationService = new AuthenticationService();     
    }    

    function execute($theRequest){     
        try{
            parent::execute($theRequest);

            $token = $this->authenticationService->sanitize($this->requestBag->request['get']['hashString']);
            $this->authenticationService->assertHashString($token);      

            $forgotpwEntity = new ForgotpasswordEntity();
            $item = $forgotpwEntity->getItemWithToken($token);

            $userEntity = new PrjUserEntity();
            $userItem = $userEntity->getItem($item['user_id']);

            $this->responseBag->set('email', $userItem['email']);
            $this->responseBag->set('token', $token);   
       
        }catch(GracianIntrusionException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());    
        }catch(GracianErrorException $e) {   
             $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());      
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error',  $e->getMessage());    
        } finally{
            return $this->responseBag;
        } 

    }
}
