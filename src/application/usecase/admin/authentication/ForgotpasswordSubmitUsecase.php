<?php  namespace gracian_system\application\usecase\admin\authentication;

use gracian_system\domain\model\forgotpassword\ForgotpasswordEntity;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\application\usecase\base\BaseSiteUsecase;
                                                                    
use gracian_system\application\service\AuthenticationService;        

class ForgotpasswordSubmitUsecase extends BaseSiteUsecase{

    function __construct(){        
        parent::__construct();  
        $this->authenticationService = new AuthenticationService();     
    }        

    function execute($theRequest){             
        try{
            parent::execute($theRequest);
            $fields['email'] = $this->authenticationService->sanitizeId($this->requestBag->request['post']['email']);
            $userEntity = new PrjUserEntity();
            $isValid = $this->validationService->validateInputFields($fields, $userEntity->getSchema());       
            
            $count_users = $userEntity->countUsersWithEmail($fields['email']);

            /*
            NOTE: if no user is found, ($count_users < 1) then don't mention it to the user.
            If you do mention if an email is present in the system or not,
            then a hacker can find out what the valid emialadresses are.
            Just say that a message is send, but don't send it.
            */

            if($count_users === 1){
                /* TODO: check  use_expire_date  an expire_date is in future
                   if exception, send message with reason.
                */     

                $userId = $userEntity->fetchIdWithEmail($fields['email']);
                if(!is_null($userId)){
                   // --- create record in table pw_change_request ---
                   // precondition: the usr_email is a valid one because a user_id is present now
                         
                    $forgotpasswordEntity = new ForgotpasswordEntity();

                    //first remove old tokens with same userId
                    $forgotpasswordEntity->destroyItemsOfId($userId);

                    // the long random string, unhashed
                    $id_string = $forgotpasswordEntity->setToken($userId);     
                    
                    // for unit testing
                    $this->responseBag->set('token', $id_string);

                    $company_name =  $this->config->companyName;
                    $reset_link = $this->config->app['base_url'] . $this->config->app['admin_basepath']  . '/changepassword/' . $id_string; 
                    $userItem = $userEntity->getItem($userId);
                    $nameFrom = $this->config->companyName;
                    $emailFrom = $this->config->companyEmail;
                    $nameTo = $userItem['fullname'];
                    $emailTo = $fields['email'];
                    $subject = "Notification: Reset your {$company_name} password";
                    $message = "Hi {$userItem['fullname']}\n"
                    . "\n"
                    . "To reset your password, please click the link below. \n"
                    . "Your changes here will update the password for {$company_name}.\n"
                    . $reset_link . "\n"
                    . "\n"
                    . "Thank you,\n"
                    . "{$company_name}\n";

                    $this->config->mailerAdapter->sendMail($emailFrom, $nameFrom, $emailTo, $nameTo, $subject, $message);

                    /*
                    // NOTE: don't mention this to the user. a hacker can misuse it to find real email-adresses.
                    */  
                    /* TODO:  you can log it...  */
                    //$type = 'notification'; 
                    //$log_arr = array('forgotpassword-email end to ' . $emailTo) ;
                    //$filePath   = 'authentication';
                    //$this->config->logAdapter->logReport($type, $log_arr, $filePath);

                   }
            }
        } catch(GracianIntrusionException $e) {     
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        } catch (GracianValidationException $e) {     
            $this->responseBag->set('fields', $fields);    
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));     
        } catch(GracianException $e) {   
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error',  $e->getMessage());    
        } finally{
            return $this->responseBag;
        }   
    }
}
