<?php  namespace gracian_system\application\usecase\admin\authentication;

use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\application\service\RepositoryFactory;
use gracian_system\application\usecase\base\BaseSiteUsecase;

class LogoutUsecase extends BaseSiteUsecase {

 

    public function execute($theRequest) {
        parent::execute($theRequest);
        /*
        NOTE: logout user by removing the login-array from the session (in the framework layer)
        */

        $login = $this->config->sessionVo->get('login');

        if(isset($login['fullname'])){     
            // On logout, set sessionVo to null ?    
            $this->responseBag->appendToFlashMessages('success', 'User '. $login['fullname'] . ' (' . $login['email'] .') logged out successfully.');
        } else {
            $this->responseBag->appendToFlashMessages('info', 'No user logged out.');
        }
        return $this->responseBag;
        //TODO: maybe better to return with ->transformOut(); (check the controller to see if it is possible)
    }
}
