<?php  namespace gracian_system\application\usecase\admin\authentication;

use gracian_system\domain\model\forgotpassword\ForgotpasswordEntity;
use gracian_system\application\usecase\base\BaseSiteUsecase;

class ForgotpasswordUsecase extends BaseSiteUsecase{



    function execute($theRequest){
        parent::execute($theRequest);

        $forgotpwEntity = new ForgotpasswordEntity();
        // start with cleaning the table       
        $forgotpwEntity->cleanup(); 
        $this->responseBag->set('companyName', $this->config->companyName);
        return $this->responseBag;
    }
}
