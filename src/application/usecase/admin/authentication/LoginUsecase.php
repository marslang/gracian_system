<?php  namespace gracian_system\application\usecase\admin\authentication;

use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\application\service\RepositoryFactory;
use gracian_system\application\usecase\base\BaseSiteUsecase;

class LoginUsecase extends BaseSiteUsecase{



    function execute($theRequest){
        parent::execute($theRequest); 
        return $this->responseBag;
    }

}
