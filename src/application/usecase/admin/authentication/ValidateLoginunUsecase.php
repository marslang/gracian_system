<?php  namespace gracian_system\application\usecase\admin\authentication;    

use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\application\service\RepositoryFactory;
use gracian_system\application\usecase\base\BaseSiteUsecase;       
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianValidationException;
use gracian_system\domain\exceptions\GracianIntrusionException;

use gracian_system\application\service\AuthenticationService;
use gracian_system\domain\exceptions\GracianAuthenticationException;                  

class ValidateLoginunUsecase extends BaseSiteUsecase{

    function __construct(){        
        parent::__construct();  
        $this->authenticationService = new AuthenticationService();     
    }             

    function execute($theRequest){
        try{            
            parent::execute($theRequest);
            $username = $this->authenticationService->sanitize($this->requestBag->request['post']['username']);
            $this->authenticationService->assertUsername($username);
            $password = $this->authenticationService->sanitize($this->requestBag->request['post']['password']);
            $this->authenticationService->assertPassword($password);

            $userEntity = new PrjUserEntity();
            $userItem = $userEntity->readByUsernamePassword($username,  $password);
            $this->responseBag->set('login', $userItem);
            $this->responseBag->appendToFlashMessages('success', 'User logged in successfully.');

        } catch(GracianIntrusionException $e) {     
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        } catch(GracianAuthenticationException $e) {    
             $this->responseBag->set('loginError', 'true');    
            $this->responseBag->appendToFlashMessages('info',  $e->getUserMessage());       
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error',  $e->getMessage());    
        }finally{
            return $this->responseBag;
        }     
    }
}
