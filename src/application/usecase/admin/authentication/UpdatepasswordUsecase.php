<?php  namespace gracian_system\application\usecase\admin\authentication;

use gracian_system\domain\model\forgotpassword\ForgotpasswordEntity;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\application\usecase\base\BaseSiteUsecase;      

use gracian_system\application\service\AuthenticationService;        

class UpdatepasswordUsecase extends BaseSiteUsecase{

    function __construct(){        
        parent::__construct();  
        $this->authenticationService = new AuthenticationService();     
    }          

    function execute($theRequest){    
        try{
            parent::execute($theRequest); 

            $token = $this->authenticationService->sanitize($this->requestBag->request['post']['forgotpassword_token']);
            //$this->authenticationService->assertHashString($token);     // Validator::alnum()->noWhitespace()->length(1,16)->setName('username');   

            $forgotpwEntity = new ForgotpasswordEntity();
            $item = $forgotpwEntity->getItemWithToken($token);
            //returns an error when token does not exist
            // TODO: check for outdated token

            if($this->requestBag->request['post']['password'] != ''){
                $fields['password'] = $this->authenticationService->sanitizePassword($this->requestBag->request['post']['password']);
            }

            $userEntity = new PrjUserEntity();
            $isValid = $this->validationService->validateInputFields($fields, $userEntity->getSchema());  
            // if validation errors, redirect to changepassword form

            $userEntity->updatePasswordForId($item['user_id'], $fields['password']);

            //remove token
            $forgotpwEntity->destroyItem($token);  
            $this->responseBag->appendToFlashMessages('success', 'Successfully changed the password.');    
    
        } catch (GracianValidationException $e) {
            $this->responseBag->set('token', $token);     
            $this->responseBag->set('validationErrors', (array) json_decode($e->getUserMessage()));   
        } catch(GracianIntrusionException $e) {   
            $this->responseBag->appendToFlashMessages('error',  $e->getUserMessage());    
        }catch(GracianException $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getUserMessage());
        } catch (\Exception $e) {
            $this->responseBag->appendToFlashMessages('error', $e->getMessage());   
        } finally {
            return $this->responseBag;         
        }
    
    }
}
