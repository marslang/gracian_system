<?php namespace gracian_system\application\infrastructurePorts;



interface CacheAdapterIF {


    //public function hasIntrusions();
    //public function getIntrusionReport();
    //public function ids_detect($request = array());         
    
    public function hasCachedItem($key, $ttl=null);    
    public function getCachedItem($key);
    public function setCachedItem($key, $data, $ttl=null);


}
