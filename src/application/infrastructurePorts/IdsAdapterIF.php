<?php namespace gracian_system\application\infrastructurePorts;



interface IdsAdapterIF {


    public function hasIntrusions();
    public function getIntrusionReport();
    public function ids_detect($request = array());        


}
