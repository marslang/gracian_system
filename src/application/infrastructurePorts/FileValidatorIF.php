<?php namespace gracian_system\application\infrastructurePorts;

interface FileValidatorIF
{
    public function validate($fields);

}
