<?php namespace gracian_system\application\infrastructurePorts;    


interface FileUploadIF {   
                                            
    public function uploadFile($target_dir, $subdir, $fileToUpload);
    public function removeOldFile($target_dir, $subdir, $oldFilename, $newFilename);
    public function removeFile($target_dir, $subdir, $filename);
    public function exists($target_dir, $subdir, $fileName);

/*
    public function save($fileToUpload, $target_dir, $subdir);
    public function remove($target_dir,  $subdir, $fileName);    
*/    
}