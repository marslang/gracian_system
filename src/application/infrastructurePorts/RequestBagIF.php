<?php namespace gracian_system\application\infrastructurePorts;

interface RequestBagIF {    

    public function transformIn($request);    
    public function transformOut();  
    public function hasFileToUpload();  

}