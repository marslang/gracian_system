<?php namespace gracian_system\application\infrastructurePorts;

interface SessionVoIF
{
    public function transformIn($session);
    public function get($key , $default=array());
    public function hasValidationErrors();  
}
