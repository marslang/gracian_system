<?php namespace gracian_system\application\infrastructurePorts;


interface MailerIF {


    public function sendMail($emailFrom, $nameFrom, $emailTo, $nameTo, $subject, $message);  

}
