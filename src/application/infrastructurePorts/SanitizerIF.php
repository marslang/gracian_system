<?php namespace gracian_system\application\infrastructurePorts;

interface SanitizerIF
{
    public function sanitize($fields, $sanitization_rules);

}
