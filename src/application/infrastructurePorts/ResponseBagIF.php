<?php namespace gracian_system\application\infrastructurePorts;

interface ResponseBagIF {    
    
    public function hasValidationErrors();
    public function hasLoginError();
    public function appendToFlashMessages($key , $value);
    public function hasFlashMessages($type);
    public function get($key);
    public function set($key , $value);
    public function transformOut();            

}