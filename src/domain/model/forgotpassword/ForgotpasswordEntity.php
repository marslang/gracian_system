<?php namespace gracian_system\domain\model\forgotpassword;

use gracian_system\domain\model\base\BaseEntity;
use gracian_project\application\service\ConfigFactory;
use gracian_system\infrastructure\repository\pdo\PdoForgotpasswordRepository;
use gracian_system\domain\exceptions\GracianException;
//use gracian_system\domain\exceptions\CustomValidationException;
use gracian_system\domain\model\base\entityIF;
use gracian_system\infrastructure\helpers\DbHelper;

class ForgotpasswordEntity extends BaseEntity{ 

    public $repositoryName = 'forgotpassword';

    //_____________________________________________________________________________________________
    public function __construct() {
        $configFactory = ConfigFactory::Instance();
        $this->config = $configFactory->config;
        $this->repository = $configFactory->config->repositoryFactory->getRepository($this->repositoryName);
        $this->hashAdapter = $configFactory->config->hashAdapter;
        $this->dbHelper = new DbHelper();
    }

    //_____________________________________________________________________________________________
    /* when calling forgotpassword-form */
    public function cleanup(){
        $this->repository->cleanup();
    }

    //_____________________________________________________________________________________________
    /*
    TODO read http://php.net/manual/en/function.uniqid.php
    for generating really unique ids
    */
    public function setToken($userId){
        $now = $this->dbHelper->getDateTimeNow();
        $id_string = uniqid();
        $id_hash = hash('md5', $id_string);
        $this->repository->store($id_hash, $now ,$userId);
        return $id_string;
    }

    //_____________________________________________________________________________________________
    public function getItemWithToken($token){
        $id_hash = hash('md5', $token);
        return $this->repository->fetchWithHash($id_hash);
    }

    //_____________________________________________________________________________________________
    public function destroyItem($token){
        $id_hash = hash('md5', $token);
        $this->repository->destroy($id_hash);
    }

    //_____________________________________________________________________________________________
    public function destroyItemsOfId($userId){
        $this->repository->destroyItemsOfId($userId);
    }
    /*
    //_____________________________________________________________________________________________
    //public function validateInputFields($fields){
    //    $this->validateFieldIfExists($fields, 'email',    'customValidator_email');
    //}
    */
}
