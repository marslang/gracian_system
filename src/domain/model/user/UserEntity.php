<?php namespace gracian_system\domain\model\user;

use gracian_project\application\service\ConfigFactory;
use gracian_project\domain\model\user\PrjUserEntity;
use gracian_system\domain\model\base\BaseEntity;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianAuthenticationException;
//use gracian_system\domain\exceptions\CustomValidationException;
use gracian_system\domain\model\base\entityIF;
use gracian_system\infrastructure\repository\pdo\PdoUserRepository;
use gracian_system\infrastructure\helpers\DbHelper;
use gracian_system\infrastructure\helpers\FormdateHelper;
use Respect\Validation\Exceptions\ValidationExceptionInterface;     
use gracian_system\domain\model\valueObjects\GracianConstants as G;    
use gracian_system\domain\model\valueObjects\SchemaVO;   
use gracian_system\domain\service\EntityService;    



class UserEntity extends BaseEntity{ 


    public $schema;

    public $repositoryName = 'user';
    public $repository = null;
    public $config = null;

    public $record = array();
    public $item = array();
    public $userList = array();

    public $formTemplate = 'user_form';

    public $entityName = 'user';
    public $nodeName = 'user';

    
    public $commonFieldNames = array(
        'id'   => array('sanitize' => 'purge', 'validate' => 'required|integer'),        
        'username' => array('sanitize' => 'purge', 'validate' => 'alpha_num|lengthMax:255'),
        'email' => array('sanitize' => 'purge', 'validate' => 'email'),
        'password' => array('sanitize' => 'raw', 'validate' => 'password4t2x'), 
        'fullname' => array('sanitize' => 'purge', 'validate' => 'title|lengthMax:255'),   
        'role' => array('sanitize' => 'purge', 'validate' => 'alpha_num|lengthMax:255'),
        'remember_token' => array('sanitize' => 'raw', 'validate' => 'string'),  
        'creation_date' => array('sanitize' => 'purge', 'validate' => 'required|mysqldate'),     
        'modify_date' => array('sanitize' => 'purge', 'validate' => 'required|mysqldate'),     
        'expire_date' => array('sanitize' => 'purge', 'validate' => 'required|mysqldate'),     
        'use_expire_date' => array('sanitize' => 'purge', 'validate' => 'lengthMax:1'),     
    );
          

    
    public $acl = Array(
    "anon"       => Array(G::CREATE=>G::DENIED,    G::READ=>G::DENIED,  G::UPDATE=>G::DENIED,    G::DELETE=>G::DENIED),
    "su"         => Array(G::CREATE=>G::ALLOWED,   G::READ=>G::ALLOWED, G::UPDATE=>G::ALLOWED,   G::DELETE=>G::ALLOWED),
    "admin"      => Array(G::CREATE=>G::DENIED,    G::READ=>G::DENIED,  G::UPDATE=>G::DENIED,    G::DELETE=>G::DENIED),
    "user_admin" => Array(G::CREATE=>G::ALLOWED,   G::READ=>G::ALLOWED, G::UPDATE=>G::ALLOWED,   G::DELETE=>G::ALLOWED),
    );    


    //_____________________________________________________________________________________________
    public function __construct() {
        parent::__construct();
        $this->config = ConfigFactory::Instance()->config;
        $this->repository = $this->config->repositoryFactory->getRepository($this->repositoryName);
        $this->hashAdapter = $this->config->hashAdapter;
        $this->dbHelper = new DbHelper();    
        $this->entityService = new EntityService();  
        $this->schemaVO = new SchemaVO($this->commonFieldNames);

    }    
     



    //_____________________________________________________________________________________________
    public function mergeItemWithChangedFormfields($fields){
        $this->item = $this->entityService->mergeItemWithFields($this->item, $fields);    
    }
   
   
    //_____________________________________________________________________________________________
    public function fetchItem($id){
        $this->item = $this->repository->read($id);
        $user = $this->config->sessionVo->get('login');
        $this->item['permission'] = $this->authorizationService->collectPermissionsForUser($this, $user);
    }

    //_____________________________________________________________________________________________
    public function getItem($id){
        return $this->repository->read($id);
    }

    //_____________________________________________________________________________________________
    public function readByEmailPassword($email, $password){
        $hashedPw = $this->repository->readPasswordByEmail($email);
        if($hashedPw == '*'){
           // No user found : make the error msg the same to hide the content details
           throw new GracianAuthenticationException('E-mail or password is invalid', 'No User found with this email address');
        }
        if ($this->hashAdapter->checkPassword($password, $hashedPw) === FALSE ) {
            // Authentication failed
            throw new GracianAuthenticationException('E-mail or password is invalid', 'User found but password is wrong');
        }
        $result = $this->repository->readByEmailPassword($email, $hashedPw);
        $user = null;
        $count = count($result);  
        if($count < 1){
            throw new GracianAuthenticationException('E-mail or password is invalid', 'No user found');
        }
        if($count > 1){
             throw new GracianAuthenticationException('More than one user found');
        }
        if($count === 1){   
           $user =  $result[0];
           if($user['use_expire_date'] > 0){    
               $t_now = strtotime(date($this->dbHelper->getDateTimeNow()));
               $t_expire = strtotime(date($user['expire_date']));   
               if($t_expire < $t_now){
                   throw new GracianAuthenticationException('User found but activity is expired');
               }
           }
        }
        return $user;
    }

    //_____________________________________________________________________________________________
    public function readByUsernamePassword($username, $password){
        $hashedPw = $this->repository->readPasswordByUsername($username);
        if($hashedPw == '*'){
           // make the error msg the same to hide the content details
           throw new GracianAuthenticationException('Username or password is invalid');
        }
        if ($this->hashAdapter->checkPassword($password, $hashedPw) === FALSE ) {
            // Authentication failed
            throw new GracianAuthenticationException('Username or password is invalid', 'User found but password is wrong');
        }
        $result = $this->repository->readByUsernamePassword($username, $hashedPw);
        $user = null;
        $count = count($result);
        if($count < 1){
            throw new GracianAuthenticationException('Username or password is invalid', 'No user found ');
        }
        if($count > 1){
             throw new GracianAuthenticationException('More than one user found');
        }
        if($count == 1){
           $user =  $result[0];
           if($user['use_expire_date'] == 1){
               $t_now = strtotime(date($this->dbHelper->getDateTimeNow()));
               $t_expire = strtotime(date($user['expire_date']));
               if($t_expire < $t_now){
                   throw new GracianAuthenticationException('User found but activity is disabled');
               }
           }
        }
        return $user;
    }


    //_____________________________________________________________________________________________
    public function storeFields( $fields){
        if(!isset($fields['use_expire_date'])){ 
            $fields['use_expire_date'] = 0; }else{ $fields['use_expire_date'] = 1; 
        }
        $this->getEmptyItem();
        $this->item = $this->entityService->mergeItemWithFields($this->item, $fields);
        unset($this->item['id']);
        $this->item['password'] = $this->hashAdapter->hash($this->item['password']);
        //$this->filterItemWithWhitelist();   
        $this->item = $this->entityService->filterItemWithWhitelist($this->item, $this->commonFieldNames);    


        //NOTE: if username is empty, fill it with a random hash.
        if($this->item['username'] == ''){
            $now = $this->dbHelper->getDateTimeNow();
            $subject = $this->hashAdapter->hash($this->item['fullname'] . $now);
            $search  = array( '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '{', '}', '[', ']', ',', '.', '/', '?' ) ;
            $replace  = array( 'q', 'w', 'e', 'r', 't', 'y', 'U', 'I', 'O', 'P', 'A', 'S', '1', '2', '3', '4', '5', '6', '7' ) ;
            $this->item['username'] = str_replace($search, $replace, $subject);
        }

        $records = $this->repository->fetchOthersWithUsername(0, $this->item['username']);
        if(count($records) > 0){
            throw new GracianException('UserEntity: Cannot store user, username is already taken.');
        }
        // the store method returns the new id of the created record
        return $this->repository->store($this->item);
    }
     /*
    //_____________________________________________________________________________________________
    private function filterItemWithWhitelist(){
        // NOTE: only allow the known fields
        $item_to_store= array();
        foreach($this->commonFieldNames as $k => $v){
            if(isset($this->item[$k])){
                $item_to_store[$k] = $this->item[$k];
            }
        }
        $this->item = $item_to_store;
    } 
    */

    //_____________________________________________________________________________________________
    public function destroyItem($id){
        $this->repository->destroy($id);
    }

    //_____________________________________________________________________________________________
    public function listAll(){
        $user = $this->config->sessionVo->get('login');
        $this->permission = $this->authorizationService->collectPermissionsForUserNode($this, $user);
        $list = $this->repository->listAll();
        $tmpNode = new PrjUserEntity();
        foreach($list as $k => $item){
            $tmpNode->item = $item;
            $list[$k]['permission'] = $this->authorizationService->collectPermissionsForUser($tmpNode, $user);
        }
        $this->userList = $list;
    }

    //_____________________________________________________________________________________________
    /* If pw is empty, ignore it. If pw is filled, replace it. */
    public function updateItem($fields){
        if(isset($fields['password'])){
          if(trim($fields['password']) == ''){
            unset($fields['password']);
          }else{
            $fields['password'] = $this->hashAdapter->hash($fields['password']);
          }
        }
        $fields['modify_date'] = $this->dbHelper->getDateTimeNow();
        //precondition: check if username is taken (if it is filled)
        $records = $this->repository->fetchOthersWithUsername($fields['id'], $fields['username']);
        if(count($records) > 0){
            throw new GracianException('UserEntity: Cannot update user, username is already taken.');
        }
        //precondition:
        if(isset($fields['password'])){
            if(trim($fields['password']) == ''){
                throw new GracianException('UserEntity: cannot update user, password is empty.');
            }
            $this->repository->updateIncludingPassword($fields);
        }else{
            $this->repository->updateExcludingPassword($fields);
        }
    }

    //_____________________________________________________________________________________________
    public function updatePasswordForId($id, $password){
        $hashedPassword = $this->hashAdapter->hash($password);
        $this->repository->updatePasswordForId($id, $hashedPassword);
    }

    //_____________________________________________________________________________________________
    public function getEmptyItem(){
        if(!isset($this->item['use_expire_date'])){
            $this->setEmptyItem();
        }
        if($this->item['use_expire_date'] !== 1){
            $this->setEmptyItem();
        }
        return $this->item;
    }

    //_____________________________________________________________________________________________
    public function setEmptyItem(){
      $now = $this->dbHelper->getDateTimeNow();
      $this->item = array();
      foreach($this->commonFieldNames as $k => $v){
          $this->item[$k] = '';
      }
      $this->item['creation_date'] = $now;
      $this->item['modify_date'] = $now;
      $this->item['expire_date'] = $now;
      $this->item['use_expire_date'] = 1;
      $user = $this->config->sessionVo->get('login');
      $this->item['permission'] = $this->authorizationService->collectPermissionsForUserNode($this, $user);
    }

    //_____________________________________________________________________________________________
    public function transformOut(){
        if(isset($this->item['expire_date'])){
            if($this->item['expire_date'] == ''){
                $this->item['expire_date'] = $this->dbHelper->getDateTimeNow();
            }
            $fd = new FormdateHelper();
            $this->item['formdate_expire_date'] = $fd->get_formdate($this->item['expire_date'], 'formdate-expire_date-');
        }
        $data = new \stdClass();
        if(isset($this->item)){$data->item = $this->item;}
        $data->commonFieldNames = $this->commonFieldNames;
        if(isset($this->userList)){$data->userList = $this->userList;}
        if(isset($this->permission)){$data->permission = $this->permission;}
        return $data;
    }

    //_____________________________________________________________________________________________
    public function countUsersWithEmail($email){
        return $this->repository->countRecordsWithSameEmail($email);
    }

    //_____________________________________________________________________________________________
    public function fetchIdWithEmail($email){
        return $this->repository->fetchIdWithEmail($email);
    }


}
