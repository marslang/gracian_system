<?php namespace gracian_system\domain\model\valueObjects; 

class Fields_White_listVO implements \IteratorAggregate
{
    private $items = array();
    private $count = 0;

    // Required definition of interface IteratorAggregate
    public function getIterator() {
        return new \MyIterator($this->items);
    }

    public function add($key, $value) {
        $this->items[$key] = $value;
    }
}