<?php namespace gracian_system\domain\model\valueObjects;

class GracianConstants { 

    const ALLOWED = 'ALLOWED';     
    const DENIED = 'DENIED';     
    const OWNER = 'OWNER';     
    const MEMBER = 'MEMBER';   
      
    const CREATE = 'create';     
    const READ = 'read';     
    const UPDATE = 'update';     
    const DELETE = 'delete';         

}
                       