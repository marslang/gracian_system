<?php namespace gracian_system\domain\model\valueObjects;

class ValidationVO { 

    public $validationErrors = array();

    //_____________________________________________________________________________________________
    public function append($name, $value){    
       $this->validationErrors[$name] = $value;  
    }         
  
    //_____________________________________________________________________________________________
    public function transformOut(){
        return $this->validationErrors;
    }     
    
    //_____________________________________________________________________________________________
    public function hasValidationErrors(){      
        if(count($this->validationErrors) > 0){
            return TRUE;
        } 
        return FALSE;
    }           

}
