<?php namespace gracian_system\domain\model\valueObjects;   

//use gracian_system\domain\exceptions\GracianAssertException;  


class SchemaVO {
     
    private $schema = array();
  
                               
    //_____________________________________________________________________________________________
    //with lambdaFunction  
    public function __construct($commonFieldNames, $variableFieldNames=array()){
        //$this->commonFieldNames, $this->variableFieldNames      
/*
        
from   'id'        => array('sanitize' => 'purge', 'validate' => 'required|integer'),   
       'subdir'    => array('sanitize' => 'purge', 'validate' => 'required|alnum|min:2|max:2'),

to     $this->schema['id']['sanitizers']['purge'] = Array();  
        $this->schema['id']['validators']['required'] = Array();   
        $this->schema['id']['validators']['integer'] = Array();   
        //$this->schema['id']['validators']['length'] = Array('min'=> 11, 'message' => 'MIN_LENGTH');   

*/      $this->transformIn($commonFieldNames, $variableFieldNames);  
//print_arr($this->schema); exit(); 

    }           
    
    private function transformIn($commonFieldNames, $variableFieldNames){       
        if(count($commonFieldNames) > 0){            
            foreach($commonFieldNames as $k => $v){   
                $this->schema[$k] = array(); 
                if(isset($v['sanitize'])){                    
                    $pieces = explode('|', $v['sanitize']) ;
                    foreach($pieces as $pk => $pv){
                        $this->schema[$k]['sanitizers'][$pv] = Array();  
                    }    
                }
                
                if(isset($v['validate'])){                    
                    $pieces = explode('|', $v['validate']) ;
                    foreach($pieces as $pk => $pv){     
                        $args = explode(':', $pv) ;
                        if(count($args) < 2){
                            $this->schema[$k]['validators'][$pv] = Array();  
                        }else{    
                             $this->schema[$k]['validators'][$args[0]] = $this->get_valueArray($args);          

                        }
                    }
                }
            }   
        }
        
        if(count($variableFieldNames) > 0){ 
            foreach($variableFieldNames as $k => $v){    
                if(isset($v['sanitize'])){                    
                    $pieces = explode('|', $v['sanitize']) ;
                    foreach($pieces as $pk => $pv){
                        $this->schema['variable_fields'][$k]['sanitizers'][$pv] = Array();  
                    }   
                }                                             
                if(isset($v['validate'])){                                    
                    $pieces = explode('|', $v['validate']) ;
                    foreach($pieces as $pk => $pv){   
                        $args = explode(':', $pv) ;
                        if(count($args) < 2){
                            $this->schema['variable_fields'][$k]['validators'][$pv] = Array();  
                        }else{   
                            $this->schema['variable_fields'][$k]['validators'][$args[0]] = $this->get_valueArray($args); 
                        }
                    }                
                }
            }       
        }           
    }      
    
    private function get_valueArray($args){
        if($args[0] == 'equals'){ return array('field' => $args[1]); }      
        if($args[0] == 'different'){ return array('field' => $args[1]); }      
        if($args[0] == 'length'){ return array('value' => $args[1]); }      
        if($args[0] == 'lengthBetween'){ return array('min' => $args[1], 'max' => $args[2]); }      
        if($args[0] == 'lengthMin'){ return array('value' => $args[1]); }     
        if($args[0] == 'lengthMax'){ return array('value' => $args[1]); }     
        if($args[0] == 'min'){ return array('value' => $args[1]); }     
        if($args[0] == 'max'){ return array('value' => $args[1]); }      
        if($args[0] == 'in'){ return array('values' => $args[1]); }      
        if($args[0] == 'notIn'){ return array('values' => $args[1]); }      
        
        
        
        
    }
    
    
/* --- valitron validation rules ---  
required - Required field
equals - Field must match another field (email/password confirmation)
different - Field must be different than another field
accepted - Checkbox or Radio must be accepted (yes, on, 1, true)
numeric - Must be numeric
integer - Must be integer number
array - Must be array
length - String must be certain length
lengthBetween - String must be between given lengths
lengthMin - String must be greater than given length
lengthMax - String must be less than given length
min - Minimum
max - Maximum
in - Performs in_array check on given array values
notIn - Negation of in rule (not in array of values)
ip - Valid IP address
email - Valid email address
url - Valid URL
urlActive - Valid URL with active DNS record
alpha - Alphabetic characters only
alphaNum - Alphabetic and numeric characters only
slug - URL slug characters (a-z, 0-9, -, _)
regex - Field matches given regex pattern
date - Field is a valid date
dateFormat - Field is a valid date in the given format
dateBefore - Field is a valid date and is before the given date
dateAfter - Field is a valid date and is after the given date
contains - Field is a string and contains the given string
creditCard - Field is a valid credit card number
instanceOf - Field contains an instance of the given class
*/     
    
    public function getSchema(){
        return $this->schema;
    }
    

            
    
    

     
    
}    
