<?php namespace gracian_system\domain\model\stree;

class StreeMemberType extends Stree{

    public $repositoryName = 'stree';
    public $nodeType = 'streeMemberType';

    public $viewTemplate = 'StreeMemberViewTpl';
    public $formTemplate = 'StreeMemberFormTpl';
    public $variableFieldNames = array( 
        'user_id' => array('sanitize' => 'purge', 'validate' => 'required|integer')    
    );
 


    //_____________________________________________________________________________________________
    /* put the member-user-id in the owner-id filed.
     * so the acl authorization can stay simple
     */
    public function updateItem($id, $item){
        $item['owner_id'] = $item['variable_fields']['user_id'];
        parent::updateItem($id, $item);
    }

    //_____________________________________________________________________________________________
    public function storeFields($parentId, $fields){   
        if(!isset($fields['variable_fields']['user_id'])){
            echo 'Error: gracian_system/.../StreeMemberType::storeFields(): $fields[variable_fields][user_id] not found.' . "\n"; exit();
        }
        $fields['owner_id'] = $fields['variable_fields']['user_id'];
        return parent::storeFields($parentId, $fields);
    }


}
