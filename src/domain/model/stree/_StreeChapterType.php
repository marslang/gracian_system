<?php namespace gracian_system\domain\model\stree;

class StreeChapterType extends Stree{ 

    public $repositoryName = 'stree';
    public $nodeType = 'streeChapterType';

    public $viewTemplate = 'StreeChapterViewTpl';
    public $formTemplate = 'StreeChapterFormTpl';
    //public $variableFieldNames = array( 'introduction' => '');    
    public $variableFieldNames = array( 
        'introduction' => array('sanitize' => 'purge', 'validate' => 'string|max:255')        
    );



}
