<?php namespace gracian_system\domain\model\stree;

class StreeRootType extends Stree{

    public $repositoryName = 'stree';
    public $nodeType = 'streeRootType';
    public $viewTemplate = 'StreeRootViewTpl';
    public $formTemplate = 'StreeRootFormTpl';
    public $variableFieldNames = array();


}
