<?php namespace gracian_system\domain\model\stree;

class StreeImageType extends Stree{ 

    public $repositoryName = 'stree';
    public $nodeType = 'streeImageType';

    public $viewTemplate = 'StreeImageViewTpl';
    public $formTemplate = 'StreeImageFormTpl';
    public $variableFieldNames = array(
        'caption' => array('sanitize' => 'purge', 'validate' => 'string'),        
        'copyright' => array('sanitize' => 'purge', 'validate' => 'string')        
    );

 
}
