<?php namespace gracian_system\domain\model\stree;    

class StreeRepositoryDecorator implements StreeRepositoryIF {   
           
    private $repository;   
 
    //_____________________________________________________________________________________________      
    public function __construct(StreeRepositoryIF $repository){ 
        $this->repository = $repository;      
    }                    
    
    

    //_____________________________________________________________________________________________      
    // the repository functions
    //_____________________________________________________________________________________________      
    public function getItem($id, $fields){    
        $record = $this->repository->getItem($id);     
        return $this->jsonToVariableAndOrphanFields($record, $fields); 
    }          
    
    //_____________________________________________________________________________________________      
    public function getItemForCrumb($id){    
        return $this->repository->getItemForCrumb($id);        
    }    
    
    //_____________________________________________________________________________________________      
    public function getItemsOfParentOfNode($id, $nodeName){
       return $this->repository->getItemsOfParentOfNode($id, $nodeName);
    }  
    
    //_____________________________________________________________________________________________      
    public function listChildrenOfParent($parent_id){     
        return $this->repository->listChildrenOfParent($parent_id);
    }    
    
    //_____________________________________________________________________________________________      
    public function countChildrenOfParent($parent_id){           
        return $this->repository->countChildrenOfParent($parent_id);
    }     
    
    //_____________________________________________________________________________________________      
    public function getEmptyItem($parentId=0 , $nodeName=''){
        return $this->repository->getEmptyItem($parentId=0 , $nodeName);
    }       
   
    //_____________________________________________________________________________________________      
    public function store($fields){      
        $fields = $this->variableAndOrphanFieldsToJson($fields) ;  
        return $this->repository->store($fields);    
    }    
    
    //_____________________________________________________________________________________________      
    public function update($id, $fields){     
        $fields = $this->variableAndOrphanFieldsToJson($fields);   
        $this->repository->update($id, $fields);  
    }      
    
    //_____________________________________________________________________________________________      
    public function destroy($id){   
        $this->repository->destroy($id);
    }
    
    //_____________________________________________________________________________________________ 
    // the decorator functions:     
    //_____________________________________________________________________________________________      
    private function jsonToVariableAndOrphanFields($record, $fields){    
        $record['variable_fields'] = array(); 
        $uncompressed = json_decode($record['variable_fields_json']);  
        if(count($uncompressed) > 0){
            foreach($uncompressed as $k => $v){     
                if(in_array($k , $fields)){
                    throw new Exception('Error in StreePdoRepositoryDecorator: a json-fieldname is the same as a table fieldname. ');
                }
                $record['variable_fields'][$k] = $v;
            }
        }  
        return $record;
    }      
    
    //_____________________________________________________________________________________________      
    private function variableAndOrphanFieldsToJson($fields){                
        $fields['variable_fields_json'] = json_encode($fields['variable_fields']);   
        unset($fields['variable_fields']);    
        return($fields);
    }      
                     
                     
}                    