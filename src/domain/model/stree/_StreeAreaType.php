<?php namespace gracian_system\domain\model\stree;

class StreeAreaType extends Stree{

    public $repositoryName = 'stree';
    public $nodeType = 'streeAreaType';
    public $viewTemplate = 'StreeAreaViewTpl';
    public $formTemplate = 'StreeAreaFormTpl';
    public $variableFieldNames = array( 
        'introduction' => array('sanitize' => 'purge', 'validate' => 'string|max:255')           
    );


}
