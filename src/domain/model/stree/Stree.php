<?php namespace gracian_system\domain\model\stree;

use gracian_project\application\service\ConfigFactory;
use gracian_project\domain\service\NodeFactory;
use gracian_system\domain\model\base\entityIF;
use gracian_system\domain\model\base\BaseEntity;


use gracian_system\domain\model\valueObjects\SchemaVO;                                                        
   
use gracian_system\domain\service\JsonFieldsService;
use gracian_system\domain\exceptions\GracianWarningException;     
//use gracian_system\domain\exceptions\CustomValidationException;
use gracian_system\infrastructure\repository\pdo\PdoStreeRepository;
use gracian_system\infrastructure\helpers\DbHelper;
use gracian_system\infrastructure\helpers\FormdateHelper;
use Respect\Validation\Exceptions\ValidationExceptionInterface;   
use gracian_system\domain\service\EntityService;     
use gracian_system\domain\service\CrumbService;    
use gracian_system\domain\service\ImagelibService;    
                                                  
/*
 consider using the StreeRepositoryDecorator  
*/

abstract class Stree extends BaseEntity{ 

    public $formTemplate = 'StreeArticleType';
    public $variableFieldNames = array();

    public $permission;

    public $repositoryName = 'stree';
    public $repository = null;

    public $item = null;
    public $crumbItem;        
    public $crumb = null;      
    public $tree = null;
    public $siblingItems;
    public $childrenCollection;
    public $childCount;       
    
    public $moduleViewPath = '';  


    /* NOTE: 
       this is a whitelist of allowed fields 
    */
    /*
       commonFieldNames unused fields:
       // 'id' => '', // exclude the id (for store and for update)
       // 'baseclass_name',
       // 'article_bodytext'      
       
    */      
    
    public $acl = Array(
    "anon"       => Array("create"=>"DENIED",    "read"=>"ALLOWED", "update"=>"DENIED",    "delete"=>"DENIED"),
    "su"         => Array("create"=>"ALLOWED",    "read"=>"ALLOWED", "update"=>"ALLOWED",    "delete"=>"ALLOWED"),
    "admin"      => Array("create"=>"ALLOWED",    "read"=>"ALLOWED", "update"=>"ALLOWED",    "delete"=>"ALLOWED"),
    "user_admin" => Array("create"=>"DENIED",    "read"=>"ALLOWED", "update"=>"DENIED",    "delete"=>"DENIED"),
    );       
    
    private $commonFieldNames = array(
       'id'        => array('sanitize' => 'purge', 'validate' => 'required|integer'),
       'parent_id' => array('sanitize' => 'purge', 'validate' => 'required|integer'),
       'owner_id'  => array('sanitize' => 'purge', 'validate' => 'required|integer'),
       'order_nr'  => array('sanitize' => 'purge', 'validate' => 'required|integer'),
       'publish'   => array('sanitize' => 'purge', 'validate' => ''),
       'publish_start_date' => array('sanitize' => 'purge', 'validate' => 'required|mysqldate'),
       'subdir'    => array('sanitize' => 'purge', 'validate' => 'required|alpha_num|min:2|max:2'),
       'filename'  => array('sanitize' => 'purge', 'validate' => 'filename|max:255'),
       'unique_name' => array('sanitize' => 'purge', 'validate' => 'alpha_num|max:255'),
       'node_name' => array('sanitize' => 'purge', 'validate' => 'required|alpha_dash|max:255'),
       'nodetype_name' => array('sanitize' => 'purge', 'validate' => 'required|alpha_dash|max:255'),
       'template_name' => array('sanitize' => 'purge', 'validate' => 'alpha_dash|max:255'),
       'title' => array('sanitize' => 'purge', 'validate' => 'required|string|max:255'),
       'creation_date' => array('sanitize' => 'purge', 'validate' => 'required|mysqldate'),   
       'modify_date' => array('sanitize' => 'purge', 'validate' => 'required|mysqldate'),   
       'variable_fields_json' => array('sanitize' => 'raw', 'validate' => 'string'),     
       // variable_fields is send in the form-post
       'variable_fields' => array(),     
       'orphan_fields' => array(),     
    );     
    
	private	$image_resize = array(
	    'small'  => Array( 'width' => 240, 'height' => 220),
		'medium' => Array( 'width' => 720, 'height' => 720),
	    'large'  => Array( 'width' => 1024, 'height' => 1024)   
    );


    public $remove_fields = array(
        '_token' , 'submit'
    );

    //_____________________________________________________________________________________________
    public function __construct() {
        parent::__construct();
        $this->config = ConfigFactory::Instance()->config;
        $this->repository = $this->config->repositoryFactory->getRepository($this->repositoryName);
        $this->nodeFactory = new NodeFactory();
        $this->dbHelper = new DbHelper();
        $this->jsonFieldsService = new JsonFieldsService();    
        $this->entityService = new EntityService(); 
        $this->schemaVO = new SchemaVO($this->commonFieldNames, $this->variableFieldNames);
        $this->crumbService = new CrumbService();                
        $this->imagelibService = new ImagelibService($this->config);                
    }     
    

    public function transformOutListItem($item, $type ='default'){
        if($type == 'default'){
            $result = $item;
        }
        if($type == 'site'){
            //print_r()   
            $result['id'] = $item['id'];     
            $result['parent_id'] = $item['parent_id'];     
            $result['publish'] = $item['publish'];     
            $result['node_name'] = $item['node_name'];     
            $result['title'] = $item['title'];     
            $result['permission'] = $item['permission'];  
            //foreach($item['variable_fields'] as $k => $v){  
            //    $result[$k] = $v;     
            //}
        }  
        //print_r($result); exit();
        return $result;
    }


    //_____________________________________________________________________________________________        
    /*
    public function transformOut($type = 'default'){     
        if($type == 'site'){
            return  $this->transformOutSite();
        }else{
            return  $this->transformOutDefault();
            
        } 
    }   
    */
    public function transformOutSite(){     
        //echo '<pre>';	debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS); echo'</pre>';

        //print_r($this->item);
        //resize item images and setUrl         
        $this->item['file_url'] = $this->imagelibService->getFileUrlArray($this->item['subdir'], $this->item['filename'], $this->image_resize);
        /*
        if($this->imagelibService->isImage($this->item['filename'])){      
            $this->item['file_url'] = $this->imagelibService->getFileResizedUrls($this->item['subdir'], $this->item['filename'], $this->image_resize);
        }else{
            $this->item['file_url'] = $this->imagelibService->getFileOriginalUrl($this->item['subdir'], $this->item['filename']);
        }    
        */
        //resize sibling images and setUrl  
        if(count($this->siblingItems) > 0){      
            foreach($this->siblingItems as $k => $v){
                $this->siblingItems[$k]['file_url'] = $this->imagelibService->getFileUrlArray($this->siblingItems[$k]['subdir'], $this->siblingItems[$k]['filename'], $this->image_resize);
            } 
            /*
                if($this->imagelibService->isImage($this->siblingItems[$k]['filename'])){    
                    $this->siblingItems[$k]['file_url'] = $this->imagelibService->getFileResizedUrls($this->siblingItems[$k]['subdir'], $this->siblingItems[$k]['filename'], $this->image_resize);
                }else{
                    $this->siblingItems[$k]['file_url'] = $this->imagelibService->getFileOriginalUrl($this->siblingItems[$k]['subdir'], $this->siblingItems[$k]['filename']);
                }              
            */
        }
        
        //$nodeData = new \stdClass();      
        $data = array();

        $data['nodeName'] = $this->nodeName;
        $data['permission'] = $this->permission;
        
        $data['item'] = $this->item;
        if(isset($this->crumb)){
            $data['crumbPath'] = $this->crumb;    
        }
        $data['crumbItem'] = $this->crumbItem;

        //$data['siblings'] = $this->siblingItems;   
        $data['siblings'] = array();  
        //print_r($this->siblingItems);   
        if(count($this->siblingItems) > 0){            
            foreach($this->siblingItems as $k => $v){   
                $data['siblings'][$k] = $this->transformOutListItem($v, 'site' );
            }    
        }
        
        //$data['children'] = $this->childrenCollection;     
        $data['children'] = array();
        foreach($this->childrenCollection as $k => $v){
             $data['children'][$k] = array();
             foreach($v->siblingItems as $kk => $vv){
                 $data['children'][$k][$kk]['id']  = $vv['id'];
                 $data['children'][$k][$kk]['publish'] = $vv['publish'];
                 $data['children'][$k][$kk]['title'] = $vv['title'];    
                 $data['children'][$k][$kk]['node_name'] = $vv['node_name'];   
                 foreach($vv['variable_fields'] as $kkk => $vvv){
                     $data['children'][$k][$kk][$kkk] = $vvv;
                 }   
                 $data['children'][$k][$kk]['permission'] = $vv['permission']; 
                 if(isset($vv['file_url'])){ $data['children'][$k][$kk]['file_url'] = $vv['file_url'];}
             }
        }  
        
        
        
        //$data->childCount = $this->childCount;
        //$data->viewTemplate = $this->viewTemplate;
        //$data->formTemplate = $this->formTemplate;
        //$data->variableFieldNames = $this->variableFieldNames;    
        //$data->moduleViewPath = $this->moduleViewPath;   
        //echo '--- xxx----';
        if(isset($this->tree)){  
                foreach($this->tree['root'] as $rootFieldName => $rootFieldValue){     
                    if(is_array($rootFieldValue) && $rootFieldName == 'children'){
//            echo '===';      exit();
                        foreach($this->tree['root']['children'] as $chapterName => $chapterItemList){
                            foreach($this->tree['root']['children'][$chapterName] as $chapterIndex => $chapterItem){
                                 $data['tree']['root']['children'][$chapterName][$chapterIndex] = $this->transformOutListItem($chapterItem, 'site' );                             
                                 if(isset($chapterItem['children']) && count($chapterItem['children']) > 0){                                 
                                     foreach($this->tree['root']['children'][$chapterName][$chapterIndex]['children'] as $pageName => $pageItemList){
                                         foreach($this->tree['root']['children'][$chapterName][$chapterIndex]['children'][$pageName] as $pageIndex => $pageItem){
                                             $data['tree']['root']['children'][$chapterName][$chapterIndex]['children'][$pageName][$pageIndex] = $this->transformOutListItem($pageItem, 'site' );                             

                                         }
                                     }
                                 }
                            }   
                        }
                    }else{
                        $data['tree']['root'][$rootFieldName] = $rootFieldValue;
                    }
                }
            //$data->tree = $this->tree;    
            
        }
        
        /*
        echo '--- Stree->transformOut() tree ---';
        echo '<pre>';
        print_r($data->tree);            
        echo '</pre>';
        exit(); 
        */
        return $data;      
    }        
    
                            
    /*
      maybe also add nodeData->acl = this->acl  
    */         
    public function transformOut(){   

           
        // there is an item set: 
        if(count($this->item) > 0 ){     
            if(isset($this->item['publish_start_date'])){
                $fd = new FormdateHelper();
                $this->item['formdate_publish_start_date'] = $fd->get_formdate($this->item['publish_start_date'], 'formdate-publish_start_date-');
            }        
            
            $this->item['file_url'] = $this->imagelibService->getFileUrlArray($this->item['subdir'], $this->item['filename'], $this->image_resize);


        }
        
        
        //resize sibling images and setUrl  
        if(count($this->siblingItems) > 0){    
            foreach($this->siblingItems as $k => $v){  
                $this->siblingItems[$k]['file_url'] = $this->imagelibService->getFileUrlArray($this->siblingItems[$k]['subdir'], $this->siblingItems[$k]['filename'], $this->image_resize);
/*
                if(is_file($this->siblingItems[$k]['filename'])){
                    
                    if($this->imagelibService->isImage($this->siblingItems[$k]['filename'])){    
                        $this->siblingItems[$k]['file_url'] = $this->imagelibService->getFileResizedUrls($this->siblingItems[$k]['subdir'], $this->siblingItems[$k]['filename'], $this->image_resize);
                    }else{
                        $this->siblingItems[$k]['file_url'] = $this->imagelibService->getFileOriginalUrl($this->siblingItems[$k]['subdir'], $this->siblingItems[$k]['filename']);
                    }
                }              
*/                
            }
        }
        
        $nodeData = new \stdClass();
        $nodeData->nodeName = $this->nodeName;
        $nodeData->permission = $this->permission;
        
        $nodeData->item = $this->item;
        if(isset($this->crumb)){
            $nodeData->crumb = $this->crumb;    
        }
        if(isset($this->tree)){
            $nodeData->tree = $this->tree;    
        }
        $nodeData->crumbItem = $this->crumbItem;
        $nodeData->siblingItems = $this->siblingItems;
        $nodeData->childrenCollection = $this->childrenCollection;
        $nodeData->childCount = $this->childCount;
        $nodeData->viewTemplate = $this->viewTemplate;
        $nodeData->formTemplate = $this->formTemplate;
        $nodeData->variableFieldNames = $this->variableFieldNames;    
        $nodeData->moduleViewPath = $this->moduleViewPath;   
        /*
        echo '--- Stree->transformOut() ---';
        echo '<pre>';
        print_r($nodeData);            
        echo '</pre>';
        */ 

        return $nodeData;
    }

    //_____________________________________________________________________________________________
    public function getAclMemberRootAreaNode($id, $parentId){
        if($this->nodeName == 'root'){
            return null;
        }
        if($this->isAclMemberRootArea){
            return $this;
        }
        // current node is not an AclMemberRootArea, so look up
        $parentNode = $this->nodeFactory->getNode($this->parentNodeName);
        $parentNode->fetchItemRaw($parentId);
        return $parentNode->getAclMemberRootAreaNode($parentNode->item['id'], $parentNode->item['parent_id']);
    }
    //_____________________________________________________________________________________________

    public function isUserInMemberArea($user, $parentId, $thisId){
        if($this->isAclMemberRootArea){
            $memberNode = $this->nodeFactory->getNode($this->aclMemberNodeName);
            $memberNode->setSiblingsWithParentId($thisId);
            $members = $memberNode->transformOut();
            foreach($members->siblingItems as $k => $member){
                if($user['id'] == $member['variable_fields']['user_id']){
                    return TRUE;
                }
            }
        }else{ 
            // current node is not an AclMemberRootArea, so look up
            $parentNode = $this->nodeFactory->getNode($this->parentNodeName);
            $parentNode->fetchItemRaw($parentId);
            return $parentNode->isUserInMemberArea($user, $parentNode->item['parent_id'], $parentId);
        }
        return FALSE;
    }


    //_____________________________________________________________________________________________
    /*
     * a plain get item function is nessecary for authorizationService->collectPermissionsForItem
    */
    public function fetchItemRaw($id){
        $record = $this->repository->getItem($id);
        $this->item = $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $this->commonFieldNames, $this->variableFieldNames);
    }

    //_____________________________________________________________________________________________
    public function fetchItem($id){
        $user = $this->config->sessionVo->get('login');           
        //print_arr($user);
        $publish = $user['role'] == 'anon' ? true : false;
        $record = $this->repository->getItem($id, $publish);             
        $this->item = $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $this->commonFieldNames, $this->variableFieldNames);
        $this->item['permission'] = $this->authorizationService->collectPermissionsForItem($this, $user, $this->item['owner_id'], $this->item['id']);
        /*  see transformOut
        //echo $this->item['filename'] . '<br>';
        if($this->imagelibService->isImage($this->item['filename'])){  
            //echo '-1-';
            $this->item['file_url'] = $this->imagelibService->getFileResizedUrls($this->item['subdir'], $this->item['filename'], $this->image_resize);
        }else{
            //echo '-2-';
            $this->item['file_url'] = $this->imagelibService->getFileOriginalUrl($this->item['subdir'], $this->item['filename']);
        } */
    }        
       
    //_____________________________________________________________________________________________
    public function fetchChildren($id){
        $user = $this->config->sessionVo->get('login');
        $itemOwnerId = $this->repository->getItemField('owner_id', $id);
        $this->childrenCollection = array();
        foreach($this->childNodeNames as $k => $nodeName){
           $childNode = $this->nodeFactory->getNode($nodeName);
           // set the create permission for the cms-children-view
           $childNode->permission = $this->authorizationService->collectPermissionsForNode($childNode, $user, $itemOwnerId, $id);
           $childNode->setSiblingsWithParentId($id);    
           $this->childrenCollection[$nodeName] = $childNode->transformOut();          
           //print_arr($this->childrenCollection);
        }
    }

    ////_____________________________________________________________________________________________
    //public function fetchChildrenForAdmin($id){
    //    $user = $this->config->sessionVo->get('login');
    //    $itemOwnerId = $this->repository->getItemField('owner_id', $id);
    //    $this->childrenCollection = array();
    //    foreach($this->childNodeNames as $k => $nodeName){
    //       $childNode = $this->nodeFactory->getNode($nodeName);
    //       // set the create permission for the cms-children-view
    //       $childNode->permission = $this->authorizationService->collectPermissionsForNode($childNode, $user, $itemOwnerId, $id);
    //       $childNode->setSiblingsWithParentId($id);
    //       $this->childrenCollection[$nodeName] = $childNode->transformOut();
    //    }
    //}  
    
    
    //_____________________________________________________________________________________________
    public function fetchSiblings($id){
       if($this->item == null){
           $this->fetchItem($id);
       }
       $this->setSiblingsWithParentId($this->item['parent_id']); 
    }      
    
     
/*       
    //_____________________________________________________________________________________________
    public function fetchTree($id){  
        
       if($this->item == null){
           $this->fetchItem($id);
       } 
       
       if($this->siblingItems == null){
            $this->setSiblingsWithParentId($this->item['parent_id']);  
       }  
       
       if($this->childrenCollection == null){
           $this->fetchChildren($id);
       } 
                  
       if($this->crumb == null){
            $this->fetchCrumb($id);   
       }       
       
      //echo 'z-z-z'; 
       if($this->tree == null){
            $this->fetchTreeList($id);   
            //$this->tree.root.children         
       }       
       
       
    } 
*/         

    //_____________________________________________________________________________________________   
    /**
    *  fetchTree fetches the tree from root to current item for display on the site as a tree view.
    *  currently it shows only two levels deep (root -> chapter -> page)
    */
    public function fetchTree($id){     
        //echo 'fetchTreeList';
       //$this->tree = $this->crumbService->getTree($this, $id);   
       ////print_r($this->crumb); exit();      
       ////test only: throw new GracianWarningException('test exception', 'test the exception being written to the log file');   
        //echo '-getTree-';
        // if id is filled (not null), then you are calling this method from the outside. 
        // in that case, first fetch the crumbItem and set it in the node
        if($id != null){ 
            $this->setItemForCrumb( $id ); 
        }    
                                                         
       if($this->crumb == null){
            $node->fetchCrumb($id);   
       }         
        //precondition
        if(!isset($this->crumbItem['id'])){
            throw new GracianException('CrumbService: node has empty crumbItem. probably you called getCrumb() without an id.');
        }    
        
        $tree = array();
        $tree['root'] = $this->crumb[0];  
        //echo $this->crumb[1]['id'];    
                                             
        if(count($this->crumb) > 1){    
            $nodeName1 = $this->crumb[1]['node_name'];  
            //echo $nodeName1;  
            $id1 = $this->crumb[1]['id'];
            $parentId1 = $this->crumb[0]['id'];          
            $node1 = $this->nodeFactory->getNode($nodeName1);  
            $node1->setItemForCrumb( $id1 );   
            $node1->setSiblingsWithParentId($parentId1);
            //print_r($node1->siblingItems);    
            //foreach ($node1->siblingItems as $k => $v){
            //   $tree['root']['children'][$nodeName1][$k]['item'] = $v; 
            //}
            $tree['root']['children'][$nodeName1] = $node1->siblingItems;
        }        

        if(count($this->crumb) > 2){    
            $nodeName2 = $this->crumb[2]['node_name'];  
            //echo $nodeName2;  
            $id2 = $this->crumb[2]['id'];
            $parentId2 = $this->crumb[1]['id'];
            $node2 = $this->nodeFactory->getNode($nodeName2);  
            $node2->setItemForCrumb( $id2 );   
            $node2->setSiblingsWithParentId($parentId2);
            //print_r($node2->siblingItems);    
            foreach($tree['root']['children'][$nodeName1] as $k => $v){   
                $index = 0;
                if($v['id'] == $parentId2){
                    $index = $k; 
                    break;
                }
            } 
            //foreach ($node1->siblingItems as $k => $v){
            //   $tree['root']['children'][$nodeName1][$index]['children'][$nodeName2][$k]['item'] = $v; 
            //}        
            $tree['root']['children'][$nodeName1][$index]['children'][$nodeName2] = $node2->siblingItems;
        }
        
        $this->tree = $tree;   
        //echo '<pre>'; print_r( $this->crumb); echo '</pre>';
        //echo '<pre>'; print_r( $tree); echo '</pre>';
                                                                
       
    }    

    //_____________________________________________________________________________________________
    public function fetchCrumb($id){
       $this->crumb = $this->crumbService->getCrumb($this, $id);   
       //print_r($this->crumb); echo '<br>'; //exit();      
       //test only: throw new GracianWarningException('test exception', 'test the exception being written to the log file');   
    }    
    
    //_____________________________________________________________________________________________
    /*
    deze wordt aangeroepen door fetchChildren
    Root method fetchChildren (id)
        calls Chapter method setSiblingsWithParentId (id)
    */
    public function setSiblingsWithParentId($parentId, $withPermissions=true){
        $user = $this->config->sessionVo->get('login');
        $publish = $user['role'] == 'anon' ? true : false;  
        $this->siblingItems = $this->repository->getItemsOfParentOfNode($parentId, $this->nodeName, $publish) ;   
        //print_arr($this->siblingItems);
        foreach($this->siblingItems as $k => $record){
            $this->siblingItems[$k] =  $this->jsonFieldsService->jsonToVariableAndOrphanFields($record, $this->commonFieldNames, $this->variableFieldNames);
        }
        $tmpNode = $this->nodeFactory->getNode($this->nodeName);
        foreach($this->siblingItems as $k => $item){
            $tmpNode->item = $item;    
            
            // see transformOut: $this->siblingItems[$k]['file_abs_url'] = $this->imagelibService->getFileOriginalUrl($item['subdir'], $item['filename']);//$tmpNode->getFileAbsUrl();    
            if($withPermissions){
                $this->siblingItems[$k]['permission'] = $this->authorizationService->collectPermissionsForItem($tmpNode, $user , $tmpNode->item['owner_id'], $tmpNode->item['id']);
            }
        }
    }    
    


    /*
      crumb-item = id, parent_id, title
    */
    public function setItemForCrumb($id){
       $this->crumbItem = $this->repository->getItemForCrumb($id);
    }

    //_____________________________________________________________________________________________
    public function setChildCount($id){
        $this->childCount = $this->repository->countChildrenOfParent($id);
    }   
    
    //_____________________________________________________________________________________________
    public function assertZeroChildren($id){
        $this->setChildCount($id);
        if($this->childCount > 0){
            throw new GracianWarningException('This item cannot be removed because it has children. First remove all its children. Then remove this item.'); 
        }             
    }

    //_____________________________________________________________________________________________
    /**
    *   updateItem only updates the fields that are present in the item
    *
    */
    public function updateItem($id, $item){     
        $item = $this->entityService->correctCheckboxes($item, array('publish'));      
        // NOTE: put all the Type-fields as a json object in the  variable_fields_json field
        $item = $this->jsonFieldsService->variableAndOrphanFieldsToJson($item);    
        // NOTE: only allow the fields in the whitelist : this is done in PurifierSanitizeAdapter::whitelistFields(),
        $this->repository->update($id, $item);
    }



    //_____________________________________________________________________________________________
    public function setEmptyItem($parentId=0){
        //precondition: $this->item must not exist
        if(count($this->item) > 0){
            return;
        }

        $this->item = array();

        // NOOIT ID AANMAKEN in empty item... 
        $this->item['parent_id'] = $parentId;
        $this->item['title'] = '';
        $this->item['node_name'] = $this->nodeName;
        $this->item['nodetype_name'] = $this->nodeType;
        $this->item['publish'] = 0;
        $this->item['publish_start_date'] = $this->dbHelper->getDateTimeNow();     
        $this->item['creation_date'] = $this->item['publish_start_date'];
        $this->item['modify_date'] = $this->item['publish_start_date'] ;
        $this->item['subdir'] = $this->dbHelper->generate_subdir();         
        $this->item['filename'] = '';
        $this->item['variable_fields_json'] = '';
        $this->item['variable_fields'] = $this->listVariableFieldsNames();

        // why?
        $this->item['parentnode_name'] = $this->parentNodeName;

        $user = $this->config->sessionVo->get('login');
        $this->item['permission'] = $this->authorizationService->collectPermissionsForNode($this, $user, 0, $parentId);
    }   
    

    //_____________________________________________________________________________________________
    private function listVariableFieldsNames(){   
        $variableFields = array();     
        if(count($this->variableFieldNames) > 0){            
            foreach($this->variableFieldNames as $k => $v){
                $variableFields[$k] = '';
            }    
        }
        return  $variableFields;
    }    


    //_____________________________________________________________________________________________
    public function mergeItemWithChangedFormfields($fields){
        $this->item = $this->entityService->mergeItemWithFields($this->item, $fields);  
    }
   
    //_____________________________________________________________________________________________
    public function storeFields($parentId, $fields){     
        $fields = $this->entityService->correctCheckboxes($fields, array('publish'));
        $this->setEmptyItem($parentId);    
        $this->item = $this->entityService->mergeItemWithFields($this->item, $fields);    
        $this->item = $this->jsonFieldsService->variableAndOrphanFieldsToJson($this->item);   
        $this->item = $this->entityService->filterItemWithWhitelist($this->item, $this->commonFieldNames);    
        // NOTE: only allow the known fields  : this is done in PurifierSanitizeAdapter::whitelistFields(),
        // the store method returns the new id of the created record
        return $this->repository->store($this->item);             
    }     


    //_____________________________________________________________________________________________
    public function destroyItem($id){     
        // TODO: lazy instantionation if $this->item  (now, the use case sets the item)
        //$this->config->fileUploadAdapter->removeFile($this->config->app['file_ upload_abs_path'], $this->item['subdir'], $this->item['filename']);
        $this->config->fileUploadAdapter->removeFile($this->config->app['media_abs_path'] . '/original', $this->item['subdir'], $this->item['filename']);
        $this->repository->destroy($id);
    }

    //_____________________________________________________________________________________________
    // private methods
    //_____________________________________________________________________________________________
                  
                  
}
