<?php namespace gracian_system\domain\model\stree;

class StreeArticleType extends Stree{ 

    public $repositoryName = 'stree';
    public $nodeType = 'streeArticleType';

    public $viewTemplate = 'StreeArticleViewTpl';
    public $formTemplate = 'StreeArticleFormTpl';
    public $variableFieldNames = array(
        'introduction' => array('sanitize' => 'purge', 'validate' => 'string'),        
        'bodytext' => array('sanitize' => 'body', 'validate' => 'string')        
    );
    


}
