<?php namespace gracian_system\domain\model\tag;

use gracian_system\domain\model\base\BaseEntity;
use gracian_project\application\service\ConfigFactory;
use gracian_project\domain\model\tag\PrjTagEntity;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianAuthenticationException;
//use gracian_system\domain\exceptions\CustomValidationException;
use gracian_system\domain\model\base\entityIF;           
use gracian_system\domain\model\valueObjects\SchemaVO;   
use gracian_system\domain\service\EntityService;        

class TagEntity extends BaseEntity{ 

    public $repositoryName = 'tag';
    public $repository = null;
    public $config = null;

    public $formTemplate = 'tag_form';

    public $entityName = 'tag';
    public $nodeName = 'tag';            
      
    private $commonFieldNames = array(
       'id'   => array('sanitize' => 'purge', 'validate' => 'required|integer'),
       'name' => array('sanitize' => 'purge', 'validate' => 'required|string|max:32'),   
    );   

    public $acl = Array(
        "anon"         => Array(    "create" => "ALLOWED",    "read" => "ALLOWED",    "update" => "ALLOWED",    "delete" => "ALLOWED"),
        "su"           => Array(    "create" => "ALLOWED",    "read" => "ALLOWED",    "update" => "ALLOWED",    "delete" => "ALLOWED"),
        "admin"        => Array(    "create" => "DENIED",    "read" => "ALLOWED",    "update" => "DENIED",    "delete" => "DENIED"),
        "count_member" => Array(    "create" => "DENIED",     "read" => "ALLOWED",     "update" => "DENIED",     "delete" => "DENIED")
    );     
    
    public $relationCount = 0;    
    


    //_____________________________________________________________________________________________
    public function __construct() {    
        parent::__construct();   
        $this->config = ConfigFactory::Instance()->config;    
        $this->repository = $this->config->repositoryFactory->getRepository($this->repositoryName);
        $this->entityService = new EntityService();    
        $this->schemaVO = new SchemaVO($this->commonFieldNames);
    }                                                   

    //_____________________________________________________________________________________________
    public function listAll(){
        $user = $this->config->sessionVo->get('login');    
        $this->permission = $this->authorizationService->collectPermissionsForUserNode($this, $user);
        $list = $this->repository->listAll();
        $tagEntity = new PrjTagEntity();
        foreach($list as $k => $item){
            $tagEntity->item = $item;
            $list[$k]['permission'] = $this->authorizationService->collectPermissionsForUser($tagEntity, $user);
        }
        $this->tagList = $list;
    }   
    
    //_____________________________________________________________________________________________
    public function setEmptyItem(){
      $this->item = array();
      foreach($this->commonFieldNames as $k => $v){
          $this->item[$k] = '';
      }  
      $user = $this->config->sessionVo->get('login');
      $this->item['permission'] = $this->authorizationService->collectPermissionsForUserNode($this, $user);
    }          
    
    //_____________________________________________________________________________________________
    public function transformOut(){
        $data = new \stdClass();
        if(isset($this->item)){
            $data->item = $this->item;
        }
        $data->commonFieldNames = $this->commonFieldNames;
        if(isset($this->tagList)){
            $data->tagList = $this->tagList;
        }
        if(isset($this->permission)){
            $data->permission = $this->permission;
        }    
        if(isset($this->relationCount)){
            $data->relationCount = $this->relationCount;
        }        
        return $data;
    }      
    

    //_____________________________________________________________________________________________
    public function storeFields( $fields){
        $this->setEmptyItem();        
        $this->item = $this->entityService->mergeItemWithFields($this->item, $fields);
        unset($this->item['id']);
        $this->item = $this->entityService->filterItemWithWhitelist($this->item, $this->commonFieldNames);    
        //precondition: check if name is taken (if it is filled)
        $records = $this->repository->fetchOthersWithName(0, $this->item['name']);
        if(count($records) > 0){
            throw new GracianException('TagEntity: Cannot store tag, name is already used.');
        }    
        // return the new Id
        return $this->repository->store($this->item);
    }     
    
    //_____________________________________________________________________________________________
    public function getEmptyItem(){
        $this->setEmptyItem();
        return $this->item;
    }        
    
    //_____________________________________________________________________________________________
    public function mergeItemWithChangedFormfields($fields){
        $this->item = $this->entityService->mergeItemWithFields($this->item, $fields);       
    }   
    //_____________________________________________________________________________________________
    public function fetchItem($id){
        $this->item = $this->repository->read($id);
        $user = $this->config->sessionVo->get('login');
        $this->item['permission'] = $this->authorizationService->collectPermissionsForUser($this, $user);
    }     
    
    //_____________________________________________________________________________________________
    public function updateItem($fields){
        //precondition: check if name is taken (if it is filled)
        $records = $this->repository->fetchOthersWithName($fields['id'], $fields['name']);             
        if(count($records) > 0){
            throw new GracianException('TagEntity: Cannot update tag, name is already taken.');
        }
        $this->repository->update($fields);
    }
         
    //_____________________________________________________________________________________________
    public function setRelationCount($id){     
        $this->relationCount = $this->repository->countRelations($id);     
    }         
    
    //_____________________________________________________________________________________________
    /* destroys also all relations */
    public function destroyItem($id){
        $this->repository->destroyRelations($id);  
        $this->repository->destroy($id);
    }   
       

}
