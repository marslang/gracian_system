<?php namespace gracian_system\domain\model\base;

use gracian_project\application\service\ConfigFactory;
use gracian_system\domain\service\AuthorizationService;
use gracian_system\domain\exceptions\GracianException;
use gracian_system\domain\exceptions\GracianValidationException;
//use gracian_system\domain\model\base\entityIF;
use gracian_system\infrastructure\repository\pdo\PdoUserRepository;
use gracian_system\infrastructure\helpers\DbHelper;
use gracian_system\infrastructure\helpers\FormdateHelper;
use Respect\Validation\Exceptions\ValidationExceptionInterface;     

abstract class BaseEntity{ 
                                            
    public $isAclMemberRootArea = FALSE;
    public $aclMemberNodeName = '';

    public $acl = Array(
    "anon"       => Array("create"=>"DENIED",    "read"=>"DENIED", "update"=>"DENIED",    "delete"=>"DENIED"),
    "su"         => Array("create"=>"ALLOWED",    "read"=>"ALLOWED", "update"=>"ALLOWED",    "delete"=>"ALLOWED"),
    "admin"      => Array("create"=>"ALLOWED",    "read"=>"ALLOWED", "update"=>"ALLOWED",    "delete"=>"ALLOWED"),
    "user_admin" => Array("create"=>"DENIED",    "read"=>"ALLOWED", "update"=>"DENIED",    "delete"=>"DENIED"),
    );      
    
    public $schema = array();

    //_____________________________________________________________________________________________
    public function __construct() {
        $this->authorizationService = new AuthorizationService();     
    }      
    
    //_____________________________________________________________________________________________
    public function getSchema(){  
        return $this->schemaVO->getSchema();     
    }                       
    
    //_____________________________________________________________________________________________
    public function transformOut(){
    }
    


}
