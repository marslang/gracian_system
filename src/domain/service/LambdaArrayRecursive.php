<?php namespace gracian_system\domain\service;   

class LambdaArrayRecursive {
     
    private $a = array();

    //_____________________________________________________________________________________________
    public function __construct($a, $lambdaFunc){
        $this->a = $a;     
        $this->func = $lambdaFunc;
    }           
    
    //_________________________________________________________________________
    public function parseArray(){     
        $this->recurse_array($this->a);
        return $this->a;
    }
    

    //_________________________________________________________________________
    /* --- pseudocode: --- 
    // het is een array
        // array is gevuld
            // loop door de elements
                //eerste element
                //is het een array? 
                    // voeg de key toe NOTE: add it with count($keys) ,otherwise the index won't always be 0   
                    // dan parse het recursive   
                    // als je terugkomt hier, haal weer eraf
                //anders bewerk het
    */
    private function recurse_array($value,  $keys = array()){      
        if(is_array($value)){  
            if(count($value) > 0){  
                foreach($value as $k => $v){  
                    if(is_array($v)){  
                        $keys[count($keys)] = $k;  
                        $this->recurse_array($v, $keys);  
                        unset($keys[ count($keys)-1]); 
                    }else{ 
                        $this->change_array_value($keys, $k, $v);
                    }
                }
            }
        }
    }               
    
    
    //_________________________________________________________________________
    private function change_array_value($keys, $k, $value){  
        if(count($keys) === 0){    
            $this->a[$k] = call_user_func_array($this->func, array($value));
        }     
        elseif(count($keys) === 1){   
            $this->a[$keys[0]][$k] = call_user_func_array($this->func, array($value));  
        }
        elseif(count($keys) === 2){
            $this->a[ $keys[0] ][ $keys[1] ][$k] = call_user_func_array($this->func, array($value)); 
        }           
        elseif(count($keys) === 3){
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][$k] = call_user_func_array($this->func, array($value)); 
        }           
        elseif(count($keys) === 4){
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][$k] = call_user_func_array($this->func, array($value)); 
        }           
        elseif(count($keys) === 5){
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][$k] = call_user_func_array($this->func, array($value)); 
        }           
        elseif(count($keys) === 6){
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][ $keys[5] ][$k] = call_user_func_array($this->func, array($value)); 
        }           
        elseif(count($keys) > 6){
            throw  new \Exception('GracianAssertException: ArrayService::change_array_value() array is more than 6 dimensions. add a dimension handler to this method');
        }        

    }
    
     
    
}    
