<?php namespace gracian_system\domain\service;   
                                   
/*
* this claas parses two arrays with the same structure.
* for example: one is a field-tree
* the second is a schema tree with the sanitization an validation rules
* the field-array is read/write
* the schema-tree is read-only
*/
class LambdaArrayRecursive2Args {
     
    private $a = array();    
    private $b = array();
    private $hasB = false;     
                               
    //_____________________________________________________________________________________________
    public function __construct($a, $b, $lambdaFunc ){
        $this->a = $a;     
        $this->b = $b;     
        $this->func = $lambdaFunc;
    }           
    
    public function parseArray(){     
        $this->recurse_array($this->a);
        return $this->a;
    }

    private function recurse_array($value,  $keys = array()){    
        if(is_array($value)){  
            if(count($value) > 0){  
                foreach($value as $k => $v){  
                    if(is_array($v)){  
                        $keys[count($keys)] = $k;  
                        $this->recurse_array($v, $keys);  
                        unset($keys[ count($keys)-1]); 
                    }else{ 
                        $this->change_array_value($keys, $k, $v);
                    }
                }
            }
        }
    }               
    
    private function change_array_value($keys, $k, $value){  
        if(count($keys) === 0){    
            //with lambdaFunction     
            $valueB = isset($this->b[$k]) ? $this->b[$k]  : null;
            $this->a[$k] = call_user_func_array($this->func, array($value, $valueB) );
        }     
        elseif(count($keys) === 1){   
            $valueB = isset($this->b[$keys[0]][$k]) ? $this->b[$keys[0]][$k]  : null;
            $this->a[$keys[0]][$k] = call_user_func_array($this->func, array($value, $valueB));  
        }
        elseif(count($keys) === 2){
            $valueB = isset($this->b[ $keys[0] ][ $keys[1] ][$k]) ? $this->b[ $keys[0] ][ $keys[1] ][$k]  : null;
            $this->a[ $keys[0] ][ $keys[1] ][$k] = call_user_func_array($this->func, array($value, $valueB)); 
        }           
        elseif(count($keys) === 3){
            $valueB = isset($this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][$k]) ? $this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][$k]  : null;
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][$k] = call_user_func_array($this->func, array($value, $valueB)); 
        }           
        elseif(count($keys) === 4){
            $valueB = isset($this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][$k]) ? $this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][$k]  : null;
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][$k] = call_user_func_array($this->func, array($value, $valueB)); 
        }           
        elseif(count($keys) === 5){
            $valueB = isset($this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][$k]) ? $this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][$k]  : null;
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][$k] = call_user_func_array($this->func, array($value, $valueB)); 
        }           
        elseif(count($keys) === 6){
            $valueB = isset($this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][ $keys[5] ][$k]) ? $this->b[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][ $keys[5] ][$k]  : null;
            $this->a[ $keys[0] ][ $keys[1] ][ $keys[2] ][ $keys[3] ][ $keys[4] ][ $keys[5] ][$k] = call_user_func_array($this->func, array($value, $valueB)); 
        }           
        elseif(count($keys) > 6){
            throw  new \Exception('GracianAssertException: ArrayService::change_array_value() array is more than 6 dimensions. add a dimension handler to this method');
        }        
    }
    
}    
