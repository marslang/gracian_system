<?php namespace gracian_system\domain\service;

use gracian_system\domain\domainPorts\AuthorizationServiceIF;   
use gracian_project\domain\service\NodeFactory;
use gracian_system\domain\exceptions\GracianAuthorizationException;


    /*
     actionList 
        'create' > 'create',
        'store' > 'create',
        'view' > 'read',
        'show' = 'read',
        'update' = 'update',
        'edit' > 'update',
        'delete' > 'delete',
        'destroy' > 'delete'
    
    */

class AuthorizationService implements AuthorizationServiceIF {    
    

    private $actions = array('create', 'read', 'update', 'delete');

    //_____________________________________________________________________________________________
    public function __construct(){
        $this->nodeFactory = new NodeFactory();
    }

    //_____________________________________________________________________________________________
    public function collectPermissionsForUser($entity, $user){
        $permission = array();
        foreach($this->actions as $k => $action){
            $permission[$action] = $this->getUserPermissionForAction($action, $user, $entity); 
        }
        return $permission;
    }
    //_____________________________________________________________________________________________
    public function collectPermissionsForUserNode($entity, $user){
        $permission = array();
        $action = 'create';
        $permission[$action] = $this->getUserPermissionForAction($action, $user, $entity);
        return $permission;
    }

    //_____________________________________________________________________________________________
    public function collectPermissionsForItem($node,  $user, $itemOwnerId, $itemId){ 
        $permission = array();     
        //print_r($user); exit();
        foreach($this->actions as $k => $action){
            $permission[$action] = $this->getItemPermissionForAction($action, $user, $node, $itemOwnerId, $itemId); 
        }
        return $permission;
    }

    //_____________________________________________________________________________________________
    /* we need a create-permission for all children nodes
    node: a childNode of the node currently viewed
    user: the login User
    itemOwnerId the owner id of the item currently viewed
    itemId: the id of the item currently viewed
    */
    public function collectPermissionsForNode($node, $user, $itemOwnerId, $itemId){
        $permission = array();
        $action = 'create';
        $permission[$action] = $this->getItemPermissionForAction($action, $user, $node, $itemOwnerId, $itemId); 
        return $permission;
    }
    
    //_____________________________________________________________________________________________
    /*
    * the private functions
    */
    //_____________________________________________________________________________________________
    private function getUserPermissionForAction($action, $user, $entity){ 
        $role = $user['role'];
        if(!isset($entity->acl[$role])){
            throw new GracianAuthorizationException( 'AuthorizationService: ' .  $entity->nodeName . '->acl[`'.$role.'`]'   . ' not found ' );
        }
        $right = $entity->acl[$role][$action];
        if($right == 'ALLOWED'){
            return TRUE;
        }
        if($right == 'DENIED'){
            return FALSE;
        }
        if($right == 'OWNER'){
            throw new GracianAuthorizationException( 'AuthorizationService: ' .  $entity->nodeName . '->acl[`'.$role.'`]'   . ' has permission set to OWNER. The only possible values are ALLOWED, DENIED. ' );
        }
        if($right == 'MEMBER'){
            throw new GracianAuthorizationException( 'AuthorizationService: ' .  $entity->nodeName . '->acl[`'.$role.'`]'   . ' has permission set to MEMBER. The only possible values are ALLOWED, DENIED. ' );

        }
    }


    //_____________________________________________________________________________________________
    private function getItemPermissionForAction($action, $user, $node, $itemOwnerId, $itemId){    
        
        $role = $user['role'];
        if(!isset($node->acl[$role])){
            throw new GracianAuthorizationException( 'AuthorizationService: ' .  $node->nodeName . '->acl[`'.$role.'`]'   . ' not found ' );
        }  
                 
        $hasPermission = FALSE;
        $right = $node->acl[$role][$action];

        if($right == 'ALLOWED'){
            $hasPermission = TRUE;
        }
        if($right == 'DENIED'){
            $hasPermission = FALSE;
        }
        if($right == 'OWNER'){  
            $hasPermission = $this->getItemPermissionForOwner($itemOwnerId, $user['id']);
        } 
        
        if($right == 'MEMBER'){     
            $hasPermission = $this->getItemPermissionForMember($action, $user, $node, $itemOwnerId, $itemId);
        }  
        return $hasPermission;

    }        
 
    //_____________________________________________________________________________________________
    private function getItemPermissionForOwner($itemOwnerId, $userId){
        if($itemOwnerId == $userId){
            return TRUE;
        }
        return FALSE;
           
    }    

    //_____________________________________________________________________________________________       
    private function getItemPermissionForMember($user, $node, $itemId){
        $userIsInMemberArea = FALSE;     
        if(isset($node->item['id'])){ 
            // get the 4 permissions for the item
            $current = $node;
            $aclMemberRootArea = $node->getAclMemberRootAreaNode($current->item['id'], $current->item['parent_id']);
            if($aclMemberRootArea != null){
                $memberNode = $this->nodeFactory->getNode($aclMemberRootArea->aclMemberNodeName);
                $memberNode->setSiblingsWithParentId($aclMemberRootArea->item['id'], $withPermissions=FALSE);
                $members = $memberNode->transformOut();
                foreach($members->siblingItems as $k => $member){
                    if($user['id'] == $member['variable_fields']['user_id']){
                        $userIsInMemberArea = TRUE;    
                        break;
                    }
                }
            }
        }else{ 
            // get only the create permission for the child node without an item-list    
            //if($node->isUserInMemberArea($user, $itemId, null)){
            //    $userIsInMemberArea = TRUE;
            //}else{
            //    $userIsInMemberArea = FALSE; 
            //} 
            $userIsInMemberArea = $node->isUserInMemberArea($user, $itemId, null); // ? true : false;     

        }        



        return $userIsInMemberArea;     
    }        

}
