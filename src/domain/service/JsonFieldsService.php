<?php namespace gracian_system\domain\service;   

class JsonFieldsService {    
    
    
    /*
    put all JsonFields and all VariableFields in $record['variable_fields'] 
    */
    //_____________________________________________________________________________________________
    public function jsonToVariableAndOrphanFields($record, $commonFieldNames, $variableFieldNames){    
        //print_arr($variableFieldNames);
        // add an variable_fields-Array to the record you received from the database
        $record['variable_fields'] = array();   
        $record['orphan_fields'] = array();
        $uncompressed = (array) json_decode($record['variable_fields_json']);   
        // put all json fields from the $variableFieldNames and all missing $variableFieldNames in  record['variable_fields']    
        // put all json fields not in the $variableFieldNames in  record['orphan_fields'] 
        // leave out the orphan_fields with an empty value
        if(count($uncompressed) > 0){   
            foreach($uncompressed as $k => $v){
                // on the way, check if a json-field has the same name as a commonField  . throw exception if so
                //if(in_array($k , $commonFieldNames)){
                //    throw new Exception('Error in StreePdoRepositoryDecorator: a json-fieldname is the same as a table fieldname. ');
                //} 
                // if jsonfield is not a variablefieldname (is an orphanfield) and it is empty , then don't add it  to record['variable_fields']
                if(array_key_exists($k, $variableFieldNames)) {
                    $record['variable_fields'][$k] = $v;
                }else{
                    if($v != ''){
                        $record['orphan_fields'][$k] = $v;
                    }
                }
            }
        }
        // add the missing fields from the type-whitelist
        foreach($variableFieldNames as $k => $v){
            if(!isset( $record['variable_fields'][$k])){
               $record['variable_fields'][$k] = '';
            }
        }   
        unset($record['variable_fields_json']); 
        return $record;
    }     
    
    //_____________________________________________________________________________________________
    public function variableAndOrphanFieldsToJson($fields){ 
        $fields = $this->remove_orphan_fields_checked_for_removal($fields);
        if(isset($fields['orphan_fields']) ){
            if( count($fields['orphan_fields']) > 0){
                $fields['variable_fields'] = array_merge($fields['variable_fields'], $fields['orphan_fields']);
            }
            unset($fields['orphan_fields']);
        }
        $fields['variable_fields_json'] = json_encode($fields['variable_fields']);       
        unset($fields['variable_fields']);
        return($fields);
    }   
    
    
    //_____________________________________________________________________________________________
    private function remove_orphan_fields_checked_for_removal($fields){   
        if(isset($fields['orphan_fields']) ){  
            if(count($fields['orphan_fields']) > 0){  
                foreach($fields['orphan_fields']  as $k => $v){
                    if(isset($fields['orphan_fields']['remove_' . $k])){
                        unset($fields['orphan_fields'][$k]);
                        unset($fields['orphan_fields']['remove_' . $k]);
                    }
                }    
            }   
        }
        return $fields;    
    }          
           

   /*
    //_____________________________________________________________________________________________
    public function getOrphanJsonFields($variableFieldNames, $variableFields){
        $orphanFields = array();
        // NOTE: if a jsonField is missing in the database, then add it so the form-view won't crash     
        // zorg dat alle velden uit de whitelist in de itemList zitten
        foreach($variableFieldNames as $k => $v){
            if(!isset($variableFields[$k])){
                $variableFields[$k] = '';
            }
        }
        // NOTE: if there are fields in the database that ar not defined in the node or entity, save them as orphanFields, so the data won't get lost
        // stop nu alle velden uit de itemList die niet in de whiteList zitten  in de orphanList (als ze gevuld zijn met tekst)
        foreach($variableFields as $k => $v){    
            // check if it is an orphan field
            if(!isset($variableFieldNames[$k])){
                $orphanFieldHasContent =  trim($v) != '';
                if($orphanFieldHasContent){
                    $orphanFields[$k] = $v;
                }
            }
        }
        return $orphanFields;
    }      
    */
    

               

}
