<?php namespace gracian_system\domain\service;

use gracian_system\domain\domainPorts\EntityServiceIF;  

class EntityService implements EntityServiceIF { 

    //_____________________________________________________________________________________________
    public function mergeItemWithFields($item, $fields){
        if(count($fields) > 0){
            foreach($fields as $k => $v){
                if(is_array($v)){
                    foreach($v as $k2 => $v2){
                         $item[$k][$k2] = $v2;
                    }
                }else{
                    $item[$k] = $v;
                }
            }
        }    
        return $item;
    }     
    
    //_____________________________________________________________________________________________
    public function filterItemWithWhitelist($item, $commonFieldNames){
        // NOTE: only allow the known fields
        $item_to_store = array();
        foreach($commonFieldNames as $k => $v){
            if(isset($item[$k])){
                $item_to_store[$k] = $item[$k];
            }
        }  
        return $item_to_store;  
    }    
    
    //_____________________________________________________________________________________________
    public function correctCheckboxes($fields, $arrayOfCheckboxFieldnames){
        if(count($arrayOfCheckboxFieldnames) > 0){
            foreach($arrayOfCheckboxFieldnames as $k => $v){     
                // precondition: fix the publish checkbox issue voor de test-cases  
                // NOTE: use the triple ===
                if(isset($fields[$v]) && $fields[$v] === 0){ 
                    unset($fields[$v]);  
                }
                $fields[$v] = isset($fields[$v]) ? 1 : 0;       
            } 
        }   
        return $fields;
    }   

}
