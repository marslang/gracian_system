<?php  namespace gracian_system\domain\service;     
                                                 
//use gracian_system\domain\domainPorts\CrumbServiceIF;        
//use gracian_system\domain\infrastructurePorts\ImagelibAdapterIF;      
use gracian_project\domain\service\NodeFactory;      
use gracian_system\domain\exceptions\GracianException;      
  

class ImageLibService  {         
    
    //_____________________________________________________________________________________________ 
    //public function __construct(ImagelibAdapterIF $adapter){     
    public function __construct($config){     
        $this->config = $config;
    }      
    
    //_____________________________________________________________________________________________ 
    public function getFileUrlArray($subdir, $filename, $image_resize){   
        $file_url = array();
        $file_path = $this->getFileOriginalPath($subdir, $filename, $image_resize);
        if(isset($file_path['original'])){   
            if(is_file($file_path['original'])){      
                if($this->isImage($filename)){           
                    $file_url = $this->getFileResizedUrls($subdir, $filename, $image_resize);
                }else{  
                    $file_url = $this->getFileOriginalUrl($subdir, $filename);
                }      
            }
        }   
        return $file_url;       
    }      
    
    //_____________________________________________________________________________________________  
    // private
    //_____________________________________________________________________________________________ 
    private function isImage($filename){    
        if($filename == ''){  
            return false;
        }          
        $imageExtensions = array('jpg', 'jpeg', 'png', 'gif');
        $pieces = explode('.', $filename) ;   
        //echo '-' . $pieces[count($pieces)-1];
        if(in_array($pieces[count($pieces)-1], $imageExtensions)){
            return true;
        }else{
            return false;
        }
    }      
    
    //_____________________________________________________________________________________________
    private function getFileOriginalPath($subdir, $filename){     
        if( $filename != ''){   
            $file_path['original'] = $this->config->app['media_abs_path'] . '/original/' . $subdir . '/' . $filename;    
            return $file_path;
        }else{
            return array();
        } 
    }
            
    
    //_____________________________________________________________________________________________
    private function getFileOriginalUrl($subdir, $filename){     
        if( $filename != ''){   
            $file_url['original'] = $this->config->app['media_url'] . '/original/' . $subdir . '/' . $filename;  
            return $file_url;
        }else{
            return array();
        } 
    }
                      
    //_____________________________________________________________________________________________
    private function getFileResizedUrls($subdir, $filename, $image_resize){      
        //echo 'ImageLibService->getFileResizedUrls';    
        //echo '<pre>';
        //debug_print_backtrace();    
        //echo '</pre>';
        //exit();
        $file_urls = array();    
        $pieces = explode('.', $filename) ;    
        $lastIndex = count($pieces)-1;
        $ext = $pieces[$lastIndex];
        unset($pieces[$lastIndex]);
        $name = implode('.' , $pieces);
        if( $filename != ''){   
           $file_urls['original'] = $this->config->app['media_url'] . '/original/' . $subdir . '/' . $filename;   
           //echo $file_urls['original']; exit();
           foreach($image_resize as $k => $v){
               $file_urls[$k] = $this->config->app['media_url'] . '/resized/' . $subdir . '/' . $name . '_' . $v['width'] . 'x' . $v['height'] . '.' . $ext;               
           }   
           $this->resize($subdir, $filename, $image_resize);    
        }
        return $file_urls; 
    }    
    
    //_____________________________________________________________________________________________
    private function resize($subdir, $filename, $image_resize){    
        $file_paths = array();   
        $pieces = explode('.', $filename) ;     
        $lastIndex = count($pieces)-1;
        $ext = $pieces[$lastIndex];
        unset($pieces[$lastIndex]);
        $name = implode('.' , $pieces);          
        $file_paths['original'] = $this->config->app['media_abs_path'] . '/original/' . $subdir . '/' . $filename;     
        //echo $file_paths['original']; exit();   
        $resized_path = $this->config->app['media_abs_path'] . '/resized/' . $subdir;
		if(!is_dir($resized_path )){    
		    //echo $resized_path; echo "\n";
			mkdir($resized_path, 0777);
		}
        foreach($image_resize as $k => $v){  
            $file_paths[$k] = $this->config->app['media_abs_path'] . '/resized/' . $subdir . '/' . $name . '_' . $v['width'] . 'x' . $v['height'] . '.' . $ext;               
			if(!is_file($file_paths[$k])){
                $this->config->imagelibAdapter->resize($file_paths['original'], $file_paths[$k],  $v['width'], $v['height']); 
			}               
        }  
    }       
 

}