<?php namespace gracian_system\domain\infrastructurePorts;

interface ForgotpasswordRepositoryIF {        
    
    public function cleanup();   
    public function store($id_hash, $now ,$userId);
    public function fetchWithHash($hash);
    public function destroy($id_hash); 
    public function destroyItemsOfId($user_id);

}                           