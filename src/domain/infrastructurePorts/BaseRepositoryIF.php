<?php namespace gracian_system\domain\infrastructurePorts;

interface BaseRepositoryIF {        
    
    public function truncateTestTable($tableName);   

}                           