<?php namespace gracian_system\domain\infrastructurePorts;   

interface StreeRepositoryIF {       
  
    public function getItemField($fieldname, $id);
    public function getItem($id, $publish=false);
    public function getItemForCrumb($id);
    public function getItemsOfParentOfNode($id, $nodeName, $publish=false);
    public function listOfNodeOfParent($nodeName, $id, $publish=false);
    public function listChildrenOfParent($parent_id);
    public function countChildrenOfParent($parent_id);
    public function store($fields_to_store);
    public function update($id, $fields_to_store);
    public function updateFieldForId($id, $fieldName, $fieldValue);
    public function destroy($id);
                                               

}
