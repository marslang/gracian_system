<?php namespace gracian_system\domain\domainPorts;

interface AuthorizationServiceIF {
                           
    public function collectPermissionsForUser($entity, $user);
    public function collectPermissionsForUserNode($entity, $user);
    public function collectPermissionsForItem($node,  $user, $itemOwnerId, $itemId);
    public function collectPermissionsForNode($node, $user, $itemOwnerId, $itemId);
     
}        
