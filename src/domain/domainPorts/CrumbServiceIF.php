<?php namespace gracian_system\domain\domainPorts;

interface CrumbServiceIF {
    public function getCrumb($node, $id=null);
}