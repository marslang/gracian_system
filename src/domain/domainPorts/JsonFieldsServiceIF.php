<?php namespace gracian_system\domain\domainPorts;

interface JsonFieldsServiceIF {  
    
     public function jsonToVariableAndOrphanFields($record, $commonFieldNames, $variableFieldNames);
     //public function variableAndOrphanFieldsToJson($fields);
     public function variableAndOrphanFieldsTojson($fields);           
     //public function getOrphanJsonFields($variableFieldNames, $variableFields);

    
}        
