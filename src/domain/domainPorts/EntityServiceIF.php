<?php namespace gracian_system\domain\domainPorts;

interface EntityServiceIF {
     public function mergeItemWithFields($item, $fields);
     public function filterItemWithWhitelist($item, $commonFieldNames);                          

    
}        
