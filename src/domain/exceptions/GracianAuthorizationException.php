<?php namespace gracian_system\domain\exceptions;

use gracian_system\domain\exceptions\GracianException;    

class GracianAuthorizationException extends GracianException {

    function __construct($userMessage = '', $logMessage = '')
    {
        parent::__construct($userMessage);
    }

}
