<?php

namespace gracian_system\domain\exceptions;


class GracianFileValidationException extends \Exception {          
    
    protected $logMessage = null; 


    function __construct($userMessage = '', $logMessage = '')
    {
        parent::__construct($userMessage);    
        $this->logMessage = $logMessage;      

    }

    public function getUserMessage()
    {
        return  $this->getMessage();
    }

     
    public function getLogMessage()
    {
        return $this->logMessage;
    }   
                 

}       