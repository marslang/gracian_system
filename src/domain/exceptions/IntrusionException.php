<?php  namespace gracian_system\domain\exceptions;    

/**
 * An IntrusionException should be thrown anytime an error condition arises that 
 * is likely to be the result of an attack in progress. IntrusionExceptions are 
 * handled specially by the IntrusionDetector, which is equipped to respond by
 * either specially logging the event, logging out the current user, or invalidating
 * the current user's account.
 * <P>
 * Unlike other exceptions in the ESAPI, the IntrusionException is a 
 * RuntimeException so that it can be thrown from anywhere and will not require a 
 * lot of special exception handling.
 *
 */
class IntrusionException extends \Exception
{
    
    protected $logMessage = null; 

    /**
     * Instantiates a new intrusion exception.
     * 
     * @param string $userMessage the message displayed to the user
     * @param string $logMessage  the message logged
     * 
     * @return does not return a value.
    */
    function __construct($userMessage = '', $logMessage = '')
    {
        parent::__construct($userMessage);    
        $this->logMessage = $logMessage;      
    }

    /**
     * Returns a String containing a message that is safe to display to users
     * 
     * @return string a String containing a message that is safe to display to users
     */
    public function getUserMessage()
    {
        return $this->getMessage();
    }

    /**
     * Returns a String that is safe to display in logs, but probably not to users
     * 
     * @return string a String containing a message that is safe to display in 
     *                logs, but probably not to users
     */   
     
    public function getLogMessage()
    {
        return $this->logMessage;
    }   
    

}
?>