<?php namespace gracian_system\domain\exceptions;

use gracian_system\domain\exceptions\GracianException;

class GracianAssertException extends GracianException {

    function __construct($userMessage = '', $logMessage = '')
    {
        parent::__construct($userMessage);
    }

}
