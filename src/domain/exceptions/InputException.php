<?php  namespace gracian_system\domain\exceptions;    

class InputException extends \Exception
{
    
    protected $logMessage = null; 


    function __construct($userMessage = '', $logMessage = '')
    {
        parent::__construct($userMessage);    
        $this->logMessage = $logMessage;      

    }

    public function getUserMessage()
    {
        return 'InputException: ' . $this->getMessage();
    }

     
    public function getLogMessage()
    {
        return 'InputException: ' . $this->logMessage;
    }   
    

}
?>